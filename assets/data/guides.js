export default {
  items: [
    {
      id: '1',
      title: 'Item text 1',
      image: 'https://picsum.photos/id/1/200',
      score: 1,
      author: {
        id: '1',
        name: 'Paul Noah',
        avatar: 'https://picsum.photos/id/1/40',
      },
    },
    {
      id: '2',
      title: 'Item text 2',
      image: 'https://picsum.photos/id/10/200',
      score: 1,
      author: {
        id: '1',
        name: 'Paul Noah',
        avatar: 'https://picsum.photos/id/1/40',
      },
    },

    {
      id: '3',
      title: 'Item text 3',
      image: 'https://picsum.photos/id/1002/200',
      score: 1,
      author: {
        id: '1',
        name: 'Paul Noah',
        avatar: 'https://picsum.photos/id/1/40',
      },
    },
    {
      id: '4',
      text: 'Item text 4',
      image: 'https://picsum.photos/id/1006/200',
      score: 1,
      author: {
        id: '1',
        name: 'Paul Noah',
        avatar: 'https://picsum.photos/id/1/40',
      },
    },
    {
      id: '5',
      title: 'Item text 5',
      score: 1,
      image: 'https://picsum.photos/id/1008/200',
      author: {
        id: '1',
        name: 'Paul Noah',
        avatar: 'https://picsum.photos/id/1/40',
      },
    },
  ],
};
