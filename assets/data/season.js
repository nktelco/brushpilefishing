export default {
  items: [
    {
      id: 7,
      title: 'Season 7',
      episodes: [
        {
          id: 1,
          title: 'Episode 1',
          place: 'Rend Lake, Illinois',
        },
        {
          id: 2,
          title: 'Episode 2',
          place: 'Rend Lake, Illinois',
        },
        {
          id: 3,
          title: 'Episode 3',
          place: 'Rend Lake, Illinois',
        },
        {
          id: 4,
          title: 'Episode 4',
          place: 'Rend Lake, Illinois',
        },
        {
          id: 5,
          title: 'Episode 5',
          place: 'Rend Lake, Illinois',
        },
        {
          id: 6,
          title: 'Episode 6',
          place: 'Rend Lake, Illinois',
        },
        {
          id: 7,
          title: 'Episode 7',
          place: 'Rend Lake, Illinois',
        },
        {
          id: 8,
          title: 'Episode 8',
          place: 'Rend Lake, Illinois',
        },
        {
          id: 3,
          title: 'Episode 3',
          place: 'Rend Lake, Illinois',
        },
      ],
    },
    {
      id: 6,
      title: 'Season 6',
      episodes: [
        {
          id: 1,
          title: 'Episode 1',
          place: 'Rend Lake, Illinois',
        },
        {
          id: 2,
          title: 'Episode 2',
          place: 'Rend Lake, Illinois',
        },
        {
          id: 3,
          title: 'Episode 3',
          place: 'Rend Lake, Illinois',
        },
        {
          id: 4,
          title: 'Episode 4',
          place: 'Rend Lake, Illinois',
        },
        {
          id: 5,
          title: 'Episode 5',
          place: 'Rend Lake, Illinois',
        },
        {
          id: 6,
          title: 'Episode 6',
          place: 'Rend Lake, Illinois',
        },
        {
          id: 7,
          title: 'Episode 7',
          place: 'Rend Lake, Illinois',
        },
        {
          id: 8,
          title: 'Episode 8',
          place: 'Rend Lake, Illinois',
        },
        {
          id: 3,
          title: 'Episode 3',
          place: 'Rend Lake, Illinois',
        },
      ],
    },
    {
      id: 5,
      title: 'Season 5',
      episodes: [
        {
          id: 1,
          title: 'Episode 1',
          place: 'Rend Lake, Illinois',
        },
        {
          id: 2,
          title: 'Episode 2',
          place: 'Rend Lake, Illinois',
        },
        {
          id: 3,
          title: 'Episode 3',
          place: 'Rend Lake, Illinois',
        },
        {
          id: 4,
          title: 'Episode 4',
          place: 'Rend Lake, Illinois',
        },
        {
          id: 5,
          title: 'Episode 5',
          place: 'Rend Lake, Illinois',
        },
        {
          id: 6,
          title: 'Episode 6',
          place: 'Rend Lake, Illinois',
        },
        {
          id: 7,
          title: 'Episode 7',
          place: 'Rend Lake, Illinois',
        },
        {
          id: 8,
          title: 'Episode 8',
          place: 'Rend Lake, Illinois',
        },
        {
          id: 3,
          title: 'Episode 3',
          place: 'Rend Lake, Illinois',
        },
      ],
    },
    {
      id: 4,
      title: 'Season 3',
      episodes: [
        {
          id: 1,
          title: 'Episode 1',
          place: 'Rend Lake, Illinois',
        },
        {
          id: 2,
          title: 'Episode 2',
          place: 'Rend Lake, Illinois',
        },
        {
          id: 3,
          title: 'Episode 3',
          place: 'Rend Lake, Illinois',
        },
        {
          id: 4,
          title: 'Episode 4',
          place: 'Rend Lake, Illinois',
        },
        {
          id: 5,
          title: 'Episode 5',
          place: 'Rend Lake, Illinois',
        },
        {
          id: 6,
          title: 'Episode 6',
          place: 'Rend Lake, Illinois',
        },
        {
          id: 7,
          title: 'Episode 7',
          place: 'Rend Lake, Illinois',
        },
        {
          id: 8,
          title: 'Episode 8',
          place: 'Rend Lake, Illinois',
        },
        {
          id: 3,
          title: 'Episode 3',
          place: 'Rend Lake, Illinois',
        },
      ],
    },
    {
      id: 2,
      title: 'Season 2',
      episodes: [
        {
          id: 1,
          title: 'Episode 1',
          place: 'Rend Lake, Illinois',
        },
        {
          id: 2,
          title: 'Episode 2',
          place: 'Rend Lake, Illinois',
        },
        {
          id: 3,
          title: 'Episode 3',
          place: 'Rend Lake, Illinois',
        },
        {
          id: 4,
          title: 'Episode 4',
          place: 'Rend Lake, Illinois',
        },
        {
          id: 5,
          title: 'Episode 5',
          place: 'Rend Lake, Illinois',
        },
        {
          id: 6,
          title: 'Episode 6',
          place: 'Rend Lake, Illinois',
        },
        {
          id: 7,
          title: 'Episode 7',
          place: 'Rend Lake, Illinois',
        },
        {
          id: 8,
          title: 'Episode 8',
          place: 'Rend Lake, Illinois',
        },
        {
          id: 3,
          title: 'Episode 3',
          place: 'Rend Lake, Illinois',
        },
      ],
    },
    {
      id: 1,
      title: 'Season 1',
      episodes: [
        {
          id: 1,
          title: 'Episode 1',
          place: 'Rend Lake, Illinois',
        },
        {
          id: 2,
          title: 'Episode 2',
          place: 'Rend Lake, Illinois',
        },
        {
          id: 3,
          title: 'Episode 3',
          place: 'Rend Lake, Illinois',
        },
        {
          id: 4,
          title: 'Episode 4',
          place: 'Rend Lake, Illinois',
        },
        {
          id: 5,
          title: 'Episode 5',
          place: 'Rend Lake, Illinois',
        },
        {
          id: 6,
          title: 'Episode 6',
          place: 'Rend Lake, Illinois',
        },
        {
          id: 7,
          title: 'Episode 7',
          place: 'Rend Lake, Illinois',
        },
        {
          id: 8,
          title: 'Episode 8',
          place: 'Rend Lake, Illinois',
        },
        {
          id: 3,
          title: 'Episode 3',
          place: 'Rend Lake, Illinois',
        },
      ],
    },
  ],
  last: {
    id: 2,
    title: 'Latest Episode • Season 7',
    place: 'Rend Lake, Illinois',
    episodes: [
      {
        id: 1,
        title: 'Episode 1',
        place: 'Rend Lake, Illinois',
      },
      {
        id: 2,
        title: 'Episode 2',
        place: 'Rend Lake, Illinois',
      },
      {
        id: 3,
        title: 'Episode 3',
        place: 'Rend Lake, Illinois',
      },
    ],
  },
};
