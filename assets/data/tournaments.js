export default {
  items: [
    {
      id: 7,
      title: 'Tournament 7',
      date: '01/10/2020',
      tags: ['Fish', 'Crappie', 'Lake'],
      image: 'https://picsum.photos/id/7/200',
      description:
        'A fishing tournament, or derby, is an organised competition among anglers. Fishing tournaments typically take place as a series of competitive events around or on a clearly defined body of water with specific rules applying to each event.',
    },
    {
      id: 6,
      title: 'Tournament 6',
      date: '01/10/2020',
      tags: ['Fish', 'Crappie', 'Lake'],
      image: 'https://picsum.photos/id/6/200',
      description:
        'A fishing tournament, or derby, is an organised competition among anglers. Fishing tournaments typically take place as a series of competitive events around or on a clearly defined body of water with specific rules applying to each event.',
    },
    {
      id: 5,
      title: 'Tournament 5',
      date: '01/10/2020',
      tags: ['Fish', 'Crappie', 'Lake'],
      image: 'https://picsum.photos/id/5/200',
      description:
        'A fishing tournament, or derby, is an organised competition among anglers. Fishing tournaments typically take place as a series of competitive events around or on a clearly defined body of water with specific rules applying to each event.',
    },
    {
      id: 4,
      title: 'Tournament 3',
      date: '01/10/2020',
      tags: ['Fish', 'Crappie', 'Lake'],
      image: 'https://picsum.photos/id/4/200',
      description:
        'A fishing tournament, or derby, is an organised competition among anglers. Fishing tournaments typically take place as a series of competitive events around or on a clearly defined body of water with specific rules applying to each event.',
    },
    {
      id: 3,
      title: 'Tournament 3',
      date: '01/10/2020',
      tags: ['Fish', 'Crappie', 'Lake'],
      image: 'https://picsum.photos/id/3/200',
      description:
        'A fishing tournament, or derby, is an organised competition among anglers. Fishing tournaments typically take place as a series of competitive events around or on a clearly defined body of water with specific rules applying to each event.',
    },
    {
      id: 2,
      title: 'Tournament 2',
      date: '01/10/2020',
      tags: ['Fish', 'Crappie', 'Lake'],
      image: 'https://picsum.photos/id/2/200',
      description:
        'A fishing tournament, or derby, is an organised competition among anglers. Fishing tournaments typically take place as a series of competitive events around or on a clearly defined body of water with specific rules applying to each event.',
    },
    {
      id: 1,
      title: 'Tournament 1',
      date: '01/10/2020',
      tags: ['Fish', 'Crappie', 'Lake'],
      image: 'https://picsum.photos/id/1/200',
      description:
        'A fishing tournament, or derby, is an organised competition among anglers. Fishing tournaments typically take place as a series of competitive events around or on a clearly defined body of water with specific rules applying to each event.',
    },
  ],
};
