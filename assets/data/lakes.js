export const lakes = [
  {
    id: 0,
    name: 'Lake Superior',
    lat: 47.68,
    long: -86.94,
  },
  {
    id: 1,
    name: 'Lake Huron',
    lat: 47.68,
    long: -86.94,
  },
  {
    id: 2,
    name: 'Lake Michigan',
    lat: 43.405457,
    long: -87.28693,
  },
  {
    id: 3,
    name: 'Lake Erie',
    lat: 42.040264,
    long: -81.341922,
  },
  {
    id: 4,
    name: 'Lake Ontario',
    lat: 47.68,
    long: -86.94,
  },
  {
    id: 5,
    name: 'Great Salt',
    lat: 43.570279,
    long: -77.845664,
  },
];
