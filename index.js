/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
import {slug as appName} from './app.json';

AppRegistry.registerComponent(appName, () => App);
