import moment from "moment";

const monthNames = [
  "January",
  "February",
  "March",
  "April",
  "May",
  "June",
  "July",
  "August",
  "September",
  "October",
  "November",
  "December",
];

export const convertLogs = (logs) => {
  const result = [];
  const newLogs = logs.map((log) => {
    return { ...log, date: moment(log.date) };
  });
  monthNames.forEach((monthName, index) => {
    const logsOfMonth = newLogs.filter((log) => log.date.month() === index);
    if (logsOfMonth.length > 0) {
      result.push({
        title: monthName,
        data: logsOfMonth,
        count: logsOfMonth.length,
      });
    }
  });

  return result.map((e, index) => {
    return { ...e, index: index };
  });
};
