import React from "react";
import { decode } from "html-entities";
import * as Calendar from 'expo-calendar';

export const getColorForTemp = (temp) => {
  let color = "#29A5C2";
  switch (true) {
    case temp > 20:
      color = "#FF6B0F";
      break;
    case temp > 10:
      color = "#49D1F1";
    case temp > 0:
      color = "#37C5D8";
    case temp > -10:
      color = "#29A5C2";
    default:
      break;
  }
  return color;
};

import {
  BottomRadial,
  DayCloud,
  DayCloudRain,
  DayCloudSnow,
  DayCloudSun,
  DayCloudThunder,
  DaySun,
} from "res/svgs";

export const getWeatherSymbol = (weather) => {
  switch (weather) {
    case "Clear":
      return <DaySun width="100%" height="100%" />;
    case "sun_cloud":
      return <DayCloudSun width="100%" height="100%" />;
    case "Clouds":
      return <DayCloud width="100%" height="100%" />;
    case "Rain":
      return <DayCloudRain width="100%" height="100%" />;
    case "Snow":
      return <DayCloudSnow width="100%" height="100%" />;
    case "Thunderstorm":
      return <DayCloudThunder width="100%" height="100%" />;
    default:
      return <DayCloud width="100%" height="100%" />;
  }
};

export const parseText = (text) => {
  return decode(text);
};

export const decodeHtmlCharCodes = (str) => {
  return str.replace(/(&#(\d+);)/g, function(match, capture, charCode) {
    return String.fromCharCode(charCode);
  });
}

export const dateStrings = (number) => {
  const dateString = ["", "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
  return dateString[number];
};

async function getDefaultCalendarSource() {
  const defaultCalendar = await Calendar.getDefaultCalendarAsync();
  return defaultCalendar.source;
}

export const getCalendars = async () => {
    const { status } = await Calendar.requestCalendarPermissionsAsync();
    if (status === 'granted') {
      const calendars = await Calendar.getCalendarsAsync(Calendar.EntityTypes.EVENT);
      return(calendars);
    }
};

export const getCalendarEvents = async (calendarIds, startDate, endDate) => {
  const events = Calendar.getEventsAsync(calendarIds, startDate, endDate);

  return events;
};

export async function createNewCalendar() {
  const defaultCalendarSource =
      Platform.OS === 'ios'
          ? await getDefaultCalendarSource()
          : { isLocalAccount: true, name: 'BrushPile Fishing' };
  const newCalendarID = await Calendar.createCalendarAsync({
    title: 'BrushPile Fishing',
    color: 'orange',
    entityType: Calendar.EntityTypes.EVENT,
    sourceId: defaultCalendarSource.id,
    source: defaultCalendarSource,
    name: 'BrushPile Fishing',
    ownerAccount: 'personal',
    accessLevel: Calendar.CalendarAccessLevel.OWNER,
  });

  return newCalendarID;
}

export const updateCalendar = async(eventId, details) => {

  const updatedEvent = await Calendar.updateCalendarAsync(eventId, details);
  return updatedEvent;
}

export const addToCalendar = async(calendarId, details) => {

  const createdEvent = await Calendar.createEventAsync(calendarId, details);
  return createdEvent;
}
