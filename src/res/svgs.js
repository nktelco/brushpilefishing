import DayCloud from './svgs/ic_day_cloud.svg';
import DaySun from './svgs/ic_day_sun.svg';
import DayCloudSun from './svgs/ic_day_cloud_sun.svg';
import DayCloudSnow from './svgs/ic_day_cloud_snow.svg';
import DayCloudThunder from './svgs/ic_day_cloud_thunder.svg';
import DayCloudRain from './svgs/ic_day_cloud_rain.svg';
import BottomRadial from './svgs/ic_weather_bottom_radial.svg';
import user from './svgs/user.svg';
import images from "./images";
export {
  DayCloud,
  DaySun,
  DayCloudSun,
  DayCloudSnow,
  DayCloudThunder,
  DayCloudRain,
  BottomRadial,
  user
};