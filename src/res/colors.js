export default colors = {
  orange: "#FF6C0E",
  darkBlue: "#021D49",
  primary: "#00C1DE",
  mainBack: "#EEF4F5",
  blackText: "#011E48",
  grayText: "#8C93A7",
  darkGray: "#CACDD6",
  lightText: "#535D7A",
  whiteText: "#EEF4F5",
  blueText: "#1A284F",
  orangeEnd: "#FCBFA5",
};

export const gradientColors = {
  black: "#020a1a", //0,1,2,3
  darkBlue: "#06388a", //4,5,6,7
  blue: "#0f3d99",
  lightBlue: "#0c70ba",
  paleBlue: `#188ee5`,
  almostWhite: `#469dee`,
  yellow: "#e9db51",
  darkOrange: "#e0813c", //4,5,6,7
  lightOrange: "#e3945d",
  violet: "#4115b5", //4,5,6,7
  darkViolet: `#330671`,
};

//this probably works for summer colors but we probably need a winter one
//build a time of day based on sunrise/sunset in the future
export const timeOfDayGradientColors = [
  gradientColors.black, //0-1
  gradientColors.darkBlue,

  gradientColors.black, //1-2
  gradientColors.darkBlue,

  gradientColors.darkBlue, //2-3
  gradientColors.darkViolet,

  gradientColors.darkBlue, //3-4
  gradientColors.darkViolet,

  gradientColors.darkViolet, //4-5
  gradientColors.darkOrange,

  gradientColors.darkViolet, //5-6
  gradientColors.darkOrange,

  gradientColors.lightOrange, //6-7
  gradientColors.darkOrange,

  gradientColors.lightOrange, //7-8
  gradientColors.darkOrange,

  gradientColors.lightOrange,
  gradientColors.yellow, //8-9

  gradientColors.paleBlue,
  gradientColors.yellow, //9-10

  gradientColors.paleBlue, //10-11
  gradientColors.darkBlue,

  gradientColors.lightBlue, //11-12
  gradientColors.paleBlue,

  gradientColors.paleBlue, //12-13
  gradientColors.almostWhite,

  gradientColors.paleBlue, //13-14
  gradientColors.almostWhite,

  gradientColors.paleBlue, //14-15
  gradientColors.lightBlue,

  gradientColors.paleBlue, //15-16
  gradientColors.lightBlue,

  gradientColors.paleBlue, //16-17
  gradientColors.darkBlue,

  gradientColors.darkBlue, //17-18
  gradientColors.lightBlue,

  gradientColors.darkOrange, //18-19
  gradientColors.darkViolet,

  gradientColors.darkOrange, //19-20
  gradientColors.darkViolet,

  gradientColors.darkViolet, //20-21
  gradientColors.darkBlue,

  gradientColors.darkViolet, //21-22
  gradientColors.darkBlue,

  gradientColors.darkViolet, //22-23
  gradientColors.black,

  gradientColors.darkViolet, //23-24
  gradientColors.black,
];
