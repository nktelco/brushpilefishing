const images = {
  logo: require("./images/Logo.png"),
  logo_circle: require("./images/ic_logo_circle.png"),
  viewMore: require("./images/ic_view_more.png"),
  playSmall: require("./images/ic_play_small.png"),
  empty_gray: require("./images/ic_empty_gray.png"),
  empty_black: require("./images/ic_empty_black.png"),
  edit: require("./images/ic_edit.png"),
  calendar_add: require("./images/ic_calendar_add.png"),
  back: require("./images/ic_back.png"),
  star: require("./images/ic_star.png"),
  arrow_right: require("./images/ic_arrow_right.png"),
  arrow_left: require("./images/ic_arrow_left.png"),
  thermometer_sun: require("./images/thermometer-sun.png"),
  warning: require("./images/ic_warning.png"),
  notes: require("./images/ic_notes.png"),
  home_tab: require("./images/home_tab_icon.png"),
  log_tab: require("./images/log_tab_icon.png"),
  show_tab: require("./images/show_tab_icon.png"),
  offer_tab: require("./images/offer_tab_icon.png"),
  menu_tab: require("./images/menu_tab_icon.png"),
  home_tab_active: require("./images/home_tab_active_icon.png"),
  log_tab_active: require("./images/log_tab_active_icon.png"),
  show_tab_active: require("./images/show_tab_active_icon.png"),
  offer_tab_active: require("./images/offer_tab_active_icon.png"),
  menu_tab_active: require("./images/menu_tab_active_icon.png"),
  home_header_title: require("./images/img_home_title.png"),
  home_video_preview: require("./images/video-preview.png"),
  ad_preview: require("./images/img_add_template.png"),
  ic_add: require("./images/ic_plus.png"),
  weather_sunny: require("./images/ic_log_sun_cloud_back.png"),
  weather_rainy: require("./images/ic_log_rainy_back.png"),
  three_dots: require("./images/ic_three_dots.png"),
  caught: require("./images/ic_caught.png"),
  video_loading: require("./images/video_loading.jpg"),
  profile_tournament: require("./images/ic_tournament.png"),
  profile_weather: require("./images/ic_profile_weather.png"),
  profile_crappie_info: require("./images/ic_profile_crappie_info.png"),
  profile_guide: require("./images/ic_profile_guide.png"),
  setting: require("./images/ic_setting.png"),
  photo: require("./images/ic_camera.png"),
  profile_edit: require("./images/ic_profile_edit.png"),
  profile_notification: require("./images/ic_profile_notification.png"),
  profile_privacy: require("./images/ic_profile_privacy.png"),
  profile_contact_us: require("./images/ic_profile_contact_us.png"),
  profile_shield: require("./images/ic_profile_shield.png"),
  xmark: require("./images/ic_search_cancel.png"),
  weather_sun_cloud: require("./images/img_weather_suncloud.png"),
  weather_wind_speed: require("./images/ic_weather_windspeed.png"),
  weather_humidity: require("./images/ic_weather_humidity.png"),
  weather_chance_of_rain: require("./images/ic_weather_chance_of_rain.png"),
  fishing_tips: require("./images/ic_fishing_tips.png"),
  minus: require("./images/ic_minus.png"),
  calendar: require("./images/la_calendar-solid.png"),
  bgAbout: require("./images/bg-about.png"),
  playbutton: require("./images/playbutton.png"),
  tournamentDefault: require("./images/lake-delaware.png"),
  user: require("./images/user.png"),
  log_lake: require("./images/log_lake.png"),
  play_circle: require("./images/play-circle.svg"),
};

export default images;
