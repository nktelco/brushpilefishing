import {Stack} from '@mobily/stacks';
import React from 'react';
import styled from 'styled-components';
import colors from 'res/colors';

const LiveButton = () => {
  return (
    <Container>
      <Stack space={10} horizontal align="center">
        <Circle color={ colors.orange}/>
        <Text>Live</Text>
      </Stack>
    </Container>
  );
};

export default LiveButton;

const Container = styled.TouchableOpacity`
  flex: 1;
  justify-content: center;
  align-items: center;
  padding: 10px 12px;
  background: rgba(255, 255, 255, 0.13);;
  border-radius: 20px;
`;
const Text = styled.Text`
  font-family: Roboto-Regular;
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  line-height: 16px;
  color: white;
`;

const Circle = styled.View`
  background: ${props => props.color};
  width: 12px;
  height: 12px;
  border-radius: 6px;
`;
