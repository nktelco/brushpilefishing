import React, {useEffect, useState} from 'react';
import {StyleSheet, View} from 'react-native';
import styled from 'styled-components';
import images from 'res/images';
import {LinearGradient} from 'expo-linear-gradient';
import colors from 'res/colors';
import {getGuideDetailByName, getBpfApiPath} from "../api/api";

const GuestAvatar = (guest) => {
    const [guestDetail, setGuestDetail] = useState();

    useEffect(() => {
        getGuideDetailByName(guest.guest).then((result) => {
            console.log("GOT BACK", result);
            if(result == "") {
                setGuestDetail(null);
            } else {
                setGuestDetail(result);
            }
        }).catch((e) => {
            console.log(e);
            setGuestDetail(null);
        })
    },[])

  return (
    <View>
      <LinearGradient
        colors={[colors.primary, colors.darkBlue]}
        style={styles.background}>
        <Avatar source={guestDetail == null || !guestDetail.image ? images.user : {uri: `${getBpfApiPath()}/${guestDetail.image}`}} />
      </LinearGradient>

      <STAR source={images.star}></STAR>
    </View>
  );
};

export default GuestAvatar;

const styles = StyleSheet.create({
  background: {
    width: 54,
    height: 54,
    borderRadius: 27,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

const Avatar = styled.Image`
  width: 48px;
  height: 48px;
  background-color: #c4c4c4;
  border-radius: 27px;
`;

const STAR = styled.Image`
  width: 18px;
  height: 18px;
  position: absolute;
  right: -2px;
  bottom: -3px;
`;
