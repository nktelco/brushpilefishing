import React from "react";
import { StyleSheet, Dimensions, View } from "react-native";
import Svg, { Defs, LinearGradient, Rect, Stop } from "react-native-svg";
const HeaderGradient = () => {
  const originalWidth = 375;
  const originalHeight = 264;
  const aspectRatio = originalWidth / originalHeight;
  const windowWidth = Dimensions.get("window").width;
  return (
    <View
      style={{
        width: windowWidth,
        aspectRatio,
        position: "absolute",
        bottom: 0,
      }}
    >
      <Svg
        width="100%"
        height="100%"
        viewBox={`0 0 ${originalWidth} ${originalHeight}`}
      >
        <Rect width="100%" height="100%" fill="url(#grad)" />
        <Defs>
          <LinearGradient
            id="grad"
            x1="187.5"
            y1="0"
            x2="187.5"
            y2="264"
            gradientUnits="userSpaceOnUse"
          >
            <Stop stopColor="#021D49" stopOpacity="0" />
            <Stop offset="1" stopColor="#021D49" />
          </LinearGradient>
        </Defs>
      </Svg>
    </View>
  );
};

export default HeaderGradient;

const styles = StyleSheet.create({});
