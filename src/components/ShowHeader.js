import {Stack, Box} from '@mobily/stacks';
import React from 'react';
import {StyleSheet} from 'react-native';
import styled from 'styled-components';
import colors from 'res/colors';
import LiveButton from './LiveButton';
const ShowHeader = ({textColor = 'black'}) => {
  return (
    <Container color={colors.darkBlue}>
      <Box style={{position: 'absolute', top: -10}}>
        <LiveButton></LiveButton>
      </Box>
      <Stack align="center" space={8}>
        <Text color={colors.orange}>Pickin’ Crappie</Text>
        <Stack align="center" space={8}>
          <Title>BrushPile Fishing: Full Episode</Title>
          <GrayText>Season 3 • Episode 12</GrayText>
        </Stack>
      </Stack>
    </Container>
  );
};

export default ShowHeader;

const styles = StyleSheet.create({});

const Container = styled.View`
  flex: 1;
  justify-content: center;
  align-items: center;
  padding: 46px 20px 38px 20px;
  background-color: ${props => props.color};
`;
const Title = styled.Text`
  font-family: Roboto-Bold;
  font-style: normal;
  font-weight: normal;
  font-size: 22px;
  line-height: 26px;
  text-align: center;
  color: #eef4f5;
`;
const Text = styled.Text`
  font-family: Roboto-Bold;
  font-style: normal;
  font-weight: bold;
  font-size: 17px;
  line-height: 17px;
  color: ${props => props.color ?? '#000000'};
`;

const GrayText = styled.Text`
  font-family: Roboto-Bold;
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  line-height: 16px;
  color: #eef4f5;
`;
