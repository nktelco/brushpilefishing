import {Ionicons} from '@expo/vector-icons';
import {Box, Stack} from '@mobily/stacks';
import React from 'react';
import {Image, StyleSheet, Text, TouchableWithoutFeedback} from 'react-native';
import images from 'res/images';
import styled from 'styled-components';

const ItemPicker = ({title, onPress}) => {
  return (
    <Stack space={13}>
      <Title>{title}</Title>
      <Border>
        <TouchableWithoutFeedback onPress={onPress}>
          <Box
            alignX="between"
            alignY="center"
            direction="row"
            paddingLeft={23}
            paddingRight={18}>
            <Text>{'Select ' + title}</Text>
            {title === 'Weather' ? (
              <WeatherSymbol>
                <Image source={images.thermometer_sun} resizeMode="contain" />
              </WeatherSymbol>
            ) : (
              <Ionicons name="search-outline" size={26} color="black" />
            )}
          </Box>
        </TouchableWithoutFeedback>
      </Border>
    </Stack>
  );
};

export default ItemPicker;

const styles = StyleSheet.create({});

const Title = styled.Text`
  font-family: Roboto-Regular;
  font-style: normal;
  font-weight: normal;
  font-size: 16px;
  line-height: 19px;
  color: #000000;
`;

const Border = styled.View`
  background: #ffffff;
  border: 1px solid #000000;
  height: 48px;
  justify-content: center;
`;

const WeatherSymbol = styled.View`
  background: #c4c4c4;
  border-radius: 20px;
  width: 43px;
  height: 35px;
  justify-content: center;
  align-items: center;
`;
