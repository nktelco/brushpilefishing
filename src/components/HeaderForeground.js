import React from "react";
import { Image, StyleSheet, View } from "react-native";
import images from "res/images";
import HeaderGradient from "components/HeaderGradient";
const HeaderForeground = () => {
  return (
    <View style={styles.container}>
      <HeaderGradient />
      <Image source={images.logo_circle} style={styles.logo} />
    </View>
  );
};

export default HeaderForeground;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
  },
  logo: {
    position: "absolute",
    bottom: 34,
    width: 75,
    height: 70,
  },
});
