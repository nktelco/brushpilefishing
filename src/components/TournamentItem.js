import {Box, Inline, Stack} from '@mobily/stacks';
import React from 'react';
import {
  Image,
  StyleSheet,
  TouchableOpacity,
  TouchableWithoutFeedback,
  View,
} from 'react-native';
import {Chip} from 'react-native-paper';
import images from 'res/images';
import styled from 'styled-components';
import colors from '../res/colors';
import { dateStrings } from '../helpers/helpers';

const TournamentItem = ({item, onPress, hideImage}) => {

  const startArr = ((item.start_date.split(" ")[0].split("-")));
  const endArr = ((item.end_date.split(" ")[0].split("-")));

  const start_date = `${dateStrings(Number(startArr[1]))} ${startArr[2]},${startArr[0]}`;
  const end_date = `${dateStrings(Number(startArr[1]))} ${endArr[2]},${endArr[0]}`;

    let asURL = item.images[0]=="String";


    return (
    <TouchableWithoutFeedback onPress={onPress}>
        <Container style={ hideImage && {width: null}}>
            { !hideImage &&
                <PreviewImage
                    source={asURL ? {uri: item.images[0]} : item.images[0]}
                />
            }

                {
                  item.organizer.map((o, index) => (
                      <Text key={index}>{o.organizer}</Text>
                  ))
                }
                <DateText>
                  {
                    start_date == end_date ?
                    `${start_date}` :
                    `${start_date} - ${end_date}`
                  }
                </DateText>

              <View
                  style={{
                    flex: 1,
                    width: "100%",
                  }}
                >
                  <Title numberOfLines={4}>
                    {item?.title}
                  </Title>
              </View>
              <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                      alignItems: 'center',
                      background: 'red',
                      flex: 0,
                      height: 30,
                      width: "100%",
                      marginTop: 10,
                      marginBottom: 10
                    }}>
                  <Inline space={7}>
                    {item.tags.map((item, index) => {
                      return (
                          <Chip
                              key={index.toString()}
                              mode="outlined"
                              style={styles.chip}
                              textStyle={styles.chipText}>
                            {item}
                          </Chip>
                      );
                    })}
                  </Inline>
                {
                  /*
                  <TouchableOpacity>
                    <Image source={images.calendar_add}></Image>
                  </TouchableOpacity>
                   */
                }
               </View>
        </Container>
    </TouchableWithoutFeedback>
  );
};

export default TournamentItem;

const Container = styled.View`
  background-color: white;
  width: 285px;
  border-radius: 16px;
  padding: 10px;
`;

const PreviewImage = styled.Image`
  /* background: rgba(196, 196, 196, 0.29); */
  width: 100%;
  height: 153px;
  border-radius: 8px;
  resizeMode: cover;
  overflow: hidden;
  flex: 0;
`;

const Text = styled.Text`
  font-family: Roboto-Bold;
  font-style: normal;
  font-weight: normal;
  font-size: 15px;
  color: ${colors.orange};
  margin: 4px 0;
`;

const Title = styled.Text`
  font-family: Roboto-Bold;
  font-style: normal;
  font-weight: bold;
  font-size: 20px;
  color: ${colors.blackText};
  margin: 2px 0;
`;

const DateText = styled.Text`
  font-family: Lato_400Regular;
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  color: ${colors.lightText};
  margin: 6px 0;
`;

const styles = StyleSheet.create({
  chipText: {
    fontFamily: 'Lato_400Regular',
    fontSize: 13,
    color: colors.lightText,
  },
  chip: {
    borderColor: colors.lightText,
  },
});
