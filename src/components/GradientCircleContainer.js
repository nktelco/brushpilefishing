import React, {Children} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {LinearGradient} from 'expo-linear-gradient';
const GradientCircleContainer = props => {
  return (
    <LinearGradient
      colors={[colors.primary, colors.darkBlue]}
      style={[
        styles.background,
        props.size && {
          width: props.size,
          height: props.size,
          borderRadius: props.size / 2,
        },
      ]}>
      {props.children}
    </LinearGradient>
  );
};

export default GradientCircleContainer;

const styles = StyleSheet.create({
  background: {
    width: 54,
    height: 54,
    borderRadius: 27,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
