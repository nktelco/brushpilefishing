import {Stack} from '@mobily/stacks';
import {useNavigation} from '@react-navigation/native';
import React from 'react';
import images from 'res/images';
import styled from 'styled-components';
import colors from 'res/colors';
const ViewMore = ({season, onPress}) => {
  const navigation = useNavigation();
  return (
    <Container
      onPress={() => {
        navigation.navigate('SeasonDetail', {season: season});
      }}>
      <Stack space={11} align="center">
        <Image source={images.viewMore} style={{width: 48, height: 48}} />
        <GrayText color={colors.grayText}>View More</GrayText>
      </Stack>
    </Container>
  );
};

export default ViewMore;

const Container = styled.TouchableOpacity`
  flex: 1;
  justify-content: center;
  align-items: center;
`;

const Text = styled.Text`
  font-family: Roboto-Regular;
  font-style: normal;
  font-weight: normal;
  font-size: 13px;
  line-height: 15px;
  color: white;
`;

const GrayText = styled.Text`
  font-family: Roboto-Regular;
  font-style: normal;
  font-weight: normal;
  font-size: 13px;
  line-height: 15px;
  color: ${props => props.color};
`;

const Image = styled.Image`
  width: 43px;
  height: 43px;
`;
