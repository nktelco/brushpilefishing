import React from 'react';
import {Image, StyleSheet, TouchableOpacity, View} from 'react-native';
import images from 'res/images';
import styled from 'styled-components';
import {LinearGradient} from 'expo-linear-gradient';
const WatchEpisodeButton = ({onPress}) => {
  return (
    <TouchableOpacity style={styles.container} onPress={onPress}>
      <Image source={ images.playbutton }/>
      {
        /*
        <LinearGradient
            colors={[colors.primary, 'white']}
            style={styles.background}>
          <View style={styles.buttonView}>
            <Text>Watch Episode</Text>
            <Image source={images.playSmall}/>
          </View>
        </LinearGradient>
         */
      }
    </TouchableOpacity>
  );
};

export default WatchEpisodeButton;

const Text = styled.Text`
  font-family: Roboto-Regular;
  font-style: normal;
  font-weight: normal;
  font-size: 18px;
  line-height: 18px;
  color: #000000;
  padding-right: 10px;
  padding-top: 4px;
`;

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'center',
    top: 15,
  },
  background: {
    borderRadius: 25,
    alignItems: 'center',
    justifyContent: 'center',
    padding: 5,
  },
  buttonView: {
    justifyContent: 'center',
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#d9d9d9',
    borderRadius: 25,
    padding: 9,
  }
});
