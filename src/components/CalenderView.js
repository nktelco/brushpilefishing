import React, {useState} from 'react';
import {StyleSheet, Image} from 'react-native';
import {Calendar, LocaleConfig} from 'react-native-calendars';
import colors from '../res/colors';
import images from 'res/images';
LocaleConfig.locales['en'] = {
  formatAccessibilityLabel: "dddd d 'of' MMMM 'of' yyyy",
  monthNames: [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December',
  ],
  monthNamesShort: [
    'jan',
    'feb',
    'mar',
    'apr',
    'may',
    'jun',
    'jul',
    'aug',
    'sep',
    'oct',
    'nov',
    'dec',
  ],
  dayNames: [
    'Sunday',
    'Monday',
    'Tuesday',
    'Wednesday',
    'Thursday',
    'Friday',
    'Saturday',
  ],
  dayNamesShort: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
};

LocaleConfig.defaultLocale = 'en';

const CalenderView = () => {
  const [selectedDay, setSelectedDay] = useState(null);

  const renderArrow = direction => {
    const image = direction === 'left' ? images.arrow_left : images.arrow_right;
    return <Image source={image} />;
  };

  return (
    <Calendar
      markedDates={{
        [selectedDay]: {
          selected: true,
        },
      }}
      theme={{
        backgroundColor: 'transparent',
        calendarBackground: 'transparent',
        textSectionTitleColor: colors.lightText,
        textSectionTitleDisabledColor: '#d9e1e8',
        selectedDayBackgroundColor: colors.orange,
        selectedDayTextColor: '#ffffff',
        todayTextColor: 'red',
        dayTextColor: colors.lightText,
        textDisabledColor: '#E0E0E0',
        dotColor: '#00adf5',
        selectedDotColor: '#ffffff',
        arrowColor: colors.primary,
        disabledArrowColor: '#A3A3A3',
        monthTextColor: '#000000',
        indicatorColor: 'blue',
        textDayFontFamily: 'Lato_400Regular',
        textMonthFontFamily: 'Lato_400Regular',
        textDayHeaderFontFamily: 'Lato_700Bold',
        textDayFontWeight: '400',
        textMonthFontWeight: '400',
        textDayHeaderFontWeight: '400',
        textDayFontSize: 14,
        textMonthFontSize: 16,
        textDayHeaderFontSize: 14,
        'stylesheet.calendar.header': {
          week: {
            flexDirection: 'row',
            justifyContent: 'space-between',
            backgroundColor: colors.mainBack,
            borderTopColor: '#E1E6EA',
            borderTopWidth: 2,
            paddingHorizontal: 16,
          },
          dayHeader: {
            marginTop: 15,
            marginBottom: 9,
          },
        },
        'stylesheet.calendar.main': {
          monthView: {
            backgroundColor: 'white',
            borderColor: '#E1E6EA',
            borderWidth: 2,
            borderRadius: 8,
          },
        },
      }}
      headerStyle={{
        borderTopLeftRadius: 8,
        borderTopRightRadius: 8,
        backgroundColor: 'white',
        marginHorizontal: 1,
      }}
      onDayPress={day => {
        console.log('selectedDay', day);
        setSelectedDay(day.dateString);
      }}
      renderArrow={renderArrow}
    />
  );
};

export default CalenderView;

const styles = StyleSheet.create({});
