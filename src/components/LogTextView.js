import {Stack} from '@mobily/stacks';
import React, {useState} from 'react';
import {Image, StyleSheet, TextInput} from 'react-native';
import images from 'res/images';
import styled from 'styled-components';

const LogTextView = ({title}) => {
  const [value, setValue] = useState(null);
  return (
    <Stack space={13}>
      <Title>{title}</Title>
      <Border>
        <TextInput
          multiline
          textAlignVertical="top"
          placeholder={'Type ' + title}
          style={{
            paddingLeft: 15,
            paddingRight: 42,
            paddingTop: 15,
            paddingBottom: 15,
            flex: 1,
          }}
          value={value}
        />
        {title === 'Caught' ? (
          <Image source={images.warning} style={styles.image} />
        ) : (
          <Image source={images.notes} style={styles.image} />
        )}
      </Border>
    </Stack>
  );
};

export default LogTextView;

const styles = StyleSheet.create({
  image: {
    position: 'absolute',
    top: 17,
    right: 15,
  },
});
const Title = styled.Text`
  font-family: Roboto-Regular;
  font-style: normal;
  font-weight: normal;
  font-size: 16px;
  line-height: 19px;
  color: #000000;
`;

const Border = styled.View`
  background: #ffffff;
  border: 1px solid #000000;
  height: 100px;
`;
