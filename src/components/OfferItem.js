import { Stack } from "@mobily/stacks";
import React from "react";
import {
  Image,
  StyleSheet,
  Text,
  TouchableWithoutFeedback,
  View,
  Linking,
} from "react-native";
import { parseText } from "../helpers/helpers";
import colors from "../res/colors";

const OfferItem = ({ item }) => {
  const image = item.images.length > 0 ? item.images[0].thumbnail : null;

  return (
    <TouchableWithoutFeedback
      onPress={() => {
        Linking.openURL(item.permalink);
      }}
    >
      <View style={styles.container}>
        <Image style={styles.image} source={{ uri: image }} />
        <View
          style={{
            flexDirection: "column",
            justifyContent: "flex-end",
            alignItems: "center",
            height: "100%",
            // backgroundColor: 'red',
          }}
        >
          <Stack space={14} align="center" paddingBottom={16}>
            <Text style={[styles.text, styles.title]}>
              {parseText(item.name)}
            </Text>
            <Text style={[styles.text, styles.offer]}>GET OFFER</Text>
          </Stack>
        </View>
      </View>
    </TouchableWithoutFeedback>
  );
};

export default OfferItem;

const styles = StyleSheet.create({
  container: {
    height: 255,
  },
  text: {
    color: "white",
    fontWeight: "bold",
  },
  title: {
    fontSize: 18,
  },
  offer: {
    fontSize: 13,
  },
  image: {
    height: 255,
    position: "absolute",
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    overflow: "hidden",
    borderRadius: 16,
    backgroundColor: colors.darkBlue,
  },
});
