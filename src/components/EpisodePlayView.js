import WatchEpisodeButton from "components/WatchEpisodeButton";
import {
  Audio,
  AVPlaybackStatus,
  Video,
  VideoFullscreenUpdateEvent,
} from "expo-av";
import { Asset } from "expo-asset";
import * as ScreenOrientation from "expo-screen-orientation";
import React, { useEffect, useRef, useState } from "react";
import {
  ActivityIndicator, Alert,
  Dimensions,
  Platform,
  StyleSheet,
} from "react-native";
import { useSafeAreaInsets } from "react-native-safe-area-context";
import { WebView } from "react-native-webview";
import styled from "styled-components";
import { backgroundColor } from "react-native-calendars/src/style";
const EpisodePlayView = ({ videoURL, posterImage, videoId }) => {
  const width = Dimensions.get("window").width;

  const inset = useSafeAreaInsets();
  const videoRef = useRef(null);

  const [isFullScreen, setIsFullScreen] = useState(false);
  const [isPresenting, setIsPresenting] = useState(false);
  const [isDismissing, setIsDismissing] = useState(false);
  const [isLoaded, setIsLoaded] = useState(false);
  const [videoAsset, setVideoAsset] = useState(null);

  const onFullscreenUpdate = async ({
    status,
    fullscreenUpdate,
  }) => {
    await Audio.setAudioModeAsync({
      playsInSilentModeIOS: true,
      interruptionModeIOS: Audio.INTERRUPTION_MODE_IOS_DO_NOT_MIX,
    });

    switch (fullscreenUpdate) {
      case Video.FULLSCREEN_UPDATE_PLAYER_DID_PRESENT:
        try {
          await ScreenOrientation.lockAsync(
            ScreenOrientation.OrientationLock.LANDSCAPE
          );
        } catch (error) {
          console.log(error);
        }
        if (status.isPlaying !== true) {
          await videoRef.current.playAsync();
        }
        setIsFullScreen(true);
        break;
      case Video.FULLSCREEN_UPDATE_PLAYER_DID_DISMISS:
        try {
          await ScreenOrientation.lockAsync(
            ScreenOrientation.OrientationLock.PORTRAIT_UP
          );
        } catch (error) {
          console.log(error);
        }

        if (status.isPlaying === true) {
          await videoRef.current.pauseAsync();
        }
        setIsFullScreen(false);
        break;
    }
  };

  const onPlaybackStatusUpdate = async (status: AVPlaybackStatus) => {
    if (status.isLoaded) {
      setIsLoaded(true);
    }

    if (status.isPlaying === true) {
      if (isFullScreen === false && isPresenting === false) {
        try {
          setIsPresenting(true);
          await videoRef.current.presentFullscreenPlayer();
          setIsPresenting(false);
        } catch (error) {
          console.log(error);
        }
      }
    }
  };

  const onTogglePlay = async () => {
    if (isFullScreen === false && isPresenting === false) {
      try {
        setIsPresenting(true);
        await videoRef.current.presentFullscreenPlayer();
        setIsPresenting(false);
      } catch (error) {
        console.log(error);
      }
    }
    videoRef.current.playAsync();
  };

  useEffect(() => {
    if (videoURL != null) {
      const videoAsset = new Asset({ uri: videoURL, type: "m3u8" });
      setVideoAsset(videoAsset);
    }
  }, [videoURL]);

  return (
    <Container>
      {videoURL == null ? (
        <Image source={{ uri: posterImage }} />
      ) : (
        <Video
          source={videoAsset}
          style={styles.video}
          shouldPlay={false}
          ref={videoRef}
          resizeMode="cover"
          isLooping={true}
          posterSource={{ uri: posterImage }}
          posterStyle={{ backgroundColor: "black" }}
          usePoster={true}
          onFullscreenUpdate={onFullscreenUpdate}
          onPlaybackStatusUpdate={onPlaybackStatusUpdate}
        />
      )}
      {isLoaded ? (
        <WatchEpisodeButton onPress={onTogglePlay} />
      ) : (
        <ActivityIndicator size="large" color={colors.darkBlue} />
      )}
    </Container>
  );
};

const styles = StyleSheet.create({
  video: {
    position: "absolute",
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    height: "100%",
    width: "100%",
  },
  loadingText: {
    fontSize: 20,
    color: colors.black,
  },
  loadingView: {
    backgroundColor: "white",
    padding: 4,
  },
});

export default EpisodePlayView;

const Container = styled.View`
  justify-content: center;
  align-items: center;
  background-color: #e2e2e2;
  aspect-ratio: 1.15;
`;

const Image = styled.Image`
  position: absolute;
  top: 0px;
  left: 0px;
  right: 0px;
  bottom: 0px;
  height: 100%;
  width: 100%;
`;

// const styles = StyleSheet.create({
//   banner: {
//     height: 285,
//   },
// });
