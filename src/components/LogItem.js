import { Box, Inline, Stack } from "@mobily/stacks";
import moment from "moment";
import React, { useState } from "react";
import { Image, StyleSheet, TouchableOpacity, View, Text } from "react-native";
import { Chip } from "react-native-paper";
import images from "res/images";
import styled from "styled-components";
import colors from "../res/colors";
const LogItem = ({ item, showMoreOptions }) => {
  const [showCaught, setShowCaught] = useState(false);
  const [showNote, setShowNote] = useState(false);
  return (
    <View style={styles.container}>
      <Stack space={8} marginTop={20} paddingLeft={20} paddingRight={20}>
        <Box direction="row" alignX="between" alignY="center">
          <GrayText>{moment(item.date).format("DD MMM, YYYY")}</GrayText>
          <TouchableOpacity onPress={showMoreOptions}>
            <Image source={images.three_dots} />
          </TouchableOpacity>
        </Box>
        <Box direction="row" alignX="between" alignY="center">
          <Title>{item.lake?.name}</Title>
            {
                /*
                <Image source={images.weather_sunny}/>
                */
            }
        </Box>
        <Inline space={7}>
          {item.items.map((item, index) => {
            return (
              <Chip
                key={index.toString()}
                mode="outlined"
                style={styles.chip}
                textStyle={styles.chipText}
              >
                {item.value}
              </Chip>
            );
          })}
        </Inline>
        <Box>
          <View
            style={{
              backgroundColor: "#E1E6EA",
              height: 1,
            }}
          >
            <View
              style={{
                height: 1,
                width: 24,
                backgroundColor: colors.orange,
              }}
            />
          </View>
          <TouchableOpacity onPress={() => setShowCaught(!showCaught)}>
            <View
              style={{
                justifyContent: "space-between",
                alignItems: "center",
                flexDirection: "row",
                marginTop: 8,
                marginBottom: 8,
              }}
            >
              <Box direction="row" alignY="center">
                <Image source={images.caught} style={{ marginRight: 8 }} />
                <Text style={[styles.text, styles.caughtText]}>Caught</Text>
              </Box>
              <Image
                source={images.arrow_right}
                style={{
                  transform: [{ rotate: showCaught ? "-90deg" : "0deg" }],
                }}
              />
            </View>
          </TouchableOpacity>
          {showCaught && <Text style={styles.longText}>{item.caught}</Text>}
          <View
            style={{
              backgroundColor: "#E1E6EA",
              height: 1,
            }}
          >
            <View
              style={{
                height: 1,
                width: 24,
                backgroundColor: colors.primary,
              }}
            />
          </View>
          <TouchableOpacity onPress={() => setShowNote(!showNote)}>
            <View
              style={{
                justifyContent: "space-between",
                alignItems: "center",
                flexDirection: "row",
                marginTop: 8,
                marginBottom: 8,
              }}
            >
              <Box direction="row" alignY="center">
                <Image source={images.notes} style={{ marginRight: 8 }} />
                <Text style={[styles.text, styles.notesText]}>Notes</Text>
              </Box>
              <Image
                source={images.arrow_right}
                style={{
                  transform: [{ rotate: showNote ? "-90deg" : "0deg" }],
                }}
              />
            </View>
          </TouchableOpacity>
          {showNote && <Text style={styles.longText}>{item.note}</Text>}
        </Box>
      </Stack>
    </View>
  );
};

export default LogItem;

const styles = StyleSheet.create({
  container: {
    backgroundColor: "white",
    marginBottom: 8,
    borderRadius: 8,
  },
  chipText: {
    fontFamily: "Lato_400Regular",
    fontSize: 13,
    color: colors.lightText,
  },
  chip: {
    borderColor: colors.lightText,
    marginVertical: 0,
  },
  text: {
    fontFamily: "Lato_400Regular",
    fontSize: 14,
  },
  caughtText: {
    color: colors.orange,
  },
  notesText: {
    color: colors.primary,
  },
  longText: {
    color: colors.blackText,
    fontFamily: "Lato_400Regular",
    fontSize: 14,
    paddingTop: 8,
    paddingBottom: 16,
  },
});

const Title = styled.Text`
  font-family: Roboto-Bold;
  font-style: normal;
  font-weight: normal;
  font-size: 24px;
  color: ${colors.blackText};
`;

const GrayText = styled.Text`
  font-family: Lato_400Regular;
  font-style: normal;
  font-weight: normal;
  font-size: 14px;

  color: ${colors.lightText};
`;

const Temperature = styled.Text`
  font-family: Roboto-Regular;
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  line-height: 16px;

  color: #000000;
`;
