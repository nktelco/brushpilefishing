import { Box, Stack } from "@mobily/stacks";
import React, { useState, useEffect } from "react";
import { ScrollView } from "react-native";
import styled from "styled-components";
import ViewMore from "./ViewMore";
import EpisodeItem from "./EpisodeItem";
import colors from "../res/colors";

const SeasonPreviewItem = ({ season }) => {
  const [episodes, setEpisodes] = useState([]);

  // useEffect(() => {
  //   loadEpisodesForSeason(season.id, 3).then(result => {
  //     console.log('season', result);
  //     setEpisodes(result);
  //   });
  // }, [season]);

  const renderItem = ({ item }) => {
    return <EpisodeItem />;
  };

  return (
    <Box
      style={{
        paddingTop: 11,
        paddingBottom: 25,
        backgroundColor: colors.darkBlue,
      }}
    >
      <Stack space={25}>
        <Title>{season.name}</Title>
        <ScrollView
          style={{ flexGrow: 1 }}
          contentContainerStyle={{
            paddingLeft: 25,
            paddingRight: 29,
          }}
          horizontal
          nestedScrollEnabled={true}
          showsHorizontalScrollIndicator={false}
        >
          <Stack space={48} horizontal>
            {season.episodes.map((episode, index) => (
              <EpisodeItem episode={episode} key={index} />
            ))}
            <ViewMore season={season} />
          </Stack>
        </ScrollView>
      </Stack>
    </Box>
  );
};

export default SeasonPreviewItem;

const Title = styled.Text`
  font-family: Roboto-Bold;
  font-style: normal;
  font-weight: bold;
  font-size: 14px;
  text-transform: uppercase;
  color: ${colors.mainBack};
  margin-left: 20px;
`;
