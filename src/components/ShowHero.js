import WatchEpisodeButton from 'components/WatchEpisodeButton';
import React from 'react';
import images from 'res/images';
import styled from 'styled-components';
import {LinearGradient} from 'expo-linear-gradient';
const ShowHero = () => {
  return (
    <Container>
      <BackgroundImage source={images.home_video_preview} />
      <LinearGradient
        colors={['transparent', colors.darkBlue]}
        style={{
          position: 'absolute',
          top: 150,
          right: 0,
          left: 0,
          bottom: 0,
        }}
      />
      <Logo source={images.logo_circle} />
    </Container>
  );
};

export default ShowHero;

const Container = styled.View`
  flex: 1;
  justify-content: center;
  align-items: center;
  height: 414px;
  width: 100%;
  /* background-color: #e2e2e2; */
`;

const Image = styled.Image`
  position: absolute;
  top: 0px;
  left: 0px;
  right: 0px;
  bottom: 0px;
`;

const Logo = styled.Image`
  position: absolute;
  bottom: 34px;
  width: 75px;
  height: 70px;
`;

const BackgroundImage = styled.Image`
  position: absolute;
  top: 0px;
  left: 0px;
  right: 0px;
  bottom: 0px;
  width: 100%;
`;

// const styles = StyleSheet.create({
//   banner: {
//     height: 285,
//   },
// });
