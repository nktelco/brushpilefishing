import { Box, Stack } from "@mobily/stacks";
import { useNavigation } from "@react-navigation/native";
import React from "react";
import { Image, StyleSheet, TouchableOpacity, View, Text } from "react-native";
import images from "res/images";
import { LinearGradient } from "expo-linear-gradient";
import colors from "res/colors";
const CrappieInfoCard = ({ guide }) => {
  const navigation = useNavigation();

  const onPress = () => {
    navigation.navigate("CrappieInfoPdf", { guide: guide.content.rendered });
  };

  //we're going to assume we only have one author
  const author = (guide._embedded.author[0].name);
  const avatarUrls = guide._embedded.author[0].avatar_urls;
  const urlKeys = Object.keys(avatarUrls);
  const authorAvatar = avatarUrls[urlKeys[0]];

  return (
    <TouchableOpacity onPress={onPress}>
      <View style={styles.container}>
        <View
          style={{ flexDirection: "row", flexShrink: 1, alignItems: "center" }}
        >
          <Image
            style={styles.thumbnail}
            source={{ uri: guide?.jetpack_featured_media_url }}
          />
          <View style={{ margin: 7, flexShrink: 1 }}>
            <View
              style={{
                flexDirection: "row",
                alignItems: "center",
                justifyContent: "space-between",
                marginBottom: 8,
              }}
            >
              <Text style={styles.score} numberOfLines={0}>
                {guide?.title?.rendered}
              </Text>
            </View>

            <View style={{ flexDirection: "row", alignItems: "center" }}>
              <LinearGradient
                colors={[colors.primary, "white"]}
                style={{
                  alignItems: "center",
                  justifyContent: "center",
                  width: 36,
                  height: 36,
                  borderRadius: 18,
                  marginRight: 7,
                }}
              >
                <Image
                  style={styles.avatar}
                  source={{ uri: authorAvatar }}
                />
              </LinearGradient>
              <Text style={styles.author}>{author}</Text>
            </View>
          </View>
        </View>

        <View
          style={{
            flexDirection: "row",
            alignItems: "center",
            flexGrow: 1,
            alignSelf: "flex-start",
            paddingTop: 7,
            paddingRight: 16,
            justifyContent: "flex-end",
          }}
        >
            {/*
                <Image source={images.star}/>
                <Text style={{marginLeft: 4}}>{"3/5"}</Text>
            */}
        </View>
      </View>
    </TouchableOpacity>
  );
};

export default CrappieInfoCard;

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    backgroundColor: "white",
    justifyContent: "space-between",
    width: "100%",
    flex: 1,
    borderRadius: 16,
  },

  text: {
    fontFamily: "HelveticaNeue-Bold",
    fontSize: 18,
    color: colors.blackText,
  },
  score: {
    fontFamily: "Lato_700Bold",
    fontSize: 14,
    color: "#535D7A",
  },
  author: {
    fontFamily: "Lato_400Regular",
    fontSize: 13,
    color: colors.grayText,
  },
  thumbnail: {
    width: 71,
    height: 74,
    borderRadius: 8,
    margin: 8,
  },
  avatar: {
    backgroundColor: "rgba(196, 196, 196, 0.5)",
    width: 32,
    height: 32,
    borderRadius: 16,
  },
});
