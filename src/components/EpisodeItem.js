import {Stack} from '@mobily/stacks';
import {useNavigation} from '@react-navigation/core';
import React from 'react';
import {StyleSheet, Text, TouchableOpacity, View, Image} from 'react-native';
import {LinearGradient} from 'expo-linear-gradient';
import colors from 'res/colors';

const EpisodeItem = ({episode}) => {
  const navigation = useNavigation();
  const onPress = () => {
    navigation.navigate('EpisodeDetail', {episode: episode.id});
  };

  const title = episode?.title.rendered.replace(/&#.*?;/g, '');
  const uri = episode?.jetpack_featured_media_url ?? 'https://fakeimg.pl/300/';
  // const title = 'title';
  return (
    <TouchableOpacity onPress={onPress}>
      <View style={styles.container}>
        <Stack space={8} align="center">
          <LinearGradient
            colors={[colors.primary, colors.darkBlue]}
            style={styles.gradient}>
            <Image style={styles.image} source={uri ? {uri: uri} : null} />
          </LinearGradient>

          <Stack space={5} align="center">
            <Text style={{...styles.title, color: 'white'}}>{title}</Text>
            {/*
            <Text style={styles.place}>{episode.place}</Text>
            <Text style={styles.title}>{episode.title}</Text>
            */}
          </Stack>
        </Stack>
      </View>
    </TouchableOpacity>
  );
};

export default EpisodeItem;

const styles = StyleSheet.create({
  container: {
    // flex: 1,
    alignItems: 'center',
  },
  gradient: {
    width: 110,
    height: 110,
    borderRadius: 55,
    justifyContent: 'center',
    alignItems: 'center',
  },
  image: {
    width: 100,
    height: 100,
    borderRadius: 50,
    backgroundColor: '#C4C4C4',
  },
  place: {
    fontFamily: 'Roboto-Regular',
    fontStyle: 'normal',
    fontSize: 13,
  },
  title: {
    fontFamily: 'Roboto-Regular',
    fontStyle: 'normal',
    fontSize: 15,
    width: 100,
  },
  circleView: {
    backgroundColor: '#3CBDD8',
    width: 115,
    height: 115,
    position: 'absolute',
    borderRadius: 100,
    marginTop: 0,
  },
  circleViewGradient: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    height: '100%',
  },
});
