import {Stack} from '@mobily/stacks';
import {useNavigation} from '@react-navigation/core';
import React from 'react';
import {Image, StyleSheet, TouchableOpacity, View} from 'react-native';
import styled from 'styled-components';
import {LinearGradient} from 'expo-linear-gradient';
import colors from 'res/colors';
const CircleItem = ({item}) => {
  const navigation = useNavigation();
  const onPress = () => {
    // navigation.navigate('EpisodeDetail', {episode: episode});
  };
  return (
    <TouchableOpacity onPress={onPress}>
      <Stack space={8} align="center">
        <LinearGradient
          colors={[colors.primary, 'white']}
          style={styles.background}>
          <Image
            style={styles.image}
            source={{uri: 'https://i.pravatar.cc/300'}}
          />
        </LinearGradient>

        <Stack space={5} align="center">
          <Title style={{color: colors.blackText}}>{item.title}</Title>
          <Body style={{color: colors.blackText}}>{item.place}</Body>
        </Stack>
      </Stack>
    </TouchableOpacity>
  );
};

export default CircleItem;

const styles = StyleSheet.create({
  container: {
    // flex: 1,
    alignItems: 'center',
  },
  image: {
    width: 99,
    height: 99,
    borderRadius: 49.5,
  },
  background: {
    width: 113,
    height: 113,
    borderRadius: 56.5,
    alignItems: 'center',
    justifyContent: 'center',
  },
  backgroundImage: {
    width: `100%`,
    height: `100%`,
    borderRadius: 61,
  },
});

const Title = styled.Text`
  font-family: Roboto-Regular;
  font-style: normal;
  font-weight: normal;
  font-size: 13px;
  line-height: 15px;
  color: #000000;
`;

const Body = styled.Text`
  font-family: Roboto-Regular;
  font-style: normal;
  font-weight: normal;
  font-size: 13px;
  line-height: 15px;
  color: rgba(0, 0, 0, 0.4);
`;
