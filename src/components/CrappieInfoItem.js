import {Box, Inline, Stack} from '@mobily/stacks';
import {useNavigation} from '@react-navigation/native';
import React from 'react';
import {StyleSheet, TouchableWithoutFeedback, View} from 'react-native';
import {Chip} from 'react-native-paper';
import styled from 'styled-components';
const CrappieInfoItem = ({item}) => {
  const navigation = useNavigation();
  const onPress = () => {
    navigation.navigate('CrappieInfoPdf');
  };
  return (
    <TouchableWithoutFeedback onPress={onPress}>
      <Container>
        <PreviewImage
          source={{
            uri: item.uri,
          }}
        />
        <Box
          paddingTop={32}
          paddingLeft={20}
          paddingBottom={12}
          paddingRight={14}>
          <Stack space={16}>
            <Stack space={12}>
              <Title>{item.text}</Title>
              <Date>01/10/2020</Date>
            </Stack>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
              }}>
              <Inline space={7}>
                {['Fish', 'Crappie', 'Chips'].map((item, index) => {
                  return (
                    <Chip
                      key={index.toString()}
                      mode="outlined"
                      style={styles.chip}
                      textStyle={styles.chipText}>
                      {item}
                    </Chip>
                  );
                })}
              </Inline>
            </View>
          </Stack>
        </Box>
      </Container>
    </TouchableWithoutFeedback>
  );
};

export default CrappieInfoItem;

const Container = styled.View`
  border: 1px solid rgba(0, 0, 0, 0.2);
`;

const PreviewImage = styled.Image`
  background: rgba(196, 196, 196, 0.29);
  height: 150px;
`;

const Text = styled.Text`
  font-family: Roboto-Regular;
  font-style: normal;
  font-weight: normal;
  font-size: 13px;
  line-height: 15px;
  color: #000000;
`;

const Title = styled.Text`
  font-family: Roboto-Bold;
  font-style: normal;
  font-weight: bold;
  font-size: 18px;
  line-height: 21px;
  color: #000000;
`;

const Date = styled.Text`
  font-family: Roboto-Regular;
  font-style: normal;
  font-weight: normal;
  font-size: 13px;
  line-height: 15px;
  color: rgba(0, 0, 0, 0.4);
`;

const styles = StyleSheet.create({
  chipText: {
    fontFamily: 'Roboto-Regular',
    fontSize: 13,
    color: '#000',
  },
  chip: {
    borderColor: '#000',
  },
});
