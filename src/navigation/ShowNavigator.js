import { createStackNavigator } from "@react-navigation/stack";
import React from "react";
import { Image, StyleSheet } from "react-native";
import { useSafeAreaInsets } from "react-native-safe-area-context";
import images from "res/images";
import { ShowScreen } from "../screens";
import ShowSearchScreen from "../screens/ShowSearch";

const Stack = createStackNavigator();

const ShowNavigator = () => {
  const inset = useSafeAreaInsets();

  const headerScreenOptions = () => ({
    headerStyle: {
      height: 56 + inset.top,
      backgroundColor: "white",
      borderBottomStartRadius: 16,
      borderBottomEndRadius: 16,
    },
    headerLeftContainerStyle: {
      paddingLeft: 8,
    },
    headerTitleStyle: {
      fontFamily: "Lato_400Regular",
      fontWeight: "400",
      fontSize: 21,
    },
    headerBackImage: ({ props }) => {
      return <Image source={images.back} {...props} />;
    },
    headerBackTitleVisible: false,
  });
  return (
    <Stack.Navigator screenOptions={headerScreenOptions}>
      <Stack.Screen
        name="Shows"
        component={ShowScreen}
        options={({ navigation, route }) => ({
          headerTransparent: "true",
        })}
      />
      <Stack.Screen
        name="ShowSearch"
        component={ShowSearchScreen}
        options={{ headerShown: false }}
      />
    </Stack.Navigator>
  );
};

export default ShowNavigator;

const styles = StyleSheet.create({});
