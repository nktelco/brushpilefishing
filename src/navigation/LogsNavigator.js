import {createStackNavigator} from '@react-navigation/stack';
import React from 'react';
import {Image, StyleSheet} from 'react-native';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import images from 'res/images';
import {LogsScreen} from '../screens';

const Stack = createStackNavigator();

const LogsNavigator = () => {
  const inset = useSafeAreaInsets();

  const headerScreenOptions = () => ({
    headerStyle: {
      height: 56 + inset.top,
      backgroundColor: 'white',
      borderBottomStartRadius: 16,
      borderBottomEndRadius: 16,
    },
    headerLeftContainerStyle: {
      paddingLeft: 20,
    },
    headerTitleStyle: {
      fontFamily: 'Lato_400Regular',
      fontWeight: '400',
      fontSize: 21,
    },
    headerBackImage: ({props}) => {
      return <Image source={images.back} {...props} />;
    },
    headerBackTitleVisible: false,
  });
  return (
    <Stack.Navigator screenOptions={headerScreenOptions}>
      <Stack.Screen name="Logs" component={LogsScreen} />
    </Stack.Navigator>
  );
};

export default LogsNavigator;

const styles = StyleSheet.create({});
