import { AntDesign } from "@expo/vector-icons";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import React from "react";
import images from "res/images";
import {CrappieInfoPdfView, TournamentScreen} from "../screens";
import { MainStackScreen } from "./MainStackScreen";

const Stack = createStackNavigator();

const AppNavigator = () => {
  const headerScreenOptions = () => ({
    headerStyle: {
      height: 56 + 44,
      backgroundColor: "white",
      borderBottomStartRadius: 16,
      borderBottomEndRadius: 16,
    },
    headerLeftContainerStyle: {
      paddingLeft: 20,
    },
    headerTitleStyle: {
      fontFamily: "Lato_400Regular",
      fontWeight: "400",
      fontSize: 21,
    },
    headerBackImage: ({ props }) => {
      return <Image source={images.back} {...props} />;
    },
    headerBackTitleVisible: false,
  });

  return (
    <NavigationContainer>
      <Stack.Navigator mode="modal" screenOptions={headerScreenOptions}>
        <Stack.Screen
          name="Main"
          component={MainStackScreen}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="CrappieInfoPdf"
          component={CrappieInfoPdfView}
          options={({ navigation, route }) => ({
            headerBackImage: ({ props }) => {
              return <AntDesign name="close" size={30} color="black" />;
            },
          })}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default AppNavigator;
