import { createStackNavigator } from "@react-navigation/stack";
import React from "react";
import { useSafeAreaInsets } from "react-native-safe-area-context";
import {
    EpisodeScreen,
    NewLogScreen,
    SeasonScreen,
    TournamentDetailScreen,
    TournamentScreen,
    CrappieInfoScreen,
    GuideScreen,
    LogsScreen
} from "../screens";
import TabNavigator from "./TabNavigator";
import { Image } from "react-native";
import images from "res/images";
import GuideDetailScreen from "../screens/GuideDetail";
import {AntDesign} from "@expo/vector-icons";

const MainStack = createStackNavigator();

export function MainStackScreen() {
  const inset = useSafeAreaInsets();

  const headerScreenOptions = () => ({
    headerStyle: {
      height: 56 + inset.top,
      backgroundColor: "white",
      borderBottomStartRadius: 16,
      borderBottomEndRadius: 16,
    },
    headerLeftContainerStyle: {
      paddingLeft: 20,
    },
    headerTitleStyle: {
      fontFamily: "Lato_400Regular",
      fontWeight: "400",
      fontSize: 21,
    },
    headerBackImage: ({ props }) => {
      return <Image source={images.back} {...props} />;
    },
    headerBackTitleVisible: false,
  });
  return (
    <MainStack.Navigator screenOptions={headerScreenOptions}>
      <MainStack.Screen
        name="Tab"
        component={TabNavigator}
        options={{ headerShown: false }}
      />
      <MainStack.Screen
        name="TournamentDetail"
        component={TournamentDetailScreen}
        options={({ navigation, route}) => ({
            headerBackImage: ({ props }) => {
                return <AntDesign name="close" size={30} color="black" />;
            },
        })}
      />

        <MainStack.Screen
            name="CrappieInfo"
            component={CrappieInfoScreen}
            options={({ navigation, route}) => ({
                headerBackImage: ({ props }) => {
                    return <AntDesign name="close" size={30} color="black" />;
                },
            })}
        />

        <MainStack.Screen
            name="LogsScreen"
            component={LogsScreen}
            options={({ navigation, route}) => ({
                headerBackImage: ({ props }) => {
                    return <AntDesign name="close" size={30} color="black" />;
                },
            })}
        />

      <MainStack.Screen
          name="Tournaments"
          component={TournamentScreen}
          options={({ navigation, route}) => ({
              headerBackImage: ({ props }) => {
                  return <Image source={images.back} {...props} />;
              },
          })}
        />

        <MainStack.Screen
            name="Guide"
            component={GuideScreen}
            options={({ navigation, route}) => ({
                headerBackImage: ({ props }) => {
                    return <Image source={images.back} {...props} />;
                },
            })}
        />

      <MainStack.Screen
        name="NewLog"
        component={NewLogScreen}
        options={headerScreenOptions}
      />
      <MainStack.Screen name="SeasonDetail" component={SeasonScreen} />
      <MainStack.Screen
        name="EpisodeDetail"
        component={EpisodeScreen}
        options={({ navigation, route }) => ({
          headerTransparent: "true",
          headerTitleStyle: {
            fontFamily: "Lato_400Regular",
            fontWeight: "400",
            fontSize: 21,
          },
          headerBackImage: ({ props }) => {
            return <Image source={images.back} {...props} />;
          },
        })}
      />
      <MainStack.Screen name="GuideDetail" component={GuideDetailScreen} />
    </MainStack.Navigator>
  );
}
