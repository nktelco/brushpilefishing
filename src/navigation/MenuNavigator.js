import { createStackNavigator } from "@react-navigation/stack";
import React from "react";
import { Image, Settings } from "react-native";
import { useSafeAreaInsets } from "react-native-safe-area-context";
import images from "res/images";
import GuideScreen from "../screens/Guide";
import {
  MenuScreen,
  SettingScreen,
  TournamentScreen,
  CrappieInfoScreen,
  CrappieInfoCategoryScreen,
  WeatherDetailScreen,
} from "../screens";

import WeatherScreen from "../screens/Weather";
import AboutScreen from "../screens/About";
import ProfileEditScreen from "../screens/ProfileEdit";
const Stack = createStackNavigator();
const headerOption = ({ navigation, route }) => ({
  headerStyle: {
    height: 84,
    backgroundColor: "#E2E2E2",
  },
  headerTitleStyle: {
    fontFamily: "Roboto-Regular",
    fontWeight: "400",
    fontSize: 24,
    lineHeight: 28,
  },
  headerLeft: () => {
    return null;
  },
});

const MenuNavigator = () => {
  const inset = useSafeAreaInsets();

  const headerScreenOptions = () => ({
    headerStyle: {
      height: 56 + inset.top,
      backgroundColor: "white",
      borderBottomStartRadius: 16,
      borderBottomEndRadius: 16,
    },
    headerLeftContainerStyle: {
      paddingLeft: 20,
    },
    headerTitleStyle: {
      fontFamily: "Lato_400Regular",
      fontWeight: "400",
      fontSize: 21,
    },
    headerBackImage: ({ props }) => {
      return <Image source={images.back} {...props} />;
    },
    headerBackTitleVisible: false,
  });

  return (
    <Stack.Navigator screenOptions={headerScreenOptions}>
      <Stack.Screen
        name="Menu"
        component={MenuScreen}
        options={{ headerShown: false }}
      />
      <Stack.Screen name="Tournaments" component={TournamentScreen} />
      <Stack.Screen
        name="Weather"
        component={WeatherScreen}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="CrappieInfo"
        component={CrappieInfoScreen}
        options={({ navigation, route }) => ({
          title: "Crappie info",
        })}
      />
      <Stack.Screen
        name="Guide"
        component={GuideScreen}
        options={({ navigation, route }) => ({
          title: "Find a Guide",
        })}
      />
      <Stack.Screen name="About" component={AboutScreen} />
      <Stack.Screen
        name="CrappieCategory"
        component={CrappieInfoCategoryScreen}
        options={({ navigation, route }) => ({
          title: "Crappie info",
        })}
      />
      <Stack.Screen
        name="Setting"
        component={SettingScreen}
        options={({ navigation, route }) => ({
          headerShown: false,
        })}
      />
      <Stack.Screen name="WeatherDetail" component={WeatherDetailScreen} />
      <Stack.Screen name="ProfileEdit" component={ProfileEditScreen} />
    </Stack.Navigator>
  );
};

export default MenuNavigator;
