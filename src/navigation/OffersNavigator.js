import { createStackNavigator } from "@react-navigation/stack";
import React from "react";
import { Image } from "react-native";
import { useSafeAreaInsets } from "react-native-safe-area-context";
import images from "res/images";
import OffersScreen from "../screens/Offer";
import OfferDetailScreen from "../screens/Offer/OfferDetailScreen";

const Stack = createStackNavigator();

const OffersNavigator = () => {
  const inset = useSafeAreaInsets();

  const headerScreenOptions = () => ({
    headerStyle: {
      height: 56 + inset.top,
      backgroundColor: "white",
      borderBottomStartRadius: 16,
      borderBottomEndRadius: 16,
    },
    headerLeftContainerStyle: {
      paddingLeft: 20,
    },
    headerTitleStyle: {
      fontFamily: "Lato_400Regular",
      fontWeight: "400",
      fontSize: 21,
    },
    headerBackImage: ({ props }) => {
      return <Image source={images.back} {...props} />;
    },
    headerBackTitleVisible: false,
  });

  return (
    <Stack.Navigator screenOptions={headerScreenOptions}>
      <Stack.Screen name="Offers" component={OffersScreen} />
      <Stack.Screen name="OfferCategory" component={OfferDetailScreen} />
    </Stack.Navigator>
  );
};
export default OffersNavigator;
