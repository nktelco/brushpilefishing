import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import React from 'react';
import {Image, StyleSheet} from 'react-native';
import images from 'res/images';
import colors from 'res/colors';
import {HomeScreen} from '../screens';
import LogsNavigator from './LogsNavigator';
import MenuNavigator from './MenuNavigator';
import OffersNavigator from './OffersNavigator';
import ShowNavigator from './ShowNavigator';

const Tab = createBottomTabNavigator();

const TabNavigator = () => {
  return (
    <Tab.Navigator
      screenOptions={({route}) => ({
        tabBarIcon: ({focused, color, size}) => {
          let image;
          switch (route.name) {
            case 'Home':
              image = focused ? images.home_tab_active : images.home_tab;
              break;
            case 'Logs':
              image = focused ? images.log_tab_active : images.log_tab;
              break;
            case 'Show':
              image = focused ? images.show_tab_active : images.show_tab;
              break;
            case 'Offers':
              image = focused ? images.offer_tab_active : images.offer_tab;
              break;
            case 'Menu':
              image = focused ? images.menu_tab_active : images.menu_tab;
              break;
          }

          return <Image source={image} />;
        },
      })}
      tabBarOptions={{
        activeTintColor: colors.blackText,
        inactiveTintColor: colors.grayText,
      }}>
      <Tab.Screen name="Home" component={HomeScreen} />
      <Tab.Screen name="Logs" component={LogsNavigator} />
      <Tab.Screen name="Show" component={ShowNavigator} />
      <Tab.Screen name="Offers" component={OffersNavigator} />
      <Tab.Screen name="Menu" component={MenuNavigator} />
    </Tab.Navigator>
  );
};

export default TabNavigator;

const styles = StyleSheet.create({});
