import React, { createContext, useEffect, useState } from "react";
import { log } from "react-native-reanimated";
import { db, getLogs } from "../api/db";

export const LogsContext = createContext();

const LogsContextProvider = (props) => {
  const [logs, setLogs] = useState([]);
  const [isRefreshing, setIsRefreshing] = useState(false);
  const reloadLogs = async () => {
    setIsRefreshing(true);
    console.log("refreshing");
    try {
      const result = await getLogs();

      setLogs(
        result.map((log) => {
          return {
            ...log,
            items: JSON.parse(log.items),
            lake: JSON.parse(log.lake),
          };
        })
      );
    } catch (error) {
      console.log(error);
    } finally {
      setIsRefreshing(false);
      console.log("refreshing end");
    }
  };
  useEffect(() => {
    reloadLogs();
  }, []);

  return (
    <LogsContext.Provider value={{ logs, reloadLogs, isRefreshing }}>
      {props.children}
    </LogsContext.Provider>
  );
};

export default LogsContextProvider;
