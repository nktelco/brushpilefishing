import React, { createContext, useReducer } from 'react';
import { newLogReducer } from '../reducer/NewLogReducer';


export const NewLogContext = createContext();

const NewLogContextProvider = (props) => {
  const [state, dispatch] = useReducer(newLogReducer, {items: [], note: null, caught: null, date: new Date(), lake: {}, id:null});
  const { items, lake, note, date, caught, id } = state
  return (
    <NewLogContext.Provider value={{ items, lake, note, date, caught, dispatch, id }}>
      {props.children}
    </NewLogContext.Provider>
  );
}
 
export default NewLogContextProvider;
