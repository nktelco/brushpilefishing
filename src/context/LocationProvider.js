import React, { createContext, useEffect, useState } from "react";
import * as Location from "expo-location";
import { lakes } from "../../assets/data/lakes";
import { getCurrentWeather, mockAllWeather } from "../api/weather";
import { useAppState } from '@react-native-community/hooks'
import moment from "moment";
export const LocationContext = createContext();

const LocationContextProvider = (props) => {
  const [location, setLocation] = useState({});
  const [isLoading, setIsLoading] = useState(false);
  const [refreshing, setRefreshing] = useState(false);
  const [lastUpdate, setLastUpdate] = useState(null);
  const [locationPermission, setLocationPermission] = useState(null);

  const [currentWeather, setCurrentWeather] = useState(mockAllWeather);
  const [loadedWeather, setLoadedWeather] = useState(false);
  const [loadedForecast, setLoadedForecast] = useState(false);

  const currentAppState = useAppState();

  const getWeatherData = async (forceRefresh = false, newLocation) => {
    if (shouldRefresh() === false && !forceRefresh) {
      return;
    }
    setRefreshing(true);
    try {
      const loc = location.lat ? location : newLocation;
      if(loc) {
        const weather = await getCurrentWeather(loc.lat, loc.long);
        setCurrentWeather(weather);
        setLoadedWeather(true);
      }
    } catch (e) {}
    setLastUpdate(moment());
    setRefreshing(false);
  };

  useEffect(() => {
    if(currentAppState == "active") {
      initialize();
    }
  }, [currentAppState])

  const shouldRefresh = () => {
    if (lastUpdate === null) {
      return true;
    } else {
      if (moment().diff(lastUpdate, "minutes") >= 10) {
        return true;
      }
    }
    return false;
  };

  const initialize = async () => {
    setIsLoading(true);

    let { status } = await Location.requestForegroundPermissionsAsync();
    if (status !== "granted") {
      console.log("Permission to access location was denied");
      setLocationPermission(false);
      setLoading(false);
      return;
    } else {
      setLocationPermission(true);
    }

    let location = await Location.getCurrentPositionAsync({});
    let address = await Location.reverseGeocodeAsync({
      latitude: location.coords.latitude,
      longitude: location.coords.longitude,
    });

    const loc = {
      lat: location.coords.latitude,
      long: location.coords.longitude,
      name: address[0].city,
    };

    setLocation(loc)
    await getWeatherData(true, loc);

    setIsLoading(false);
  };

  useEffect(() => {
    initialize();
  }, []);

  return (
    <LocationContext.Provider
      value={{
        location,
        isLoading,
        loadedWeather,
        loadedForecast,
        currentWeather,
        refreshing,
        lastUpdate,
        getWeatherData,
        setLocation,
        locationPermission,
      }}
    >
      {props.children}
    </LocationContext.Provider>
  );
};

export default LocationContextProvider;
