import axios from "axios";

const wp_json_api = "https://brushpilefishing.com/wp-json/wp/v2";
const api_host = "https://brushpilefishing.com/wp-json";
const bpfdev_api = "https://bpfdev.com";
const bpfprod_api = "https://bpfapi.com"

//we need to build this per env
const bpfApi = bpfprod_api;

export const getBpfApiPath = () => {
  return bpfApi;
}

export const getAllVideoCategories = () => {

  //this is temporarily fetching 100 categories
  return new Promise((resolve, reject) => {
    axios
      .get(`${wp_json_api}/categories?per_page=100`)
      .then((result) => {
        const categories = result.data;
        const videoCats = [];
        for (let r of categories) {
          if (r.name.startsWith("Season")) {
            videoCats.push({
              name: r.name,
              id: r.id,
            });
          }
        }

        resolve(videoCats.reverse());
      })
      .catch((e) => {
        reject(e);
      });
  });
};

export const getLatestTournaments = (number = 5) => {
  return getPageOfTournaments(number, 1);
}

export const getPageOfTournaments = (number = 5, page = 1) => {
  return new Promise((resolve, reject) => {
    axios.get(
        `${api_host}/tribe/events/v1/events?page=${page}&per_page=${number}`
    ).then((result) => {
      resolve(result.data)
    }).catch((e) => {
      console.log("* * * * * * * * * * * *", e);
      reject(e);
    })
  })
}

export const loadEpisodesForSeason = (seasonId, perPage) => {
  return new Promise((resolve, reject) => {
    const paramList = [
      "id",
      "title",
      "categories",
      "jetpack_featured_media_url",
      "meta_box",
    ];
    let params = "";
    for (let p of paramList) {
      params = `${params}&_fields[]=${p}`;
    }

    axios
      .get(
        `${wp_json_api}/posts?categories=${seasonId}&per_page=${perPage}${params}`
      )
      .then((result) => {
        resolve(result.data);
      })
      .catch((e) => {
        reject(e);
      });
  });
};

export const loadCrappieInfos = (perPage=5, page=1) => {
  return new Promise((resolve, reject) => {
    const paramList = [
      /*
      "id",
      "title",
      "categories",
      "jetpack_featured_media_url",
      "meta_box",
      "content",
      "author"
       */
    ];
    let params = "";
    for (let p of paramList) {
      params = `${params}&_fields[]=${p}`;
    }

    params =`${params}&_embed=author`;

    //TODO: HOW DID WE DETERMINE CATEGORY 432?!
    console.log(`${wp_json_api}/posts?categories=432&per_page=${perPage}&page=${page}${params}`);

    axios
      .get(`${wp_json_api}/posts?&categories=432&per_page=${perPage}&page=${page}${params}`)
      .then((result) => {
        resolve(result.data);
      })
      .catch((e) => {
        reject(e);
      });
  });
};

export const loadEpisodeById = (id) => {
  return new Promise((resolve, reject) => {
    axios
      .get(`${wp_json_api}/posts/${id}`)
      .then((result) => {
        resolve(result.data);
      })
      .catch((e) => {
        reject(e);
      });
  });
};

export const getEpisodeDetails = (videoId) => {
  return new Promise((resolve, reject) => {
    axios
      .get(`${bpfApi}/auth/videos/${videoId}`)
      .then((result) => {
        resolve(result.data);
      })
      .catch((e) => {
        reject(e);
      });
  });
};

export const getGuidesByLocation = (lat, lng, range) => {
  return new Promise((resolve, reject) => {
    axios
      .get(`${bpfApi}/guide/distance/${lat}/${lng}/${range}`)
      .then((result) => {
        resolve(result.data);
      })
      .catch((e) => {
        reject(e);
      });
  });
};

export const getGuideDetailByName = (name) => {
  console.log(name);
  return new Promise((resolve, reject) => {
    axios
        .get(`${bpfApi}/guide/name/${name}`)
        .then((result) => {
          resolve(result.data);
        })
        .catch((e) => {
          console.log("error");
          reject(e);
        });
  });
};

export const getNearestGuides = (lat, lng, limit, page) => {
  return new Promise((resolve, reject) => {
    axios
        .get(`${bpfApi}/guide/nearest/${lat}/${lng}?limit=${limit}&page=${page}`)
        .then((result) => {
          resolve(result.data);
        })
        .catch((e) => {
          reject(e);
        });
    });
}

export const getOffers = (numdays, limit, page) => {
  return new Promise((resolve, reject) => {
    // const paramList = ["id", "name", "images", "description"];
    // let params = "";
    // for (let p of paramList) {
    //   params = `${params}&_fields[]=${p}`;
    // }
    axios
      .get(`${bpfApi}/campaign/current/${numdays}?limit=${limit}&page=${page}`)
      .then((result) => {
        resolve(result.data);
      })
      .catch((e) => {
        reject(e);
      });
  });
};

export const getGuideAndFishingSpotsById = (id) => {
  return new Promise((resolve, reject) => {
    axios.get(`${bpfApi}/guide/${id}/fishingspot`).then((result) => {
      console.log(result.data);
      resolve(result.data);
    }).catch((e) => {
      console.log(e)
      reject(e)
    })
  })
}

export const searchForPlaces = (text) => {
  return new Promise((resolve, reject) => {
    axios.get(`${bpfApi}/place/${text}`).then((result) => {
      resolve(result.data);
    }).catch((e) => {
      reject(e);
    })
  })
}

export const getLocationDetails = (place_id) => {
  return new Promise((resolve, reject) => {
    axios.get(`${bpfApi}/place/id/${place_id}`).then((result) => {
      resolve(result.data);
    }).catch((e) => {
      reject(e);
    })
  })
}
