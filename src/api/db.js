import * as SQLite from "expo-sqlite";

export function openDatabase() {
  if (Platform.OS === "web") {
    return {
      transaction: () => {
        return {
          executeSql: () => {},
        };
      },
    };
  }

  const db = SQLite.openDatabase("db.db");
  return db;
}

export const db = openDatabase();

async function performQuery(query, params) {
  return new Promise((resolve, reject) =>
    db.transaction((tx) => {
      tx.executeSql(
        query,
        params,
        (_, { rows }) => resolve(rows._array),
        reject
      );
    })
  );
}

export function createDB() {
  console.log("create");
  db.transaction((tx) => {
    tx.executeSql(
      "create table if not exists logs (id integer primary key not null, items text, lake text, note text, caught text, date text);",
      [],
      () => {},
      (_, error) => {
        console.log(error);
      }
    );
  });
}

export const addLog = async (log) => {
  const { items, lake, note, caught, date } = log;
  return new Promise((resolve, reject) =>
    db.transaction((tx) => {
      tx.executeSql(
        "insert into logs (items, lake, note, caught, date) values (?, ?, ?, ?, ?)",
        [
          JSON.stringify(items),
          JSON.stringify(lake),
          note,
          caught,
          date.toISOString(),
        ],
        (_, { rows }) => resolve(rows._array),
          (_, error) => {
              reject(error)
          }
      );
    })
  );
};

export const updateLog = async(log) => {
    const { items, lake, note, caught, date, id } = log;
    return new Promise((resolve, reject) => {
        db.transaction((tx) => {
            tx.executeSql(
                "update logs set items=?, lake=?, note=?, caught=?, date=? WHERE id=?",
                [
                    JSON.stringify(items),
                    JSON.stringify(lake),
                    note,
                    caught,
                    date.toISOString(),
                    id
                ],
                () => { resolve() },
            (err) => { reject(err) }
            )
        })
    })
}

export const getLogs = async () => {
  return new Promise((resolve, reject) =>
    db.transaction((tx) => {
      tx.executeSql(
        `select * from logs`,
        [],
        (_, { rows }) => resolve(rows._array),
        reject
      );
    })
  );
};

export const getLog = async (id) => {
    return new Promise((resolve, reject) => {
        db.transaction((tx) => {
            tx.executeSql(
                `select * from logs where id=?`,
                [
                    id
                ],
                (_, {rows}) => resolve(rows._array),
                (_, error) => {
                    reject(error)
                }
            )
        });
    })
}

export const getPreviousEquipment = async () => {
    return new Promise ((resolve, reject) => {
        db.transaction((tx) => {
            tx.executeSql(
                'select items from logs LIMIT 100',
                [],
                (_, {rows}) => resolve(rows._array),
                (_, error) => {
                    reject(error)
                }
            )
        });
    })
}

export const deleteLog = async (id) => {
  await performQuery(`delete from logs where id = ?;`, [id]);
};
