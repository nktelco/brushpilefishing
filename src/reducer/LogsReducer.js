import uuid from "react-native-uuid";

export const logsReducer = (state, action) => {
  switch (action.type) {
    case "ADD_LOG":
      return [...state, action.log];
    case "DELETE_LOG":
      return state.filter((item) => item.id !== action.id);
    case "UPDATE_LOG":
      return state.map((item) =>
        item.id === action.log.id ? action.item : item
      );
    case "REFRESH":
      return [];
    default:
      throw new Error();
  }
};
