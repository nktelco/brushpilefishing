import uuid from "react-native-uuid";

export const newLogReducer = (state, action) => {
  switch (action.type) {
    case "ADD_ITEM":
      return {
        ...state,
        items: [...state.items, { id: uuid.v4(), value: "" }],
      };
    case "ADD_ITEM_OF":
      return {
        ...state,
        items: [...state.items, { id: uuid.v4(), value: action.item }],
      };
    case "DELETE_ITEM":
      return {
        ...state,
        items: state.items.filter((item) => item.id !== action.id),
      };
    case "UPDATE_ITEM":
      console.log("update -->", action.item);
      return {
        ...state,
        items: state.items.map((item) =>
          item.id === action.item.id ? action.item : item
        ),
      };
    case "SELECT_DATE":
      return { ...state, date: action.date };
    case "SET_CAUGHT":
      return { ...state, caught: action.caught };
    case "SET_NOTE":
      return { ...state, note: action.note };
    case "SELECT_LAKE":
      return { ...state, lake: action.lake };
    case "RESET":
      return {
        items: [],
        note: null,
        caught: null,
        date: new Date(),
        lake: null,
        id: null
      };
    case "SET_LOG_DATA":
      return {
        items: JSON.parse(action.items),
        note: action.note,
        caught: action.caught,
        date: new Date(action.date),
        lake: JSON.parse(action.lake),
        id: action.id
      };
    default:
      throw new Error();
  }
};
