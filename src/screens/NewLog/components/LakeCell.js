import React, { useContext } from "react";
import { StyleSheet, Text, View, Image, TouchableOpacity } from "react-native";
import colors from "res/colors";
import images from "res/images";
import { AntDesign } from "@expo/vector-icons";
import { NewLogContext } from "../../../context/NewLogContext";

const LakeCell = ({ lake }) => {
  const { lake: selectedLake, dispatch } = useContext(NewLogContext);

  const selected = lake.name === selectedLake?.name;

  const selectLake = () => {
    dispatch({ type: "SELECT_LAKE", lake: lake });
  };

  return (
    <TouchableOpacity onPress={selectLake}>
      <View style={styles.container}>
        <Text style={styles.text}>{lake.name}</Text>
        {
          /*
          <Image source={images.weather_sunny}/>
        {selected ? (
          <AntDesign name="check" size={24} color={colors.primary} />
          ) : (
          <View style={{width: 24, height: 24}} />
          )}
        */
        }
      </View>
    </TouchableOpacity>
  );
};

export default LakeCell;

const styles = StyleSheet.create({
  container: {
    height: 50,
    paddingHorizontal: 20,
    flexDirection: "row",
    alignItems: "center",
  },
  text: {
    fontFamily: "Lato_400Regular",
    fontSize: 16,
    color: colors.blackText,
    flexGrow: 1,
  },
  weather: {
    height: 32,
    width: 64,
  },
});
