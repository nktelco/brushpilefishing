import React, { useContext } from "react";
import {
  Image,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  View,
} from "react-native";
import colors from "res/colors";
import { NewLogContext } from "../../../context/NewLogContext";
import images from "../../../res/images";

const EquipmentItem = ({ item }) => {
  const { id, value } = item;

  const { dispatch } = useContext(NewLogContext);
  const updateItem = (text) => {
    dispatch({ type: "UPDATE_ITEM", item: { id: id, value: text } });
  };
  const deleteItem = () => {
    dispatch({ type: "DELETE_ITEM", id: id });
  };
  return (
    <View style={styles.container}>
      <View style={styles.textInputContainer}>
        <TextInput
          defaultValue=""
          placeholder="Type your equipment name"
          value={value}
          style={styles.input}
          onChangeText={updateItem}
        />
      </View>
      <TouchableOpacity onPress={deleteItem}>
        <View style={styles.buttonContainer}>
          <Image source={images.minus} />
        </View>
      </TouchableOpacity>
    </View>
  );
};

export default EquipmentItem;

const styles = StyleSheet.create({
  container: {
    height: 48,
    marginVertical: 4,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "flex-start",
  },
  textInputContainer: {
    height: 48,
    flexGrow: 1,
    backgroundColor: colors.mainBack,
    borderRadius: 8,
    justifyContent: "center",
    paddingRight: 16,
  },
  input: {
    paddingHorizontal: 16,
    color: colors.grayColor,
  },
  buttonContainer: {
    marginLeft: 16,
  },
  minus: {
    width: 24,
    height: 24,
  },
});
