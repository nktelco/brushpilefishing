import {Stack} from '@mobily/stacks';
import React, { useContext} from 'react';
import {StyleSheet, Text, View, ScrollView, Image, TouchableOpacity} from 'react-native';
import colors from 'res/colors';
import images from 'res/images';
import { NewLogContext } from '../../../context/NewLogContext';

const AddEquipment = ({itemList}) => {
  const {dispatch} = useContext(NewLogContext)
  return (
      <>
        <TouchableOpacity onPress={() => {dispatch({type: 'ADD_ITEM'})}}>
          <View style={styles.container}>
              <View>
                <Stack space={8} horizontal align="center">
                  <Image source={images.ic_add} />
                  <Text style={styles.text}>Add More Equipments</Text>
                </Stack>
              </View>
          </View>
        </TouchableOpacity>
        <View style={styles.previousContainer}>
          <Text style={styles.previousTitle}>Previous Equipment</Text>
          <ScrollView style={styles.previous}>
          {
            (itemList || []).map((item) => (
                <TouchableOpacity onPress={() => {console.log("TOUCH"); dispatch({type: 'ADD_ITEM_OF', item: item})}}>
                  <View style={styles.selectItemContainer}>
                    <Image source={images.ic_add} />
                    <Text style={styles.selectItem}>{item}</Text>
                  </View>
                </TouchableOpacity>
            ))
          }
          </ScrollView>
        </View>
      </>
  );
};

export default AddEquipment;

const styles = StyleSheet.create({
  container: {
    height: 48,
    borderStyle: 'dashed',
    borderColor: colors.primary,
    borderWidth: 1,
    borderRadius: 8,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  previousContainer: {
    marginTop: 20,
    flex: 1,
    minHeight: 250
  },
  previousTitle: {
    fontSize: 16,
    fontWeight: "bold"
  },
  previous: {
    borderStyle: 'dashed',
    borderColor: colors.primary,
    borderWidth: 1,
    padding:10,
    height: 200
  },
  selectItemContainer: {
    flexDirection: 'row',
    marginTop: 5
  },
  selectItem: {
    fontSize: 17,
    marginLeft: 10,
  },
  text: {
    color: colors.primary,
    fontFamily: 'Lato_400Regular',
    fontSize: 14,
  },
});
