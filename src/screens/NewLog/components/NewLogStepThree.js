import React, { useState, useContext } from "react";
import {
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Image,
  TextInput,
  KeyboardAvoidingView,
  Platform,
  Keyboard,
} from "react-native";
import colors from "res/colors";
import { Stack } from "@mobily/stacks";
import images from "../../../res/images";
import DateTimePickerModal from "react-native-modal-datetime-picker";
import moment from "moment";
import { NewLogContext } from "../../../context/NewLogContext";

const NewLogStepThree = () => {
  const [show, setShow] = useState(false);

  const { date, caught, note, dispatch } = useContext(NewLogContext);

  const updateNote = (text) => {
    console.log("note ->", text);
    dispatch({ type: "SET_NOTE", note: text });
  };

  const updateCaught = (text) => {
    console.log("caught ->", text);
    dispatch({ type: "SET_CAUGHT", caught: text });
  };

  const showDatePicker = () => {
    setShow(true);
  };

  const hideDatePicker = () => {
    setShow(false);
  };

  const handleConfirm = (date) => {
    dispatch({ type: "SELECT_DATE", date: date });
    hideDatePicker();
  };

  return (
    <KeyboardAvoidingView
      style={styles.container}
      behavior={Platform.OS === "ios" ? "padding" : "height"}
      enabled
      keyboardVerticalOffset={100}
    >
      <ScrollView
        style={styles.container}
        contentContainerStyle={styles.contentStyle}
      >
        <Text style={styles.description}>
          Tell your fishing community any caught or notes about this log
        </Text>
        <Stack paddingTop={22} space={16}>
          <Stack space={10}>
            <Text>Log Date</Text>
            <TouchableOpacity onPress={showDatePicker}>
              <View style={styles.datePickerContainer}>
                <Text style={styles.dateField}>
                  {moment(date).format("DD, MMM, YYYY")}
                </Text>
                <Image source={images.calendar} style={styles.calendarBtn} />
              </View>
            </TouchableOpacity>
          </Stack>
          <Stack space={10}>
            <Text>Caught</Text>
            <View style={styles.inputContainer}>
              <TextInput
                placeholder="Type caught"
                style={styles.input}
                multiline
                value={caught}
                onChangeText={updateCaught}
                onSubmitEditing={Keyboard.dismiss}
              />
            </View>
          </Stack>
          <Stack space={10}>
            <Text>Note</Text>
            <View style={styles.inputContainer}>
              <TextInput
                placeholder="Type Note"
                style={styles.input}
                multiline
                value={note}
                onChangeText={updateNote}
                onSubmitEditing={Keyboard.dismiss}
              />
            </View>
          </Stack>
        </Stack>
        <DateTimePickerModal
          isVisible={show}
          mode="date"
          date={date}
          onConfirm={handleConfirm}
          onCancel={hideDatePicker}
          isDarkModeEnabled
        />
      </ScrollView>
    </KeyboardAvoidingView>
  );
};

export default NewLogStepThree;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.mainBack,
  },
  contentStyle: {
    paddingVertical: 26,
    paddingHorizontal: 20,
  },
  description: {
    color: colors.blackText,
    fontSize: 18,
    fontWeight: "500",
    textAlign: "center",
  },
  caption: {
    color: colors.blackText,
    fontFamily: "Lato_400Regular",
    fontSize: 14,
  },
  datePickerContainer: {
    height: 48,
    backgroundColor: "white",
    borderRadius: 8,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "flex-start",
  },
  dateField: {
    flexGrow: 1,
    color: colors.blackText,
    marginLeft: 16,
  },
  calendarBtn: {
    width: 24,
    height: 24,
    marginHorizontal: 12,
  },
  inputContainer: {
    height: 150,
    borderRadius: 8,
    backgroundColor: "white",
    padding: 16,
  },
  input: {
    color: colors.blackText,
    fontSize: 14,
    height: "100%",
    fontFamily: "Lato_400Regular",
  },
});
