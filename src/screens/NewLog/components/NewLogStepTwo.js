import React, {useContext, useState} from 'react';
import {FlatList, ScrollView, StyleSheet, Text, View} from 'react-native';
import colors from 'res/colors';
import AddEquipment from './AddEquipment';
import EquipmentItem from './EquipmentItem';
import { NewLogContext } from '../../../context/NewLogContext';
import {getPreviousEquipment} from "../../../api/db";


const NewLogStepTwo = () => {
  const [itemList, setItemList] = useState([])

  const list = [];
  getPreviousEquipment().then((result) => {
    for(let r of result) {
      let items = JSON.parse(r.items);
      if(items) {
        for(let item of items) {
          if(list.indexOf(item.value) < 0) {
            list.push(item.value)
          }
        }
      }
    }

    setItemList(list)
  })

  const {items} = useContext(NewLogContext)
  return (
    <View style={styles.container}>
      <ScrollView contentContainerStyle={styles.listContent}>
        <Text style={styles.header}>Add your equipment</Text>
        {items.map(item => (
          <EquipmentItem key={item.id} item={ item}/>
        ))}
        <View style={{height: 4}} />
        <AddEquipment itemList={itemList} />
      </ScrollView>
    </View>
  );
};

export default NewLogStepTwo;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.mainBack,
    paddingTop: 8,
  },
  listContent: {
    borderTopLeftRadius: 8,
    borderTopRightRadius: 8,
    backgroundColor: 'white',
    paddingHorizontal: 20,
    flexGrow: 1,
  },
  header: {
    marginVertical: 20,
    textAlign: 'center',
    color: colors.black,
    fontFamily: 'Lato_700Bold',
    fontSize: 18,
  },
});
