import {AntDesign} from '@expo/vector-icons';
import {Stack} from '@mobily/stacks';
import React, {useState, useContext} from 'react';
import {
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';

import {lakes} from '../../../../assets/data/lakes';
import { NewLogContext } from '../../../context/NewLogContext';

import LakeCell from './LakeCell';
import {searchForPlaces} from "../../../api/api";
import images from "../../../res/images";

const NewLogStepOne = () => {
  const [keyword, setKeyword] = useState(null);
  const [values, setValues] = useState([]);
  const {lake: selectedLake} = useContext(NewLogContext)
  let timeout;

  const handleSearch = (value) => {

    setKeyword(value);
    if(timeout) {
      clearTimeout(timeout);
    }

    timeout = setTimeout(async () => {
      console.log("timeout");

      try {
        const predictions = await searchForPlaces(value);
        console.log(predictions);

        let n = [];
        if (predictions.predictions) {
          for (const p of predictions.predictions) {
            n.push({
              name: p.structured_formatting.main_text,
              place_id: p.place_id
            });
          }

          setValues(n);
        }
      } catch(e) {
        console.log("ERROR");
        console.log(e);
      }
    }, 1500);
  }

  function clearSearchField() {
    setKeyword("");
  }
  return (
    <View style={styles.container}>
      <View style={styles.imageContainer}>
        <Image
          style={styles.image}
          source={images.log_lake}
        />
        <Text style={styles.lakeTitle}>
          {selectedLake ? selectedLake.name : ''}
        </Text>
      </View>

      <View style={styles.bottomContainer}>
        <Stack space={16} paddingTop={24}>
          <Text style={styles.searchTitle}>Search Lake</Text>
          {

            <View style={styles.searchBox}>

              <TextInput
                  autoCapitalize="none"
                  autoCorrect={false}
                  onChangeText={handleSearch}
                  style={styles.searchField}
                  value={keyword}
                  status="info"
                  placeholder="Search for lake name"
                  textStyle={{color: '#000'}}
              />



                <TouchableOpacity
                    style={styles.clearButton}
                    onPress={clearSearchField}>
                  <AntDesign name="close" size={16} color={colors.primary}/>
                </TouchableOpacity>


            </View>

          }
        </Stack>
        {

          <ScrollView style={styles.list}>
            {
              values.map(lake => {
                return (
                    <LakeCell
                        key={lake.place_id}
                        lake={lake}
                    />
                )
              })
            }
          </ScrollView>

        }
      </View>

    </View>
  );
};

export default NewLogStepOne;

const styles = StyleSheet.create({
  container: {
    height: '100%'
  },
  imageContainer: {
    marginTop: -14,
    width: '100%',
    height: 250,
    justifyContent: 'flex-end',
  },
  image: {
    position: 'absolute',
    width: "100%",
    height: "100%",
    resizeMode: 'cover'
  },
  lakeTitle: {
    fontSize: 22,
    fontWeight: 'bold',
    color: 'white',
    alignSelf: 'center',
    paddingBottom: 56,
  },
  bottomContainer: {
    marginTop: -16,
    borderTopLeftRadius: 16,
    borderTopRightRadius: 16,
    marginBottom: 0,
    backgroundColor: 'white',
    flexGrow: 1,
    height: '50%'
  },
  searchTitle: {
    color: '#011E48',
    fontFamily: 'Lato_400Regular',
    fontSize: 18,
    alignSelf: 'center',
  },
  searchBox: {
    backgroundColor: '#EEF4F5',
    borderRadius: 8,
    marginHorizontal: 20,
    marginVertical: 8,
    height: 40,
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
  },
  searchField: {
    width: '100%',
    height: '100%',
    paddingLeft: 8,
  },
  clearButton: {
    position: 'absolute',
    right: 4,
    top: 0,
    bottom: 0,
    width: 40,
    height: 40,
    alignItems: 'center',
    justifyContent: 'center',
  },
  pickerContainer: {
    backgroundColor: 'white',
    width: '100%',
  },
});
