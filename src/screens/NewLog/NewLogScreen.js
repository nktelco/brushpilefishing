import React, { useState, useRef, useLayoutEffect, useContext } from "react";
import { StyleSheet, Text, TouchableOpacity, Image, View } from "react-native";
import PagerView, {
  ViewPagerOnPageSelectedEvent,
} from "react-native-pager-view";
import { useSafeAreaInsets } from "react-native-safe-area-context";
import colors from "res/colors";
import { NewLogContext } from "../../context/NewLogContext";
import NewLogStepOne from "./components/NewLogStepOne";
import NewLogStepThree from "./components/NewLogStepThree";
import NewLogStepTwo from "./components/NewLogStepTwo";
import images from "res/images";
import {addLog, createDB, updateLog} from "../../api/db";

const NewLogScreen = ({ navigation }) => {
  const insets = useSafeAreaInsets();

  const [page, setPage] = useState(0);

  useLayoutEffect(() => {
    navigation.setOptions({
      title: "New Log",
      headerLeft: () => (
        <TouchableOpacity onPress={onPressBack}>
          <Image source={images.back} />
        </TouchableOpacity>
      ),
      headerRight: () => <Text style={{ right: 26 }}>{page + 1}/3</Text>,
    });
  }, [navigation, page]);

  const pagerRef = useRef(null);

  const { items, lake, note, date, caught, dispatch, id } =
    useContext(NewLogContext);

  const onPageSelected = (event) => {
    setPage(event.nativeEvent.position);
  };

  const onPressNext = async () => {
    if (page < 2) {
      pagerRef.current.setPage(page + 1);
      setPage(page + 1);
    } else {
      try {
        //create the DB if it doesn't exist
        await createDB();
        if(id) {
          await updateLog( {items, lake, note, date, caught, id});
        } else {
          await addLog({items, lake, note, date, caught});
        }
        navigation.goBack();
      } catch (e) {
        console.log(e);
      }
    }
  };

  const onPressBack = () => {
    if (page > 1) {
      pagerRef.current.setPage(page - 1);
      setPage(page - 1);
    } else {
      navigation.goBack();
    }
  };

  return (
    <View style={{ flex: 1 }}>
      <PagerView
        ref={pagerRef}
        style={styles.pagerView}
        initialPage={0}
        scrollEnabled={false}
        onPageSelected={onPageSelected}
      >
        <View key="0">
          <NewLogStepOne />
        </View>
        <View key="1">
          <NewLogStepTwo />
        </View>
        <View key="2">
          <NewLogStepThree />
        </View>
      </PagerView>
      <View style={styles.nextContainer}>
        <TouchableOpacity onPress={onPressNext}>
          <View style={[styles.next, { marginBottom: insets.bottom + 8 }]}>
            <Text style={styles.nextText}>Next</Text>
          </View>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default NewLogScreen;

const styles = StyleSheet.create({
  pagerView: {
    flex: 1,
    backgroundColor: colors.mainBack,
  },
  nextContainer: {
    paddingHorizontal: 20,
    backgroundColor: "white",
  },
  next: {
    marginVertical: 8,
    backgroundColor: colors.primary,
    borderRadius: 24,
    height: 48,
    width: "100%",
    alignItems: "center",
    justifyContent: "center",
  },
  nextText: {
    fontFamily: "Lato_400Regular",
    fontSize: 16,
    color: "white",
  },
});
