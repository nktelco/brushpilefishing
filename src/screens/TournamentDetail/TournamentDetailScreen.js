import {Box, Inline, Stack} from '@mobily/stacks';
import React from 'react';
import {
  Dimensions,
  Image, SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Alert,
} from 'react-native';
import {Chip} from 'react-native-paper';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import styled from 'styled-components';
import colors from 'res/colors';
import images from 'res/images';
import {getCalendars, createNewCalendar, dateStrings, getCalendarEvents, addToCalendar, updateCalendar} from "../../helpers/helpers";
import RenderHtml, {defaultHTMLElementModels, HTMLContentModel} from "react-native-render-html";

const TournamentDetailScreen = ({route, navigation}) => {
  const insets = useSafeAreaInsets();
  const {tournament} = route.params;

  /* HTML STUFF */
  const width = Dimensions.get("window").width;

  const source = {
    html: `${tournament.description}`,
  };

  const tagsStyles = {
    body: {
      whiteSpace: "normal",
      color: colors.blackText,
      fontFamily: "HelveticaNeue-Regular",
      fontSize: 16,
      margin: 20,
    },
    img: {
      marginHorizontal: 20,
      width: "50%",
    },
    a: {
      display: 'none'
    },
    ul: {
      listStyleType: "none",
    },
    h2: {
      display: 'none'
    }
  };

  const customHTMLElementModels = {
    img: defaultHTMLElementModels.img.extend({
      contentModel: HTMLContentModel.mixed,
    }),
  };

  /* END HTML STUFF */

  const startArr = ((tournament.start_date.split(" ")[0].split("-")));
  const endArr = ((tournament.end_date.split(" ")[0].split("-")));

  const start_date = `${dateStrings(Number(startArr[1]))} ${startArr[2]},${startArr[0]}`;
  const end_date = `${dateStrings(Number(startArr[1]))} ${endArr[2]},${endArr[0]}`;

  const start_time = ((tournament.start_date.split(" ")[1].split(":")));
  const end_time = ((tournament.end_date.split(" ")[1].split(":")));

  let start_time_string = "";
  let end_time_string = "";

  if(Number(start_time[0]) > 12) {
    start_time_string = `${Number(start_time[0])-12}:${start_time[1]}pm`;
  } else {
    start_time_string = `${start_time[0]}:${start_time[1]}am`;
  }

  if(Number(end_time[0]) > 12) {
    end_time_string = `${Number(end_time[0])-12}:${end_time[1]}pm`;
  } else {
    end_time_string = `${end_time[0]}:${end_time[1]}am`;
  }

  React.useLayoutEffect(() => {
    navigation.setOptions({
      title: "Tournament",
      headerBackImage: ({ props }) => {
        return <Image source={images.xmark} {...props} />;
      },
    });
  }, [navigation]);

  let asURL = tournament.images[0]=="String";

  return (
      <SafeAreaView>
        <ScrollView>
          <Box style={styles.outer}>
            <Image style={styles.image} source={asURL ? {uri: tournament.images[0]} : tournament.images[0]} />

                <Box
                  alignX="between"
                  alignY="center"
                  direction="column"
                  marginBottom={9}
                >
                  <SubTitle>
                  {
                    tournament.organizer.map((o, index)=> (
                        <Text key={index}>{o.organizer}</Text>
                    ))
                  }
                  </SubTitle>
                <DateString>{`${start_date} ${start_time_string} - ${end_date} ${end_time_string}`}</DateString>
              </Box>
            <Title>{tournament.title}</Title>
            <Inline space={7} marginTop={16} marginBottom={40}>
              {tournament.tags.map((item, index) => {
                return (
                  <Chip
                    key={index.toString()}
                    mode="outlined"
                    style={styles.chip}
                    textStyle={styles.chipText}>
                    {item}
                  </Chip>
                );
              })}
                </Inline>
        <TouchableOpacity onPress={() => addTournamentToCalendar(tournament.title, tournament, start_date, start_time, end_date, end_time)}>
          <View style={styles.addBtn}>
            <Text style={styles.addText}>Add to calendar</Text>
            <Image source={images.calendar_add} style={styles.calendar_add} />
          </View>
        </TouchableOpacity>
        <Stack space={8} marginTop={40} marginBottom={40}>
          <TournamentText>TOURNAMENT DETAILS</TournamentText>
          <RenderHtml
              contentWidth={width}
              source={source}
              tagsStyles={tagsStyles}
              customHTMLElementModels={customHTMLElementModels}
          />
        </Stack>
        </Box>
        </ScrollView>
      </SafeAreaView>
  );
};

const addTournamentToCalendar = async(title, tournament, start_date, start_time, end_date, end_time) => {
  const calendars = await getCalendars();

  //first get your calendar id
  let calendarId = null;
  for(let c of calendars) {
    if(c.title === "BrushPile Fishing") {
      calendarId = c.id;
      break;
    }
  }

  if(calendarId == null) {
    calendarId = await createNewCalendar();
  }

  const startDate = new Date(start_date);
  startDate.setHours(start_time[0], start_time[1], start_time[2]);

  const endDate = new Date(end_date);
  endDate.setHours(end_time[0], end_time[1], end_time[2]);

  const events = await getCalendarEvents([calendarId], startDate, endDate);

  let eventId;
  const filtered = events.filter((evt) => (
      //we're matching on title
      evt.title == title
  ))

  if(filtered[0]) {
    eventId = filtered[0].id;
  }

  //do a simple replacement of the html
  const notes = tournament.description.replace(/(<([^>]+)>)/ig,"").replace(/(\r\n|\r|\n){2,}/g, '$1\n');
  const event = {
    title: title,
    startDate: startDate.getTime(),
    endDate: endDate.getTime(),
    notes: notes,
    url: tournament.url,
  }

  //time zone required for android.
  //for now add it as the phone's time zone
  //but let's add this to the asset in the future
  if(Platform.OS === 'android') {
    const d = new Date().getTimezoneOffset() / 60;
    const hoursOffset = Math.floor(d);
    const minutes = (d-hoursOffset * 60).toString().padStart(2, "0");
    const GMTOffset = hoursOffset < 0 ? `GMT${GMTOffset}:minutes` :  `GMT+${GMTOffset}:minutes`;
    event.timeZone = GMTOffset;
  };

  console.log(event);

  if(eventId == null) {
    //add a new event
    try {
      await addToCalendar(calendarId, event);
      Alert.alert(`Calendar`, `Added event to your calendar`);

    } catch(e) {
      Alert.alert("Calendar", "There was a problem adding this event");
    }
  } else {
    //update the existing event
    try {
      await updateCalendar(eventId, event);
      Alert.alert("Calendar", "Updated event");

    } catch(e) {
      Alert.alert("Calendar", "There was a problem updating this event");
    }
  }
}

export default TournamentDetailScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.mainBack,
  },
  outer: {
    margin: 10,
    overflow: "hidden"
  },
  image: {
    height: 250,
    zIndex: 10,
    resizeMode: "cover"
  },
  scrollViewContent: {
    paddingTop: 26,
    paddingHorizontal: 20,
  },
  chipText: {
    fontFamily: 'Roboto-Regular',
    fontSize: 13,
    color: colors.lightText,
  },
  chip: {
    borderColor: colors.blackText,
    borderWidth: 0,
  },
  addBtn: {
    height: 48,
    backgroundColor: colors.primary,
    justifyContent: 'center',
    borderRadius: 24,
  },
  calendar_add: {
    position: 'absolute',
    top: 4,
    bottom: 4,
    left: 20,
    width: 40,
    height: 40,
  },
  addText: {
    color: 'white',
    alignSelf: 'center',
    fontFamily: 'Lato_400Regular',
    fontSize: 16,
  }
});

const SubTitle = styled.Text`
  font-family: Roboto-Bold;
  font-style: normal;
  font-weight: normal;
  font-size: 20px;
  color: ${colors.orange};
`;

const TournamentText = styled.Text`
  font-size: 20px;
`;

const Title = styled.Text`
  font-family: Roboto-Bold;
  font-style: normal;
  font-weight: bold;
  font-size: 28px;
  color: ${colors.blackText};
`;

const DateString = styled.Text`
  font-family: Lato_400Regular;
  font-style: normal;
  font-weight: normal;
  font-size: 16px;
  margin-top: 10px;
  color: ${colors.lightText};
`;

const Description = styled.Text`
  font-family: Lato_400Regular;
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  line-height: 23px;
  color: #011e48;
`;
