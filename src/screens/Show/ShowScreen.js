import { Ionicons } from "@expo/vector-icons";
import SeasonPreviewItem from "components/SeasonPreviewItem";
import ShowHeader from "components/ShowHeader";
import ShowHero from "components/ShowHero";
import React, { useRef, useState, useEffect, useLayoutEffect } from "react";
import {
  StyleSheet,
  TouchableOpacity,
  View,
  ActivityIndicator,
  Text,
} from "react-native";
import { FlatList, ScrollView } from "react-native-gesture-handler";
import { getAllVideoCategories, loadEpisodesForSeason } from "../../api/api";
import colors from "res/colors";
import HeaderForeground from "components/HeaderForeground";
import { StretchyFlatList } from "react-native-stretchy";
import images from "res/images";
const ShowScreen = ({ navigation }) => {
  const [seasonList, setSeasonList] = useState([]);
  const [isLoading, setIsLoading] = useState(false);

  useLayoutEffect(() => {
    navigation.setOptions({
      title: "",
    });
  }, [navigation]);

  useEffect(() => {
    const unsubscribe = fetchData();
    return () => {
      unsubscribe;
    };
  }, []);

  const fetchData = async () => {
    setIsLoading(true);
    const list = await getAllVideoCategories();
    const newList = [];
    for (const season of list) {
      const episodes = await loadEpisodesForSeason(season.id, 3);
      newList.push({ ...season, episodes: episodes });
    }
    setSeasonList(newList);
    setIsLoading(false);
  };

  useLayoutEffect(() => {
    navigation.setOptions({
      headerRight: (props) => (
        <TouchableOpacity
          {...props}
          style={{ marginRight: 26 }}
          onPress={() => navigation.navigate("ShowSearch")}
        >
          {
            //<Ionicons name="search-outline" size={26} color={colors.primary} />
          }
        </TouchableOpacity>
      ),
    });
  }, [navigation]);

  const renderItem = ({ item, index }) => {
    return (
      <>
        {
          //{index === 0 ? <ShowHeader/> : null}
        }

        <SeasonPreviewItem season={item} />
      </>
    );
  };

  const renderHeader = () => {
    return (
      <>
        {/* <ShowHero /> */}
        <ShowHeader />
      </>
    );
  };

  const renderEmptyView = () => {
    return isLoading ? (
      <View
        style={[
          {
            backgroundColor: colors.darkBlue,
            flex: 1,
          },
        ]}
      >
        <ActivityIndicator size="large" color={colors.primary} />
      </View>
    ) : (
      <Text>No data</Text>
    );
  };
  return (
    <View style={styles.container}>
      <StretchyFlatList
        image={images.home_video_preview}
        foreground={<HeaderForeground />}
        data={seasonList}
        style={{ backgroundColor: colors.darkBlue, flex: 1 }}
        renderItem={renderItem}
        keyExtractor={(item) => item.id.toString()}
        ListHeaderComponent={renderHeader}
        ListEmptyComponent={renderEmptyView}
        contentContainerStyle={{
          minHeight: "100%",
        }}
        ItemSeparatorComponent={() => (
          <View style={{ backgroundColor: colors.darkBlue }}>
            <View
              style={{
                backgroundColor: "rgba(255, 255, 255, 0.1)",
                margin: 20,
                height: 1,
              }}
            />
          </View>
        )}
      />
    </View>
  );
};

export default ShowScreen;

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.darkBlue,
    flex: 1,
  },
  banner: {
    height: 285,
  },
  info: {
    height: 212,
  },
});
