import React, { useState, useRef } from "react";
import { StyleSheet, Text, View, Dimensions } from "react-native";
import PhoneInput from "react-native-phone-number-input";
import { Box, Stack } from "@mobily/stacks";

const width = Dimensions.get("window").width;
const PhoneEditForm = () => {
  const phoneInput = useRef(null);
  const [value, setValue] = useState("");
  const [formattedValue, setFormattedValue] = useState("");
  const [valid, setValid] = useState(false);
  const [showMessage, setShowMessage] = useState(false);
  return (
    <View>
      <Stack marginX={20} space={10} marginTop={32}>
        <Text>Phone Number</Text>
        <PhoneInput
          ref={phoneInput}
          defaultValue={value}
          defaultCode="US"
          layout="first"
          onChangeText={(text) => {
            setValue(text);
          }}
          onChangeFormattedText={(text) => {
            setFormattedValue(text);
          }}
          //   autoFocus
          containerStyle={styles.containerStyle}
          textContainerStyle={{
            borderTopRightRadius: 8,
            borderBottomRightRadius: 8,
            backgroundColor: "white",
          }}
          flagButtonStyle={styles.flagButtonStyle}
        />
        {!valid && value.length > 0 ? <Text>Invalid phone number</Text> : null}
      </Stack>
    </View>
  );
};

export default PhoneEditForm;

const styles = StyleSheet.create({
  containerStyle: {
    borderRadius: 8,
    width: width - 40,
  },
  flagButtonStyle: {
    backgroundColor: "#DFE8EA",
    borderTopLeftRadius: 8,
    borderBottomLeftRadius: 8,
  },
});
