import { Lato_400Regular } from "@expo-google-fonts/lato";
import React, { useLayoutEffect } from "react";
import { KeyboardAvoidingView, StyleSheet, Text, View } from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";
import colors from "../../res/colors";
import PhoneEditForm from "./components/PhoneEditForm";

const ProfileEditScreen = ({ route, navigation }) => {
  const { mode } = route.params;

  useLayoutEffect(() => {
    navigation.setOptions({
      title: mode === "Phone" ? "Phone Number" : "Email",
    });
  }, [navigation, route]);

  return (
    <View style={styles.container}>
      <KeyboardAvoidingView>
        <View style={styles.form}>
          <PhoneEditForm />
        </View>
        <TouchableOpacity>
          <View style={styles.bottomContainer}>
            <Text style={styles.saveText}>Save</Text>
          </View>
        </TouchableOpacity>
      </KeyboardAvoidingView>
    </View>
  );
};

export default ProfileEditScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.mainBack,
    flexDirection: "column",
    justifyContent: "space-between",
  },
  form: {
    flexShrink: 1,
  },
  bottomContainer: {
    position: "absolute",
    marginHorizontal: 20,
    marginVertical: 8,
    backgroundColor: colors.primary,
    borderRadius: 24,
    height: 48,
    justifyContent: "center",
    alignItems: "center",
    left: 0,
    right: 0,
  },
  saveText: {
    fontFamily: "Lato_400Regular",
    fontSize: 16,
    color: "white",
  },
});
