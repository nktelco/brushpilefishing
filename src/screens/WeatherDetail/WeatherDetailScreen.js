import React, { useLayoutEffect, useState } from "react";
import {
  FlatList,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  ActivityIndicator,
} from "react-native";
import colors from "res/colors";
import useWeekWeatherStore from "./components/useWeekWeatherStore";
import WeatherDetailCard from "./components/WeatherDetailCard";
const WeatherDetailScreen = ({ route, navigation }) => {
  useLayoutEffect(() => {
    navigation.setOptions({
      title: "7 Days",
    });
  }, [navigation]);

  const { location } = route.params;

  const renderItem = ({ item }) => {
    return <WeatherDetailCard item={item} />;
  };
  const [selected, setSelected] = useState(0);

  const [weekdays, forecast, isLoading] = useWeekWeatherStore(location);

  const renderListSeparator = () => {
    return <View style={{ height: 14 }} />;
  };

  function handleChange(index) {
    setSelected(index);
  }

  return (
    <View style={styles.container}>
      <View style={styles.wrapperHorizontal}>
        <ScrollView horizontal showsHorizontalScrollIndicator={false}>
          {weekdays.map((day, index) => {
            return (
              <TouchableOpacity
                key={day.index.toString()}
                onPress={() => setSelected(index)}
              >
                <View
                  style={[
                    index === selected
                      ? styles.weekdaySelected
                      : styles.weekday,
                  ]}
                >
                  <Text
                    style={
                      index === selected
                        ? styles.weekdayTextSelected
                        : styles.weekdayText
                    }
                  >
                    {day.label}
                  </Text>
                </View>
              </TouchableOpacity>
            );
          })}
        </ScrollView>
      </View>
      {isLoading ? (
        <View
          style={[
            {
              alignItems: "center",
              justifyContent: "center",
              flexGrow: 1,
            },
            styles.list,
          ]}
        >
          <ActivityIndicator size="large" color={colors.primary} />
        </View>
      ) : (
        <FlatList
          style={styles.list}
          contentContainerStyle={styles.listContent}
          data={forecast.length > 0 ? forecast[selected] : []}
          renderItem={renderItem}
          keyExtractor={(item) => item.dt.toString()}
          ItemSeparatorComponent={renderListSeparator}
          ListEmptyComponent={() => (
            <View
              style={[
                {
                  alignItems: "center",
                  justifyContent: "center",
                },
              ]}
            >
              <Text>No data available</Text>
            </View>
          )}
        />
      )}
    </View>
  );
};

export default WeatherDetailScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.mainBack,
  },
  list: {
    backgroundColor: "white",
    borderTopLeftRadius: 32,
    borderTopRightRadius: 32,
  },
  listContent: {
    paddingHorizontal: 20,
    paddingVertical: 20,
  },
  wrapperHorizontal: {
    // height: 74,
    justifyContent: "center",
    alignItems: "center",
    margin: "auto",
    color: "black",
    marginVertical: 18,
  },
  weekday: {
    paddingHorizontal: 32,
    paddingVertical: 11,
    borderRadius: 20,
  },
  weekdaySelected: {
    paddingHorizontal: 32,
    paddingVertical: 11,
    backgroundColor: colors.primary,
    borderRadius: 20,
  },
  weekdayText: {
    fontFamily: "Lato_400Regular",
    fontSize: 14,
    color: colors.blackText,
  },
  weekdayTextSelected: {
    fontFamily: "Lato_700Bold",
    fontSize: 14,
    color: "white",
  },
});
