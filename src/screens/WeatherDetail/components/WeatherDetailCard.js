import moment from "moment";
import React from "react";
import { StyleSheet, Text, View } from "react-native";
import { getColorForTemp, getWeatherSymbol } from "../../../helpers/helpers";
const WeatherDetailCard = ({ item }) => {
  const renderWeather = () => {
    return getWeatherSymbol(item.weather[0].main);
  };

  return (
    <View
      style={[
        styles.container,
        { backgroundColor: getColorForTemp(item.temp) },
      ]}
    >
      <Text style={styles.time}>{moment.unix(item.dt).format("h:mm A")}</Text>
      <View style={styles.weatherSymbol}>{renderWeather()}</View>
      <Text style={styles.weatherLabel}>{item.weather[0].main}</Text>
      <Text style={styles.temperature}>{Math.round(item.main.temp)}°F</Text>
    </View>
  );
};

export default WeatherDetailCard;

const styles = StyleSheet.create({
  container: {
    borderRadius: 8,
    flexDirection: "row",
    alignItems: "center",
    paddingHorizontal: 16,
    justifyContent: "space-between",
    height: 65,
  },
  temperature: {
    color: "#011E48",
    fontSize: 17,
    fontWeight: "bold",
  },
  time: {
    color: "#011E48",
    fontWeight: "bold",
    fontSize: 17,
  },
  weatherLabel: {
    color: "#011E48",
    fontSize: 17,
    fontWeight: "bold",
  },
  weatherSymbol: {
    width: 56,
    height: 54,
  },
});
