import moment from "moment";
import React, { useState, useEffect } from "react";
import { StyleSheet, Text, View } from "react-native";
import { getWeatherOfFiveDays } from "../../../api/weather";

const useWeekWeatherStore = (location) => {
  const weekdays = Array.from({ length: 7 }, (v, i) => i).map((index) => {
    const dest = moment().startOf("day").add(index, "day");
    if (index === 0) {
      return { index: 0, label: "Today", time: dest };
    } else if (index === 1) {
      return { index: 1, label: "Tomorrow", time: dest };
    } else {
      return { index: index, label: dest.format("ddd, MMM DD"), time: dest };
    }
  });

  const [forecast, setForecast] = useState([]);

  const [isLoading, setIsLoading] = useState(false);

  const fetchData = async () => {
    setIsLoading(true);
    const data = await getWeatherOfFiveDays(location.lat, location.long);
    let forecast = [];
    weekdays.forEach((weekday) => {
      forecast.push(
        data.filter((item) => {
          return moment.unix(item.dt).isSame(weekday.time, "day");
        })
      );
    });
    setForecast(forecast);
    setIsLoading(false);
  };

  useEffect(() => {
    fetchData();
  }, [location]);

  return [weekdays, forecast, isLoading];
};

export default useWeekWeatherStore;
