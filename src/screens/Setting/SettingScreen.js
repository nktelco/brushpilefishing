import { Box, Stack } from "@mobily/stacks";
import React from "react";
import {
  Image,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from "react-native";
import { useSafeAreaInsets } from "react-native-safe-area-context";
import GradientCircleContainer from "../../components/GradientCircleContainer";
import colors from "../../res/colors";
import images from "../../res/images";
const SettingScreen = ({ navigation }) => {
  const insets = useSafeAreaInsets();

  const back = () => {
    navigation.goBack();
  };

  const editPhoneNumber = () => {
    navigation.navigate("ProfileEdit", { mode: "Phone" });
  };

  const editEmail = () => {
    navigation.navigate("ProfileEdit", { mode: "Email" });
  };

  return (
    <View style={styles.container}>
      <StatusBar barStyle="dark-content" />
      <View style={[styles.header, { marginTop: insets.top }]}>
        <TouchableOpacity onPress={back}>
          <Image source={images.arrow_left} />
        </TouchableOpacity>
        <TouchableOpacity>
          <Image source={images.three_dots} />
        </TouchableOpacity>
      </View>
      <View style={[styles.banner]}>
        <Stack space={15} horizontal align="center">
          <GradientCircleContainer size={76}>
            <Image
              style={styles.avatar}
              source={{ uri: "https://i.pravatar.cc/300" }}
            />
          </GradientCircleContainer>
          <Text style={styles.name}>Josh Smith</Text>
        </Stack>
      </View>
      <ScrollView style={styles.scrollView}>
        <Stack
          align="center"
          space={18}
          horizontal
          paddingTop={25}
          paddingX={30}
          paddingBottom={18}
        >
          <Image source={images.photo} style={{ width: 28, height: 28 }} />
          <Text>Set Profile Photo</Text>
        </Stack>
        <View style={{ height: 1, backgroundColor: "#EEF4F5" }} />
        <Box paddingX={20} paddingTop={20} paddingBottom={40}>
          <Text style={styles.title}>ACCOUNT</Text>
          <Stack space={6} marginTop={10}>
            <Text style={styles.label}>Phone Number</Text>
            <Box
              direction="row"
              alignX="between"
              alignY="center"
              marginBottom={6}
            >
              <TextInput
                defaultValue="+61 123 4512 12345"
                style={styles.itemText}
              />
              <TouchableOpacity onPress={editPhoneNumber}>
                <Image source={images.profile_edit} />
              </TouchableOpacity>
            </Box>
          </Stack>
          <View style={{ height: 1, backgroundColor: "#EEF4F5" }} />
          <Stack space={6} marginTop={10}>
            <Text style={styles.label}>Email Address</Text>
            <Box
              direction="row"
              alignX="between"
              alignY="center"
              marginBottom={6}
            >
              <TextInput
                defaultValue="williamlijah@gmail.com"
                style={styles.itemText}
              />
              <TouchableOpacity onPress={editEmail}>
                <Image source={images.profile_edit} />
              </TouchableOpacity>
            </Box>
          </Stack>
        </Box>
        <View style={{ height: 1, backgroundColor: "#EEF4F5" }} />
        <Box paddingX={20} paddingTop={20} paddingBottom={45}>
          <Text style={styles.title}>SETTINGS</Text>
          <Stack space={14} marginTop={13}>
            <Stack space={16} horizontal align="center">
              <Image source={images.profile_notification} />
              <Text style={styles.itemText}>Notifications</Text>
            </Stack>
            <Stack space={16} horizontal align="center">
              <Image source={images.profile_privacy} />
              <Text style={styles.itemText}>Privacy and Security</Text>
            </Stack>
          </Stack>
        </Box>
        <View style={{ height: 1, backgroundColor: "#EEF4F5" }} />
        <Box paddingX={20} paddingTop={20} paddingBottom={45}>
          <Text style={styles.title}>HELP</Text>
          <Stack space={14} marginTop={13}>
            <Stack space={16} horizontal align="center">
              <Image source={images.profile_contact_us} />
              <Text style={styles.itemText}>Contact Us</Text>
            </Stack>
            <Stack space={16} horizontal align="center">
              <Image source={images.profile_shield} />
              <Text style={styles.itemText}>Privacy Policy</Text>
            </Stack>
          </Stack>
        </Box>
      </ScrollView>
    </View>
  );
};

export default SettingScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.darkBlue,
  },
  header: {
    height: 56,
    alignItems: "center",
    justifyContent: "space-between",
    flexDirection: "row",
    paddingHorizontal: 20,
  },
  banner: {
    paddingHorizontal: 20,
    marginBottom: 16,
  },
  avatar: {
    width: 62,
    height: 62,
    borderRadius: 31,
    backgroundColor: "white",
  },
  name: {
    fontSize: 18,
    fontWeight: "bold",
    color: "white",
  },
  scrollView: {
    backgroundColor: "white",
    borderTopLeftRadius: 32,
    borderTopRightRadius: 32,
  },
  image: {
    width: 24,
    height: 24,
  },
  itemText: {
    fontSize: 16,
    color: "#19284C",
  },
  label: {
    fontSize: 14,
    color: "#535D7A",
  },
  title: {
    color: "#02C0DE",
    fontSize: 13,
    fontWeight: "bold",
  },
});
