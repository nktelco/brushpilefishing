import { Ionicons } from "@expo/vector-icons";
import { Box } from "@mobily/stacks";
import CalenderView from "components/CalenderView";
import LogItem from "components/LogItem";
import React, { useContext, useState, useEffect } from "react";
import { useActionSheet } from "@expo/react-native-action-sheet";
import {
  SectionList,
  StyleSheet,
  TouchableOpacity,
  View,
  Image,
  Text,
  RefreshControl,
} from "react-native";
import styled from "styled-components";
import colors from "res/colors";
import images from "res/images";
import { LogsContext } from "../../context/LogsContext";
import { convertLogs } from "../../utils/utils";
import { NewLogContext } from "../../context/NewLogContext";
import { deleteLog, getLog } from "../../api/db";
import button from "react-native-web/dist/exports/Button";
import context from "react-native-calendars/src/expandableCalendar/Context";
const LogsScreen = ({ navigation }) => {
  const { dispatch } = useContext(NewLogContext);

  React.useLayoutEffect(() => {
    navigation.setOptions({
      headerRight: (props) => (
        <TouchableOpacity
          {...props}
          style={{ marginRight: 26 }}
          onPress={() => {
            navigation.navigate("NewLog");
          }}
        >
          {/* <Ionicons name="add" size={21} color="black" /> */}
          <Image source={images.ic_add} />
        </TouchableOpacity>
      ),
    });
  }, [navigation]);

  useEffect(() => {
    const unsubscribe = navigation.addListener("focus", () => {
      dispatch({ type: "RESET" });
      reloadLogs();
    });

    return unsubscribe;
  }, [navigation]);

  const renderItem = ({ item }) => {
    return (
      <LogItem
        item={item}
        showMoreOptions={() => {
          showMoreOptions(item.id);
        }}
      />
    );
  };

  const { logs, isRefreshing, reloadLogs } = useContext(LogsContext);

  const [selectedYear, setSelectedYear] = useState(new Date().getFullYear());

  const { showActionSheetWithOptions } = useActionSheet();

  const sheetConfig = {
    options: ["Cancel", "Edit", "Delete"],
    cancelButtonIndex: 0,
    destructiveButtonIndex: 2,
  };

  const showMoreOptions = async (id) => {
    showActionSheetWithOptions(sheetConfig, async (buttonIndex) => {
      if (buttonIndex === 1) {
          editLog(id);
      } else if (buttonIndex === 2) {
          await deleteLog(id);
          reloadLogs();
      }
    });
  };


  const editLog = async (id) => {
    const result = await getLog(id);
    if(result[0]) {
        console.log(result[0]);
        dispatch({type: "SET_LOG_DATA", ...result[0]})
    }
    navigation.navigate("NewLog");
  }

  const logsToShow = convertLogs(logs);

  return logsToShow.length === 0 ? (
    <View
      style={{
        flex: 1,
        backgroundColor: colors.mainBack,
      }}
    >
      <Box paddingX={26} paddingY={42}>
        <CalenderView />
      </Box>
    </View>
  ) : (
    <View
      style={{
        flex: 1,
        backgroundColor: colors.mainBack,
      }}
    >
      <SectionList
        sections={logsToShow}
        keyExtractor={(item, index) => item + index}
        renderItem={renderItem}
        refreshControl={
          <RefreshControl refreshing={isRefreshing} onRefresh={reloadLogs} />
        }
        stickySectionHeadersEnabled={false}
        renderSectionHeader={({ section }) =>
          section.index === 0 ? (
            <>
              <Box paddingX={26} paddingY={42}>
                <CalenderView />
              </Box>
              <Box
                paddingX={20}
                paddingY={17}
                alignX="between"
                alignY="center"
                direction="row"
                style={{
                  backgroundColor: "white",
                  borderTopLeftRadius: 8,
                  borderTopRightRadius: 8,
                }}
              >
                <LogTitle>Logs</LogTitle>
                <Text
                  style={{
                    fontFamily: "Lato_400Regular",
                    fontSize: 14,
                    color: colors.primary,
                  }}
                >
                  {selectedYear}
                </Text>
              </Box>
              <Box
                paddingX={20}
                paddingY={11}
                alignX="between"
                alignY="center"
                direction="row"
                style={{
                  backgroundColor: colors.mainBack,
                  borderTopColor: "#E1E6EA",
                  borderTopWidth: 1,
                }}
              >
                <SectionTitle>{section.title}</SectionTitle>
                <LogCount>{section.count} Logs</LogCount>
              </Box>
            </>
          ) : (
            <Box
              paddingX={20}
              paddingY={11}
              alignX="between"
              alignY="center"
              direction="row"
              style={{
                backgroundColor: colors.mainBack,
                borderTopColor: "#E1E6EA",
                borderTopWidth: 1,
              }}
            >
              <SectionTitle>{section.title}</SectionTitle>
              <LogCount>{section.count} Logs</LogCount>
            </Box>
          )
        }
      />
    </View>
  );
};

export default LogsScreen;

const styles = StyleSheet.create({});

const Divider = styled.View`
  border: 1px solid rgba(0, 0, 0, 0.2);
`;

const LogTitle = styled.Text`
  font-family: Roboto-Bold;
  font-style: normal;
  font-weight: normal;
  font-size: 19px;
  color: ${colors.blackText};
  text-transform: capitalize;
`;

const SectionTitle = styled.Text`
  font-family: Roboto-Regular;
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  line-height: 16px;
  color: #1a284f;
`;

const LogCount = styled.Text`
  font-family: Roboto-Regular;
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  line-height: 16px;
  color: #535d7a;
`;
