import React, {useEffect, useState} from 'react';
import {FlatList, StyleSheet, View} from 'react-native';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import SeasonEpisodeItem from './components/SeasonEpisodeItem';
import {loadEpisodesForSeason} from '../../api/api';

const SeasonScreen = ({route, navigation}) => {
  const insets = useSafeAreaInsets();
  const renderItem = ({item}) => {
    return <SeasonEpisodeItem episode={item} />;
  };

  const {season} = route.params;
  const [episodes, setEpisodes] = useState([]);

  React.useLayoutEffect(() => {
    navigation.setOptions({
      title: season.name,
    });
  }, [navigation]);

  useEffect(()=> {
    getEpisodes();
  }, [])

  const getEpisodes = async () => {
    const episodes = await loadEpisodesForSeason(season.id, 99);
    setEpisodes(episodes)
  }

  return (
    <View style={{flex: 1, backgroundColor: colors.mainBack}}>
      <FlatList
        data={episodes}
        columnWrapperStyle={{
          justifyContent: 'space-between',
        }}
        contentContainerStyle={{
          paddingHorizontal: '13%',
          paddingVertical: '9%',
        }}
        ItemSeparatorComponent={() => <View style={{height: 38}} />}
        keyExtractor={item => item.id.toString()}
        numColumns={2}
        renderItem={renderItem}
      />
    </View>
  );
};

export default SeasonScreen;

const styles = StyleSheet.create({});
