import OfferItem from "components/OfferItem";
import React, { useEffect, useState, useLayoutEffect } from "react";
import {
  Dimensions,
  FlatList,
  StyleSheet,
  View,
  ActivityIndicator,
} from "react-native";
import colors from "res/colors";
import { getOffersByCategory } from "../../api/api";
import { parseText } from "../../helpers/helpers";
const width = Dimensions.get("window").width;
const OfferDetailScreen = ({ route, navigation }) => {
  const { category } = route.params;
  const [list, setList] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  useLayoutEffect(() => {
    navigation.setOptions({
      title: parseText(category.name),
    });
  }, [navigation]);

  const renderItem = ({ item }) => {
    return <OfferItem item={item} />;
  };

  const renderItemSeparator = () => {
    return <View style={{ height: 16 }} />;
  };

  const getOfferList = async () => {
    setIsLoading(true);
    console.log("categoryId", category.id);
    const list = await getOffersByCategory(category.id);
    setList(list);
    setIsLoading(false);
  };

  useEffect(() => {
    getOfferList();
  }, []);

  return (
    <View style={styles.container}>
      {isLoading ? (
        <View
          style={{ justifyContent: "center", alignItems: "center", flex: 1 }}
        >
          <ActivityIndicator size="large" color={colors.primary} />
        </View>
      ) : (
        <FlatList
          data={list}
          keyExtractor={(item) => item.id.toString()}
          contentContainerStyle={styles.contentContainer}
          renderItem={renderItem}
          ItemSeparatorComponent={renderItemSeparator}
        />
      )}
    </View>
  );
};

export default OfferDetailScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.mainBack,
  },
  contentContainer: {
    paddingHorizontal: 20,
    paddingVertical: 20,
  },
});
