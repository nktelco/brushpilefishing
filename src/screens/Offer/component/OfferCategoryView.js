import { Stack } from "@mobily/stacks";
import React from "react";
import {
  Image, Linking,
  StyleSheet,
  Text,
  TouchableWithoutFeedback,
  View,
} from "react-native";
import colors from "res/colors";
import { parseText } from "../../../helpers/helpers";
import {getBpfApiPath} from "../../../api/api";

const OfferCategoryView = ({ onPress, item }) => {
  // const image = item.images.length > 0 ? item.images[0].src : null;
  const image = item?.image;

  return (
      <TouchableWithoutFeedback onPress={() => {
        Linking.openURL(item.link);
      }}>
        <View style={styles.container}>
        <Image style={styles.image} source={{uri: `${getBpfApiPath()}/${image}`}} />
        <Stack
          space={4}
          //   align="center"
          paddingX={8}
          paddingY={8}
          style={{ backgroundColor: "white" }}
        >
          <Text style={[styles.text, styles.title]}>
            {parseText(item.sponsor_name)}
          </Text>
          <Text style={[styles.text, styles.offer]}>{item.text}</Text>
        </Stack>
      </View>
    </TouchableWithoutFeedback>
  );
};
export default OfferCategoryView;

const styles = StyleSheet.create({
  container: {
    // height: 255,
    borderRadius: 16,
    overflow: "hidden",
  },
  text: {
    color: "black",
    fontWeight: "bold",
  },
  title: {
    fontSize: 18,
  },
  offer: {
    fontSize: 13,
  },
  image: {
    height: 255,
    // position: "absolute",
    // top: 0,
    // left: 0,
    // right: 0,
    // bottom: 0,
    // overflow: "hidden",
    backgroundColor: colors.darkBlue,
  },
});
