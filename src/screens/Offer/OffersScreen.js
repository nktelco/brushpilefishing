import React, { useState, useRef, useEffect } from "react";
import {
  FlatList,
  StyleSheet,
  Text,
  View,
  Dimensions,
  Image,
  ActivityIndicator,
} from "react-native";
import OfferItem from "components/OfferItem";
import colors from "res/colors";
import { Box } from "@mobily/stacks";
import Carousel, { Pagination } from "react-native-snap-carousel";
import images from "res/images";
import { getOfferCategories, getOffers } from "../../api/api";
import OfferCategoryView from "./component/OfferCategoryView";
const width = Dimensions.get("window").width;
const OffersScreen = ({ navigation }) => {
  const [list, setList] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [currentPage, setCurrentPage] = useState(0);
  const [lastPage, setLastPage] = useState(false);
  const carouselRef = useRef();

  const onPressItem = (category) => {
    navigation.navigate("OfferCategory", { category: category });
  };

  const renderItem = ({ item }) => {
    return <OfferCategoryView onPress={() => onPressItem(item)} item={item} />;
  };

  const renderItemSeparator = () => {
    return <View style={{ height: 16 }} />;
  };

  useEffect(() => {
    setIsLoading(true);
    loadOffers();
  }, []);

  const loadOffers = async() => {
    console.log("loading offers");
    const page = currentPage;
    if(!lastPage) {
      console.log("not last page");
      const loadedList = await getOffers(30, 10, page);
      console.log(loadedList);
      if (loadedList.length > 0) {
        setIsLoading(false);
        setCurrentPage(page + 1);
        setList([...list, ...loadedList]);
      } else {
        setLastPage(true);
      }
    }
  }

  return (
    <View style={styles.container}>
      {isLoading ? (
        <View
          style={{ justifyContent: "center", alignItems: "center", flex: 1 }}
        >
          <ActivityIndicator size="large" color={colors.primary} />
        </View>
      ) : (
        <FlatList
          data={list}
          keyExtractor={(item) => item.id.toString()}
          contentContainerStyle={styles.contentContainer}
          renderItem={renderItem}
          ItemSeparatorComponent={renderItemSeparator}
          onEndReached={() => {
            loadOffers()
          }}
          // ListHeaderComponent={renderHeader}
        />
      )}
    </View>
  );
};

export default OffersScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.mainBack,
  },
  contentContainer: {
    paddingHorizontal: 20,
    paddingVertical: 20,
  },
});
