import React, { useLayoutEffect } from "react";
import { StyleSheet, Text, View } from "react-native";
import ShowSearchBar from "./components/ShowSearchBar";

const ShowSearchScreen = ({ navigation }) => {
  return (
    <View style={styles.container}>
      <ShowSearchBar />
      <Text>ShowSearchScreen</Text>
    </View>
  );
};

export default ShowSearchScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    maxWidth: "100%",
  },
});
