import { useNavigation } from "@react-navigation/core";
import React from "react";
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  Image,
  Dimensions,
} from "react-native";
import { useSafeAreaInsets } from "react-native-safe-area-context";
import images from "res/images";
const ShowSearchBar = () => {
  const width = Dimensions.get("screen").width;

  const insets = useSafeAreaInsets();

  const navigation = useNavigation();

  return (
    <View
      style={[
        styles.container,
        { height: 56 + insets.top, paddingTop: insets.top, width },
      ]}
    >
      <TouchableOpacity
        style={[styles.backBtn]}
        onPress={() => navigation.goBack()}
      >
        <Image source={images.back} />
      </TouchableOpacity>
      <View style={[styles.textContainer, { width: width - 64 }]}>
        <TextInput
          style={styles.textInput}
          placeholder="Type name, or number of episode"
          allowFontScaling={true}
        />
        <View />
        <TouchableOpacity style={styles.clear}>
          <Image source={images.xmark} />
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default ShowSearchBar;

const styles = StyleSheet.create({
  container: {
    backgroundColor: "white",
    flexDirection: "row",
    alignItems: "center",
    paddingRight: 16,
    paddingLeft: 8,
  },
  backBtn: {
    width: 44,
    height: 44,
    alignItems: "center",
    justifyContent: "center",
  },
  textContainer: {
    backgroundColor: "#EEF4F5",
    height: 40,
    borderRadius: 8,
    justifyContent: "space-between",
    flexDirection: "row",
  },
  textInput: {
    marginLeft: 16,
    paddingRight: 36,
    // flexGrow: 1,
  },
  clear: {
    position: "absolute",
    right: 0,
    width: 40,
    height: 40,
    alignItems: "center",
    justifyContent: "center",
  },
});
