import {Box} from '@mobily/stacks';
import OfferItem from 'components/OfferItem';
import TournamentItem from 'components/TournamentItem';
import React, {useEffect, useRef, useState} from 'react';
import {
  Dimensions,
  FlatList,
  SectionList,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import Carousel, {Pagination} from 'react-native-snap-carousel';
import colors from 'res/colors';
import styled from 'styled-components';
import {getPageOfTournaments} from "../../api/api";
import images from "../../res/images";
import {decodeHtmlCharCodes} from "../../helpers/helpers";

const width = Dimensions.get('window').width;
const TournamentScreen = ({navigation}) => {
  const [activeIndex, setActiveIndex] = useState(0);
  const [tournaments, setTournaments] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [loading, setLoading] = useState(false);
  const [endOfList, setEndOfList] = useState(false);

  const carouselRef = useRef();

  const onPressItem = item => {
    navigation.navigate('TournamentDetail', {tournament: item});
  };

  const renderItem = ({item}) => {
    return <TournamentItem item={item} onPress={() => onPressItem(item)} />;
  };

  useEffect(() => {
    setTournaments([]);
    loadTournaments();
  }, []);

  const loadTournaments = async() => {
    if(!endOfList && !loading) {
      setLoading(true);
      
      const fetchedTournaments = await getPageOfTournaments(5, currentPage);
      setCurrentPage(currentPage + 1);
      setLoading(false);

      const events = fetchedTournaments.events;

      if(events.length > 0) {
        const t = tournaments;

        for(const o of events) {
          const title = decodeHtmlCharCodes(o.title); //titles[titles.length - 1];

          t.push({
            id: o.id,
            start_date: o.start_date,
            end_date: o.end_date,
            url: o.url,
            organizer: o.organizer ? o.organizer : [],
            title: title,
            images: o.image ? [o.image] : [images.tournamentDefault],
            tags: o.tags ? o.tags.map((tag) => (tag.name)) : [],
            description: o.description
          });
        }

        setTournaments([...t]);
      } else {
        setEndOfList(true);
      }
    }
  };

  return (
    <>
      <FlatList
            data={tournaments}
            keyExtractor={(item) => item.id.toString()}
            contentContainerStyle={{
              justifyContent: 'center',
              padding: 10,
            }}
            vertical
            showsHorizontalScrollIndicator={false}
            onEndReached={() => { loadTournaments() }}
            renderItem={({ item }) => {
              return (
                  <TournamentItem
                      key={item.id}
                      item={item}
                      hideImage={true}
                      onPress={() => {
                        navigation.navigate("TournamentDetail", {
                          tournament: item,
                        });
                      }}
                  />
              )
          }}
        ItemSeparatorComponent={() => {
          return <View style={{ width: 27, height: 20 }} />;
        }}
        />
    </>
  );
};

export default TournamentScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.mainBack,
  },
  contentContainer: {
    paddingHorizontal: 20,
    paddingVertical: 20,
  },
  sectionTitle: {
    color: '#1A284F',
    fontWeight: 'bold',
    fontSize: 21,
  },
  viewMore: {
    color: '#02C0DE',
    fontFamily: 'Lato_400Regular',
    fontSize: 14,
  },
});

const SectionContainer = styled.View`
  padding: 29px 22px 17px 30px;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
`;
