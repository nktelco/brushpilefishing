import { Ionicons } from "@expo/vector-icons";
import { Box, Stack } from "@mobily/stacks";
import CrappieInfoCard from "components/CrappieInfoCard";
import React, { useEffect, useRef, useState } from "react";
import {
  ActivityIndicator,
  Dimensions, FlatList,
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import Carousel, { Pagination } from "react-native-snap-carousel";
import colors from "res/colors";
import images from "res/images";
import { loadCrappieInfos } from "../../api/api";
import CrappieInfoFeaturedCard from "./components/CrappieInfoFeaturedCard";
const width = Dimensions.get("window").width;
const CrappieInfoScreen = ({ navigation }) => {
  React.useLayoutEffect(() => {
    navigation.setOptions({
      headerRight: (props) => (
          <TouchableOpacity {...props} style={{marginRight: 26}}>
            {
              /*
              <Ionicons name="search-outline" size={26} color={colors.primary}/>
               */
            }
          </TouchableOpacity>
      ),
    });
  }, [navigation]);

  const [activeIndex, setActiveIndex] = useState(1);
  const [isLoading, setIsLoading] = useState(false);
  const [crappieInfo, setCrappieInfo] = useState([])
  const [endOfList, setEndOfList] = useState(false);

  const carouselRef = useRef();

  useEffect(() => {
    loadInfos();
  }, []);

  const loadInfos = async () => {
    if(!isLoading && !endOfList) {
      try {
        setIsLoading(true);
        const result = await loadCrappieInfos(5, activeIndex);
        if(result.length == 0) {
          setEndOfList(true);
        } else {

          setActiveIndex(activeIndex + 1);
          const newCrappieInfo = [...crappieInfo];

          for(const r of result) {
            newCrappieInfo.push(r);
          }
          setCrappieInfo(newCrappieInfo);
          setIsLoading(false);
        }
      } catch (error) {
        console.log("ERROR FETCHING INFO");
        console.log(error);
        setEndOfList(true);
      }
    }
  };

  return (
      <FlatList
          data={crappieInfo}
          keyExtractor={(item) => item.id.toString()}
          contentContainerStyle={{
            justifyContent: 'center',
            padding: 10,
          }}
          vertical
          showsHorizontalScrollIndicator={false}
          onEndReached={() => {
            loadInfos()
          }}
          renderItem={({item}) => {
            return (
                <CrappieInfoCard key={item.id} guide={item}/>
            )
          }}
          ItemSeparatorComponent={() => {
            return <View style={{width: 27, height: 20}}/>;
          }}
      ></FlatList>
  );
}

export default CrappieInfoScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.mainBack,
  },
  contentContainer: {
    paddingHorizontal: 20,
    paddingVertical: 20,
  },
  sectionTitle: {
    color: "#1A284F",
    fontWeight: "bold",
    fontSize: 14,
  },
  viewMore: {
    color: "#02C0DE",
    fontFamily: "Lato_400Regular",
    fontSize: 14,
  },
});

const FEATURED = [
  {
    title: "Item 1",
    text: "Text 1",
  },
  {
    title: "Item 2",
    text: "Text 2",
  },
  {
    title: "Item 3",
    text: "Text 3",
  },
];

const items = [
  {
    id: "1",
    title: "Item text 1",
    image: "https://picsum.photos/id/1/200",
    score: 1,
    author: {
      id: "1",
      name: "Paul Noah",
      avatar: "https://picsum.photos/id/1/40",
    },
  },
  {
    id: "2",
    title: "Item text 2",
    image: "https://picsum.photos/id/10/200",
    score: 1,
    author: {
      id: "1",
      name: "Paul Noah",
      avatar: "https://picsum.photos/id/1/40",
    },
  },

  {
    id: "3",
    title: "Item text 3",
    image: "https://picsum.photos/id/1002/200",
    score: 1,
    author: {
      id: "1",
      name: "Paul Noah",
      avatar: "https://picsum.photos/id/1/40",
    },
  },
  {
    id: "4",
    title: "Item text 4",
    image: "https://picsum.photos/id/1006/200",
    score: 1,
    author: {
      id: "1",
      name: "Paul Noah",
      avatar: "https://picsum.photos/id/1/40",
    },
  },
  {
    id: "5",
    title: "Item text 5",
    score: 1,
    image: "https://picsum.photos/id/1008/200",
    author: {
      id: "1",
      name: "Paul Noah",
      avatar: "https://picsum.photos/id/1/40",
    },
  },
];

const SECTIONS = [
  {
    title: "Category #1",
    data: items,
  },
  {
    title: "Category #2",
    data: items,
  },
  {
    title: "Category #3",
    data: items,
  },
];
