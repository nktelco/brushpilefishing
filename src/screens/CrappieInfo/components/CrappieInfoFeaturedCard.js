import {Box, Stack} from '@mobily/stacks';
import React from 'react';
import {TouchableWithoutFeedback, Text, View, StyleSheet} from 'react-native';
import styled from 'styled-components';

const CrappieInfoFeaturedCard = ({onPress}) => {
  return (
    <TouchableWithoutFeedback onPress={onPress}>
      <Container>
        <PreviewImage source={{uri: 'https://picsum.photos/200/300'}} />
        <View
          style={{
            flexDirection: 'column',
            justifyContent: 'flex-end',
            alignItems: 'center',
            height: '100%',
            // backgroundColor: 'red',
          }}>
          <Stack space={8} align="start" paddingBottom={16} paddingX={20}>
            <Text style={[styles.text, styles.offer]}>American Expedition</Text>
            <Text style={[styles.text, styles.title]}>
              Crappie Information, Photos {'\n'}Artwork, and Facts
            </Text>
          </Stack>
        </View>
      </Container>
    </TouchableWithoutFeedback>
  );
};

export default CrappieInfoFeaturedCard;

const styles = StyleSheet.create({
  text: {
    color: 'white',
    fontWeight: 'bold',
  },
  title: {
    fontSize: 18,
  },
  offer: {
    fontSize: 13,
  },
});

const Container = styled.View`
  height: 255px;
`;

const PreviewImage = styled.Image`
  height: 255px;
  position: absolute;
  top: 0px;
  left: 0px;
  right: 0px;
  bottom: 0px;
  overflow: hidden;
  border-radius: 16px;
`;
