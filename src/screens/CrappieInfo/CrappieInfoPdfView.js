import React from "react";
import {
  Dimensions,
  StyleSheet,
  View,
  Image,
  ScrollView,
  SafeAreaView,
} from "react-native";
import PDFReader from "rn-pdf-reader-js";
import images from "res/images";
import RenderHtml, {
  HTMLContentModel,
  defaultHTMLElementModels,
} from "react-native-render-html";
import colors from "../../res/colors";
const CrappieInfoPdfView = ({ navigation, route }) => {
  React.useLayoutEffect(() => {
    navigation.setOptions({
      title: "Crappie Facts",
      headerBackImage: ({ props }) => {
        return <Image source={images.xmark} {...props} />;
      },
    });
  }, [navigation]);

  const width = Dimensions.get("window").width;

  const content = route.params;

  const source = {
    html: `${content.guide}`,
  };

  const tagsStyles = {
    body: {
      whiteSpace: "normal",
      color: colors.blackText,
      fontFamily: "HelveticaNeue-Regular",
      fontSize: 16,
      margin: 20,
    },
    img: {
      marginHorizontal: 20,
      width: "50%",
    },
    a: {
      color: colors.blackText,
      fontFamily: "HelveticaNeue-Regular",
      fontSize: 16,
      marginTop: 10,
      marginBottom: 20,
      textDecorationLine: "none",
      textAlign: "center",
    },
    ul: {
      listStyleType: "none",
    },
  };

  const customHTMLElementModels = {
    img: defaultHTMLElementModels.img.extend({
      contentModel: HTMLContentModel.mixed,
    }),
  };

  return (
    // <View style={styles.container}>
    //   <PDFReader
    //     source={{
    //       uri: 'http://www.orimi.com/pdf-test.pdf',
    //     }}
    //     style={styles.pdf}
    //   />
    // </View>
    <SafeAreaView>
      <ScrollView>
        <RenderHtml
          contentWidth={width}
          source={source}
          tagsStyles={tagsStyles}
          customHTMLElementModels={customHTMLElementModels}
        />
      </ScrollView>
    </SafeAreaView>
  );
};

export default CrappieInfoPdfView;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  pdf: {
    flex: 1,
    width: Dimensions.get("window").width,
    height: Dimensions.get("window").height,
  },
});
