import React from 'react';
import {FlatList, StyleSheet, View} from 'react-native';
import styled from 'styled-components';
import CrappieInfoCard from 'components/CrappieInfoCard';
import colors from 'res/colors';

const CrappieInfoCategoryScreen = ({route, navigation}) => {
  const {category} = route.params;
  React.useLayoutEffect(() => {
    navigation.setOptions({
      title: category.title,
    });
  }, [navigation]);

  const ListItem = ({item}) => {
    return <CrappieInfoCard guide={item} />;
  };
  return (
    <Container>
      <FlatList
        contentContainerStyle={{
          paddingHorizontal: 20,
          paddingVertical: 16,
        }}
        data={category.data}
        renderItem={({item}) => <ListItem item={item} />}
        showsHorizontalScrollIndicator={false}
        ItemSeparatorComponent={() => {
          return <View style={{height: 10}} />;
        }}
      />
    </Container>
  );
};

export default CrappieInfoCategoryScreen;

const styles = StyleSheet.create({});
const Container = styled.View`
  flex: 1;
  background-color: ${colors.mainBack};
`;
