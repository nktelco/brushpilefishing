import React from "react";
import { StyleSheet, Text, TouchableOpacity, View } from "react-native";
import { getColorForTemp, getWeatherSymbol } from "../../../helpers/helpers";
import {
  DayCloud,
  DayCloudRain,
  DayCloudSnow,
  DayCloudSun,
  DayCloudThunder,
} from "../../../res/svgs";
const WeatherLocationCard = ({ item, onPress }) => {
  const renderWeather = () => {
    console.log(item);
    return getWeatherSymbol(item.weather);
  };

  return (
    <TouchableOpacity
      onPress={onPress}
      style={[
        styles.container,
        { backgroundColor: getColorForTemp(item.temp) },
      ]}
    >
      <View style={styles.innerContainer}>
        <Text style={styles.location}>{item.name}</Text>
        <Text style={styles.secondary_location}>{item.sec}</Text>
        {
          //<Text style={styles.weatherLabel}>{item.weather}</Text>
        }
      </View>
      {
        //<View style={styles.weatherSymbol}>{renderWeather()}</View>
      }
      {
        //<Text style={styles.temperature}>{Math.round(item.temp)}°</Text>
      }
    </TouchableOpacity>
  );
};

export default WeatherLocationCard;

const styles = StyleSheet.create({
  container: {
    borderRadius: 8,
    height: 101,
    justifyContent: "center",
    paddingLeft: 20,
    overflow: "hidden",
  },
  innerContainer: {
    alignItems: "flex-start",
    justifyContent: "center",
  },
  location: {
    color: "white",
    fontSize: 18,
    fontWeight: "bold",
    marginBottom: 4,
  },
  secondary_location: {
    color: "white",
    fontSize: 14,
    fontWeight: "bold",
    marginBottom: 8,
  },
  temperature: {
    color: "white",
    fontSize: 48,
    fontWeight: "bold",
    position: "absolute",
    right: 64,
    top: 20,
  },
  weatherLabel: {
    color: "white",
    fontWeight: "bold",
    fontSize: 13,
  },
  weatherSymbol: {
    position: "absolute",
    top: 15,
    overflow: "hidden",
    right: -28,
    zIndex: 1,
    width: 80,
    height: 80,
  },
});
