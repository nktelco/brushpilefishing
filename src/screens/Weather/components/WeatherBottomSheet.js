import { BottomSheetBackdrop, BottomSheetModal } from "@gorhom/bottom-sheet";
import React, {
  useCallback,
  useContext,
  useImperativeHandle,
  useMemo,
  useRef,
  useState,
} from "react";
import { FlatList, StyleSheet, View, Text, Alert } from "react-native";
import { ActivityIndicator } from "react-native-paper";
import { useSafeAreaInsets } from "react-native-safe-area-context";
import { LocationContext } from "../../../context/LocationProvider";
import LocationSheetHandle from "./LocationSheetHandle";
import useWeatherOfLakes from "./useWeatherOfLakes";
import WeatherLocationCard from "./WeatherLocationCard";
import {searchForPlaces, getLocationDetails} from "../../../api/api";
const WeatherBottomSheet = React.forwardRef((props, ref) => {
  const snapPoints = useMemo(() => ["80%"], []);

  const { setLocation, getWeatherData } = useContext(LocationContext);

  const bottomSheetRef = useRef();

  const insets = useSafeAreaInsets();

  const [isLoading, locations] = useWeatherOfLakes();

  const [keyword, setKeyword] = useState("");
  const [values, setValues] = useState([]);

  useImperativeHandle(ref, () => ({
    showSearch() {
      handleSnapPress(1);
    },
  }));

  const handleSnapPress = useCallback((index) => {
    bottomSheetRef.current?.present();
  }, []);

  const handleClosePress = useCallback(() => {
    bottomSheetRef.current?.close();
  }, []);

  let timeout;

  const handleSearch = (value) => {
    setKeyword(value);
    if(timeout) {
      clearTimeout(timeout);
    }

    timeout = setTimeout(async () => {
      try {
        const predictions = await searchForPlaces(value);
        console.log(predictions);

        let n = [];
        if (predictions.predictions) {
          for (const p of predictions.predictions) {
            console.log(p);
            n.push({
              name: p.structured_formatting.main_text,
              sec: p.structured_formatting.secondary_text,
              place_id: p.place_id
            });
          }
          setValues(n);
        }
      } catch(e) {
        console.log("ERROR");
        console.log(e);
      }
    }, 1500);
  }

  const renderBackdrop = useCallback(
    (props) => (
      <BottomSheetBackdrop
        {...props}
        disappearsOnIndex={-1}
        appearsOnIndex={0}
        closeOnPress={true}
      />
    ),
    []
  );

  const renderHeader = useCallback(() => {
    return <LocationSheetHandle text={keyword} handleSearch={handleSearch} />;
  }, []);

  const renderSheetItem = ({ item }) => {
    return (
      <WeatherLocationCard
        item={item}
        onPress={() => {
          const place_id = item.place_id;
          getLocationDetails(place_id).then((result) => {
            const loc = (result?.result?.geometry?.location);
            setLocation({ lat: loc.lat, long: loc.lng, name: item.name });
            getWeatherData(true);
          }).catch((e) =>{
            Alert.alert("Location Error", "error getting location");
          })
          handleClosePress();
        }}
      />
    );
  };

  const renderListSeparator = () => {
    return <View style={{ height: 8 }} />;
  };

  const renderEmptyView = () => {
    return (
      <View
        style={[
          {
            justifyContent: "center",
            alignItems: "center",
            flex: 1,
          },
        ]}
      >
        {isLoading && (
          <ActivityIndicator size="large" color={colors.primary} />
        )}
      </View>
    );
  };

  return (
    <BottomSheetModal
      ref={bottomSheetRef}
      snapPoints={snapPoints}
      enablePanDownToClose={true}
      handleComponent={renderHeader}
      backdropComponent={renderBackdrop}
    >
      <View
        style={[
          styles.locationListContentContainer,
          { marginBottom: insets.bottom },
        ]}
      >
        <FlatList
          data={values}
          keyExtractor={(item) => item.place_id}
          renderItem={renderSheetItem}
          ItemSeparatorComponent={renderListSeparator}
          ListEmptyComponent={renderEmptyView}
        />
      </View>
    </BottomSheetModal>
  );
});

export default WeatherBottomSheet;

const styles = StyleSheet.create({
  locationListContentContainer: {
    paddingHorizontal: 20,
    paddingVertical: 18,
    flex: 1,
  },
});
