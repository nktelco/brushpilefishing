import {Box, Stack} from '@mobily/stacks';
import React from 'react';
import {View, Image} from 'react-native';
import styled from 'styled-components';
import images from '../../../res/images';
const WeatherParamCard = ({param, value}) => {
  const image =
    param === 'Wind'
      ? images.weather_wind_speed
      : param === 'Humidity'
      ? images.weather_humidity
      : images.weather_chance_of_rain;

  return (
    <Box>
      <Stack align="center" space={6}>
        <Image style={{width: 34, height: 34}} source={image} />
        <Stack align="center" space={3}>
          <Value>{value}</Value>
          <Param>{param}</Param>
        </Stack>
      </Stack>
    </Box>
  );
};

export default WeatherParamCard;

const Value = styled.Text`
  font-family: Roboto-Bold;
  font-style: normal;
  font-weight: 500;
  font-size: 16px;
  color: #011e48;
`;

const Param = styled.Text`
  font-family: Lato_400Regular;
  font-size: 13px;
  color: #535d7a;
`;
