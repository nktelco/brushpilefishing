import React, { useState, useEffect } from "react";
import { getLakesWeather } from "../../../api/weather";
import { lakes } from "../../../../assets/data/lakes";
const useWeatherOfLakes = () => {
  const [locations, setLocations] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [lastUpdate, setLastUpdate] = useState(null);

  useEffect(() => {
    let isMounted = true;
    setIsLoading(true);
    getLakesWeather(lakes)
      .then((locations) => {
        if (isMounted === true) {
          setLocations(locations);
        }
      })
      .finally(() => {
        if (isMounted === true) {
          setIsLoading(false);
        }
      });
    return () => {
      isMounted = false;
    };
  }, []);

  return [isLoading, locations];
};

export default useWeatherOfLakes;
