import { Stack } from "@mobily/stacks";
import { LinearGradient } from "expo-linear-gradient";
import React, { useContext } from "react";
import { StyleSheet, Text, View, Dimensions } from "react-native";
import colors, { timeOfDayGradientColors } from "res/colors";
import { LocationContext } from "../../../context/LocationProvider";
import { getWeatherSymbol } from "../../../helpers/helpers";

const WeatherHeader = () => {
  const width = Dimensions.get("screen").width;

  const { currentWeather } = useContext(LocationContext);
  const timeArrayEntry = Math.floor(new Date().getHours() * 2);

  return (
    <View style={styles.banner}>
      <LinearGradient
        colors={[
          timeOfDayGradientColors[timeArrayEntry],
          timeOfDayGradientColors[timeArrayEntry + 1],
        ]}
        style={styles.gradient}
      />

      <View style={styles.weatherSunCloud}>
        {getWeatherSymbol(currentWeather?.current?.weather[0].main)}
      </View>
      <View style={{ position: "absolute", bottom: "12%", right: "12%" }}>
        <Stack align="bottom">
          <Text style={[styles.temperature, { fontSize: (107 * width) / 375 }]}>
            {currentWeather?.current?.temp ? Math.ceil(currentWeather?.current?.temp) : "--"}°
          </Text>
          <Text style={styles.weatherLabel}>
            {currentWeather?.current?.weather[0].main ?? "Unknown"}
          </Text>
        </Stack>
      </View>
    </View>
  );
};

export default WeatherHeader;

const styles = StyleSheet.create({
  banner: {
    borderBottomLeftRadius: 32,
    borderBottomRightRadius: 32,
    backgroundColor: colors.mainBack,
    overflow: "hidden",
    flex: 1,
  },
  gradient: {
    position: "absolute",
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
  },
  weatherSunCloud: {
    position: "absolute",
    right: "54%",
    top: 120,
    width: 180,
    height: 180,
  },
  temperature: {
    fontWeight: "bold",
    color: "white",
    fontFamily: "HelveticaNeue-Bold",
  },
  weatherLabel: {
    fontWeight: "bold",
    fontSize: 21,
    color: "white",
    fontFamily: "HelveticaNeue-Bold",
  },
});
