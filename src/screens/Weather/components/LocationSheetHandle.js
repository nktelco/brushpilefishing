import { AntDesign } from "@expo/vector-icons";
import React, { memo, useCallback, useState } from "react";
import {
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from "react-native";
import isEqual from "lodash.isequal";
const LocationSheetHandle = ({ text, handleSearch }) => {
  const handleInputChange = useCallback(({ nativeEvent: { text } }) => {
    handleSearch(text);
  }, []);
  return (
    <View>
      <View style={{ marginTop: 16, alignItems: "center" }}>
        <View style={styles.handle} />
        <Text style={styles.searchTitle}>Search Location</Text>
      </View>
      <View style={styles.searchBox}>
        <TextInput
          autoCapitalize="none"
          autoCorrect={false}
          onChange={handleInputChange}
          style={styles.searchField}
          defaultValue={text}
          status="info"
          placeholder="Search Location"
          textStyle={{ color: "#000" }}
        />
        <TouchableOpacity style={styles.clearButton}>
          <AntDesign name="close" size={16} color={colors.primary} />
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default memo(LocationSheetHandle, isEqual);

const styles = StyleSheet.create({
  handle: {
    backgroundColor: "#CACDD6",
    borderRadius: 3,
    height: 6,
    width: 40,
  },
  searchTitle: {
    marginTop: 12,
    color: "#011E48",
    fontFamily: "Lato_400Regular",
    fontSize: 18,
  },
  searchBox: {
    backgroundColor: "#EEF4F5",
    borderRadius: 8,
    marginHorizontal: 20,
    marginTop: 18,
    height: 40,
    alignItems: "center",
    justifyContent: "space-between",
    flexDirection: "row",
  },
  searchField: {
    width: "100%",
    height: "100%",
    paddingLeft: 8,
  },
  clearButton: {
    position: "absolute",
    right: 4,
    top: 0,
    bottom: 0,
    width: 40,
    height: 40,
    alignItems: "center",
    justifyContent: "center",
  },
});
