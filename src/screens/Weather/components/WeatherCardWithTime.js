import moment from "moment";
import React from "react";
import { StyleSheet, Text, View } from "react-native";
import { BottomRadial } from "res/svgs";
import colors, { timeOfDayGradientColors } from "res/colors";
import { getColorForTemp, getWeatherSymbol } from "../../../helpers/helpers";
import {LinearGradient} from 'expo-linear-gradient';
const WeatherCardWithTime = ({ item }) => {
  const renderWeather = () => {
    return getWeatherSymbol(item.weather[0].main);
  };

  const time = moment.unix(item.dt).format("h:mm A");
  const timeArrayEntry = Math.floor(moment.unix(item.dt).hour() * 2);

  return (
    <View style={{ alignItems: "center" }}>
      <View
        style={[
          styles.topContainer,
          {
            //backgroundColor: getColorForTemp(item.temp)
          },
        ]}
      >
        <LinearGradient
            colors={[
              timeOfDayGradientColors[timeArrayEntry],
              timeOfDayGradientColors[timeArrayEntry + 1],
            ]}
            style={styles.gradient}
        />
        <Text style={styles.temperature}>{Math.round(item.temp)}°</Text>
        <View style={styles.weatherSymbol}>{renderWeather()}</View>
      </View>
      <Text style={styles.time}>{time}</Text>
      <View style={styles.bottomRadial}>
        <BottomRadial />
      </View>
    </View>
  );
};

export default WeatherCardWithTime;

const styles = StyleSheet.create({
  topContainer: {
    borderRadius: 8,
    width: 69,
    height: 76,
    alignItems: "center",
    overflow: "hidden",
  },
  temperature: {
    color: "white",
    fontSize: 28,
    fontWeight: "bold",
    paddingTop: 3,
  },
  time: {
    color: "#535D7A",
    fontWeight: "500",
    fontSize: 14,
    paddingTop: 5,
    paddingBottom: 5,
  },
  weatherSymbol: {
    position: "absolute",
    // top: 34,
    overflow: "hidden",
    width: 56,
    height: 56,
    bottom: -15,
  },
  bottomRadial: {
    position: "absolute",
    bottom: 0,
    left: 0,
    right: 0,
    zIndex: -1,
  },
  gradient: {
    position: "absolute",
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
  },
});
