import {Box, Stack} from '@mobily/stacks';
import React from 'react';
import {Image, Text, View, StyleSheet} from 'react-native';
import images from 'res/images';
import styled from 'styled-components';
const FishingTipCard = () => {
  return (
    <View style={styles.container}>
      <Image source={images.fishing_tips} style={styles.image} />
      <View style={styles.textContainer}>
        <Stack space={7}>
          <Text style={{fontSize: 14, fontWeight: 'bold', color: '#1A284F'}}>
            Fishing Tips
          </Text>
          <Text
            style={{
              height: 69,
              fontFamily: 'Lato_400Regular',
              fontSize: 14,
              color: '#1A284F',
            }}>
            It’s rainy and warm outside, you might try fishing with a fishing
            pole and maybe grabbing some lunch.
          </Text>
        </Stack>
      </View>
    </View>
  );
};

export default FishingTipCard;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    paddingTop: 30,
    paddingHorizontal: 20,
    paddingBottom: 26,
  },
  textContainer: {
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'flex-start',
    marginLeft: 89,
  },
  image: {
    width: 69,
    height: 105,
    position: 'absolute',
    left: 20,
    top: 24,
  },
});
