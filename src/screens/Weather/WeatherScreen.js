import { AntDesign } from "@expo/vector-icons";
import { Box, Stack } from "@mobily/stacks";
import moment from "moment";
import React, {useCallback, useContext, useEffect, useRef} from 'react';
import {
  ActivityIndicator,
  FlatList,
  RefreshControl,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import { useSafeAreaInsets } from "react-native-safe-area-context";
import { StretchyScrollView } from "react-native-stretchy";
import colors from "res/colors";
import { LocationContext } from "../../context/LocationProvider";
import FishingTipCard from "./components/FishingTipCard";
import WeatherBottomSheet from "./components/WeatherBottomSheet";
import WeatherCardWithTime from "./components/WeatherCardWithTime";
import WeatherHeader from "./components/WeatherHeader";
import WeatherParamCard from "./components/WeatherParamCard";
const WeatherScreen = ({ navigation }) => {
  const ref = useRef(null);
  const inset = useSafeAreaInsets();

  const {
    refreshing,
    isLoading,
    loadedWeather,
    loadedForecast,
    location,
    currentWeather,
    getWeatherData,
    lastUpdate,
  } = useContext(LocationContext);
  const now = currentWeather.current.dt
    ? moment(currentWeather.current.dt * 1000)
    : moment();
  const renderItem = ({ item }) => {
    return <WeatherCardWithTime item={item} />;
  };

  const renderSeparator = () => {
    return <View style={{ width: 14 }} />;
  };

  const { hourly } = currentWeather;

  const onRefresh = useCallback(() => {
    getWeatherData();
  }, [location]);


  useEffect(() => {
    getWeatherData()
  }, [location]);


  const windSpeed =
    Math.round((currentWeather?.current?.wind_speed ?? 0));

  const humidity = currentWeather?.current?.humidity ?? 0;

  const clouds = currentWeather?.current?.clouds ?? 0;

  const navToDetail = () => {
    navigation.navigate("WeatherDetail", { location: location });
  };

  const showSearch = () => {
    ref?.current.showSearch();
  };

  return (
    <View style={styles.container}>
      <Stack
        space={8}
        align="center"
        paddingTop={inset.top}
        style={{ zIndex: 100, position: "absolute" }}
      >
        <Text style={styles.bannerDate}>{now.format("ll")}</Text>
        <TouchableOpacity onPress={showSearch}>
          <Stack horizontal align="center" space={12}>
            <Text style={styles.bannerPlace}>{location.name}</Text>
            <AntDesign name="down" size={18} color="white" />
          </Stack>
        </TouchableOpacity>
      </Stack>
      <StretchyScrollView
        nestedScrollEnabled={true}
        imageOverlay={<WeatherHeader />}
        imageHeight={399}
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
        }
      >
        <Box
          direction="row"
          alignX="between"
          paddingLeft={38}
          paddingRight={27}
          paddingTop={22}
          paddingBottom={lastUpdate ? 44 : 24}
          style={{ width: "100%", backgroundColor: colors.mainBack }}
        >
          <WeatherParamCard param="Wind" value={windSpeed ? `${windSpeed} mph` : "--"} />
          <WeatherParamCard param="Humidity" value={humidity ? `${humidity}%` : "--"} />
          <WeatherParamCard param="Chance of rain" value={clouds ? `${clouds}%` : "--"} />
          <Text style={styles.lastUpdate}>
            Updated at {moment(lastUpdate).format("HH:mm")}
          </Text>
        </Box>
        <Box style={styles.card}>
          <Box
            direction="row"
            alignX="between"
            paddingY={17}
            paddingLeft={20}
            paddingRight={19}
          >
            <Text style={styles.today}>TODAY</Text>
            <TouchableOpacity onPress={navToDetail}>
              <Stack horizontal space={4} align="center">
                <Text style={styles.weekMore}>7 days</Text>
                <AntDesign name="right" size={17} color={colors.primary} />
              </Stack>
            </TouchableOpacity>
          </Box>
          <View
            style={{
              backgroundColor: "rgba(0, 0, 0, 0.1)",
              height: 1,
              width: "100%",
            }}
          />
          <FlatList
            data={hourly.slice(0, Math.min(24, hourly.length))}
            contentContainerStyle={{
              paddingHorizontal: 20,
              paddingTop: 15,
              paddingBottom: 24,
            }}
            keyExtractor={(item) => item.dt.toString()}
            renderItem={renderItem}
            horizontal
            showsHorizontalScrollIndicator={false}
            ItemSeparatorComponent={renderSeparator}
          />
        </Box>
        <View style={[styles.card, { marginTop: 8, marginBottom: 8 }]}>
          {
            //<FishingTipCard/>
          }
        </View>
      </StretchyScrollView>
      <WeatherBottomSheet ref={ref} />
    </View>
  );
};

export default WeatherScreen;

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.mainBack,
    flex: 1,
  },

  today: {
    fontFamily: "Lato_700Bold",
    fontSize: 14,
    color: "#1A284F",
  },
  weekMore: {
    fontFamily: "Lato_400Regular",
    fontSize: 14,
    color: colors.primary,
  },
  card: { backgroundColor: "white", borderRadius: 8 },
  lastUpdate: {
    position: "absolute",
    fontSize: 18,
    bottom: 10,
    right: 10,
  },
  bannerDate: {
    fontFamily: "Lato_400Regular",
    fontSize: 18,
    color: "white",
  },
  bannerPlace: {
    fontFamily: "Lato_700Bold",
    fontSize: 18,
    color: "white",
  },
});
