import { Box, Stack } from "@mobily/stacks";
import { LinearGradient } from "expo-linear-gradient";
import React, {useEffect, useState} from 'react';
import { Image, ScrollView, StyleSheet, Text, View, Linking } from "react-native";
import colors from "res/colors";
import images from "res/images";
import Email from "res/svgs/ic_email.svg";
import Phone from "res/svgs/ic_phone.svg";
import {getGuideAndFishingSpotsById} from '../../api/api';
import {getBpfApiPath} from "../../api/api";
import {Link} from '@react-navigation/native';

const GuideDetailScreen = ({ route, navigation }) => {
  const { guide } = route.params;
  const image = `${getBpfApiPath()}/${guide.image}`;

  const [guideData, setGuideData] = useState({});
  navigation.setOptions({title: "Guide Details"})

  useEffect(() => {
    getGuideAndFishingSpotsById(guide.id).then((result) => {
      setGuideData(result[0]);
    }).catch((e) => {
      console.log(e);
    })
  }, [])

  return (
    <ScrollView style={styles.container}>
      <Box style={styles.topContainer}>
        <Stack space={9}>
          <View
            style={{ flexDirection: "row", justifyContent: "space-between" }}
          >
          </View>
          <Text style={styles.guideTitle}>
              {
                guideData ? guideData.name : "loading"
              }
          </Text>
          <Text style={styles.guideServicing}>
            Servicing:
          </Text>
              {
                (guideData.fishingSpots || [{name: "loading"}]).map((fishingSpot) => {
                  return(
                      <Text style={styles.guideLakes}>
                        {fishingSpot.name}
                      </Text>)
                })
              }
        </Stack>
      </Box>
      {
        <View
            style={{
              flexDirection: "row",
              paddingHorizontal: 20,
              alignItems: "center",
              paddingVertical: 8,
            }}
        >
          <Stack horizontal space={12} align="center" style={{flexGrow: 1}}>
            {
              <LinearGradient
                  colors={[colors.primary, "white"]}
                  style={{
                    alignItems: "center",
                    justifyContent: "center",
                    width: 80,
                    height: 80,
                    borderRadius: 40,
                  }}
              >
                <Image source={{uri: image}} style={styles.avatar}/>
              </LinearGradient>
            }
            <Stack>
              <Text style={styles.title}>{guide?.title}</Text>
              <Text style={styles.author}>{guide?.author?.name}</Text>
            </Stack>
          </Stack>
          {
            <Text>
              {
                  guide.phone &&
                  <Phone width={40} height={40} onPress={() => {
                    Linking.openURL(`tel:${guide?.phone}`);
                  }}/>
              }
            </Text>
          }

          {
            <View style={{width: 24}}/>
          }
          {
              <Text>
                {
                  guide?.email &&
                    <Email width={40} height={40} onPress={() => {
                      Linking.openURL(`mailto:${guide?.email}`);
                    }}/>
                }
              </Text>
          }
        </View>
      }
      <View style={styles.bottomContainer}>
        <Stack space={24}>
          <Stack space={8}>
            <Text style={styles.sectionTitle}>BIO</Text>
            <Text style={styles.sectionBody}>
              {guideData.bio}
            </Text>
          </Stack>
          <Stack space={8}>
            <Text style={styles.sectionTitle}>GUIDE DETAILS</Text>
            <Text style={styles.sectionBody}>
              {guideData.details}
            </Text>
          </Stack>
        </Stack>
      </View>
    </ScrollView>
  );
};

export default GuideDetailScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.mainBack,
  },
  topContainer: {
    borderRadius: 16,
    backgroundColor: "white",
    paddingHorizontal: 20,
    paddingVertical: 24,
    marginTop: 8,
  },
  bottomContainer: {
    borderRadius: 16,
    paddingHorizontal: 20,
    paddingVertical: 24,
    backgroundColor: "white",
    paddingBottom: 94,
    marginBottom: 32,
  },
  avatar: {
    width: 80,
    height: 80,
    borderRadius: 40,
  },
  title: {
    fontFamily: "Lato_700Bold",
    fontSize: 16,
    color: colors.blackText,
  },
  author: {
    fontFamily: "Lato_400Regular",
    fontSize: 14,
    color: colors.primary,
  },
  sectionTitle: {
    fontFamily: "HelveticaNeue-Bold",
    fontSize: 13,
    color: colors.blackText,
  },
  sectionBody: {
    fontFamily: "Lato_400Regular",
    fontSize: 14,
    color: colors.blackText,
  },
  date: {
    fontFamily: "Lato_400Regular",
    fontSize: 14,
    color: colors.grayText,
  },
  score: {
    fontFamily: "Lato_400Regular",
    fontSize: 14,
    color: colors.grayText,
  },
  guideTitle: {
    fontFamily: "HelveticaNeue-Bold",
    fontSize: 28,
    color: colors.blackText,
  },
  guideServicing: {
    fontFamily: "HelveticaNeue-Bold",
    fontSize: 22,
    color: colors.primary,
    lineHeight: 22,
  },
  guideLakes: {
    fontFamily: "HelveticaNeue-Bold",
    fontSize: 20,
    color: colors.blackText,
    lineHeight: 22,
  },
  image: {
    width: 54,
    height: 54
  }
});
