import {Box, Stack, useStacks} from '@mobily/stacks';
import React, {useContext, useEffect, useLayoutEffect, useRef, useState} from 'react';
import {
  FlatList,
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  ActivityIndicator,
  Platform,
  Alert,
} from "react-native";
import { startActivityAsync, ActivityAction } from 'expo-intent-launcher';
import { openURL } from 'expo-linking';
import colors from "res/colors";
import guides from "../../../assets/data/guides";
import GuideCard from "./components/GuideCard";
import {AntDesign, Ionicons} from '@expo/vector-icons';
import { LocationContext } from "../../context/LocationProvider";
import {getNearestGuides} from "../../api/api";
import BookItem from "../Episode/components/BookItem";
import WeatherBottomSheet from '../Weather/components/WeatherBottomSheet';

const GuideScreen = ({ navigation }) => {

  const { isLoading, currentWeather, location, locationPermission } = useContext(LocationContext);
  const [guideList, setGuideList] = useState([]);
  const [guideIdList, setGuideIdList] = useState([]);
  const [loadingGuides, setLoadingGuides] = useState(false);
  const [guidePage, setGuidePage] = useState(0);
  const [endOfList, setEndOfList] = useState(false);
  const [showingAlert, setShowingAlert] = useState(false);
  const ref = useRef(null);

  useLayoutEffect(() => {
    navigation.setOptions({
      title: "Find Guides",
      headerRight: (props) => (
        <TouchableOpacity {...props} style={{ marginRight: 26 }}>
          {
            /*
            <Ionicons name="search-outline" size={26} color={colors.primary}/>
            */
          }
        </TouchableOpacity>
      ),
    });
  }, [navigation]);

  useEffect(() => {
    if(!isLoading) {
      loadGuides(true);
    }
  }, [isLoading, location])

  const loadGuides = async (forceLoad) => {
    if (!loadingGuides || forceLoad) {

      try {
        setLoadingGuides(true);
        const resultGuides = await getNearestGuides(
            location.lat, location.long, 15, forceLoad ? 0 : guidePage
        );

        if(resultGuides.length == 0 && !forceLoad) {
          setEndOfList(true);
        } else {
          setEndOfList(false);
        }

        setLoadingGuides(false);
        let gPage = forceLoad ? 0 : guidePage;
        gPage++;
        setGuidePage(gPage);

        const gList = forceLoad ? [] : [...guideList];
        const gidList = forceLoad ? [] : [...guideIdList];

        for (const item of resultGuides) {
          if (gidList.indexOf(item.id) < 0) {
            gidList.push(item.id);
            gList.push(item);
            console.log(item);
          }
        }

        setGuideIdList(gidList);
        setGuideList(gList);

      } catch (e) {
        console.log(e);
      }
    }
  }

  const renderItem = ({ item }) => {
    return <BookItem book={item} />;
  };

  const renderSeparator = () => {
    return <View style={{ height: 10 }} />;
  };

  const showSearch = () => {
    ref?.current.showSearch();
  };

  useEffect(() => {
    if (locationPermission === false) {
      if (!showingAlert) {
        setShowingAlert(true);
        Alert.alert(
            "Location Services",
            "You must enable Location Services to find guides",
        [
            {text: 'Cancel', onPress:() => {

            }},
            {text: 'Settings', onPress: () => {
              const platform = Platform.OS;
              if(platform == "android") {
                startActivityAsync("android.settings.LOCATION_SOURCE_SETTINGS")
              } else if(platform == "ios") {
                openURL('app-settings:').then((result) => {
                  console.log(result);
                }).catch((e) => {
                  console.log(e);
                })
              }
            }}
        ])

        setShowingAlert(false);
      } else {
        setShowingAlert(false);
      }
    }
  }, [locationPermission]);

  return (
    <View style={styles.container}>
      <Box
        paddingTop={16}
        paddingLeft={20}
        paddingRight={20}
        paddingBottom={21}
      >
        <Box direction="row" alignX="between">

          <Text style={styles.locationLabel}>Your Location</Text>

          <TouchableOpacity onPress={showSearch}>
            <Stack horizontal align="center" space={12}>
              <Text style={styles.locationValue}>
                {
                  isLoading ? "...locating" : location.name
                }
              </Text>
              <AntDesign name="down" size={18} style={styles.locationValue}/>
            </Stack>
          </TouchableOpacity>

        </Box>
      </Box>
      {
        guideList.length == 0 && loadingGuides ?
            <ActivityIndicator size="large" color={colors.primary} /> :
            guideIdList.length == 0 && !loadingGuides ?
            <View style={{
              paddingLeft: 20,
              paddingRight: 20,
              paddingBottom: 30
            }}>
              <Text style={styles.noGuides}>No guides near {location.name}</Text>
            </View> :
            <FlatList
                data={guideList}
                keyExtractor={(item) => item.id.toString()}
                contentContainerStyle={{
                  paddingLeft: 20,
                  paddingRight: 20,
                  paddingBottom: 30,
                }}
                renderItem={renderItem}
                ItemSeparatorComponent={renderSeparator}
                onEndReached={() => {
                  loadGuides()
                }}
            />
      }

    <WeatherBottomSheet ref={ref} />
    </View>
  );
};

export default GuideScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.mainBack,
  },
  locationLabel: {
    fontFamily: "Lato_400Regular",
    fontSize: 14,
    color: colors.blackText,
  },
  locationValue: {
    fontFamily: "Lato_400Regular",
    fontSize: 14,
    color: colors.primary,
  },
  noGuides: {
    fontFamily: "Lato_400Regular",
    fontSize: 22,
    color: colors.blackText,
  },
});
