import React from "react";
import { StyleSheet, Text, View, Image } from "react-native";
import { Box, Stack } from "@mobily/stacks";
import { LinearGradient } from "expo-linear-gradient";
import colors from "res/colors";
import { useNavigation } from "@react-navigation/core";
import { TouchableWithoutFeedback } from "react-native-gesture-handler";
const GuideCard = ({ guide }) => {
  const navigation = useNavigation();
  const onPress = () => {
    navigation.navigate("GuideDetail", { guide });
  };
  return (
    <TouchableWithoutFeedback onPress={onPress}>
      <Stack horizontal space={18} align="center">
        <LinearGradient
          colors={[colors.primary, "white"]}
          style={{
            alignItems: "center",
            justifyContent: "center",
            width: 76,
            height: 76,
            borderRadius: 38,
          }}
        >
          <Image source={{ uri: guide?.image }} style={styles.avatar} />
        </LinearGradient>
        <Stack>
          <Text style={styles.title}>{guide?.title}</Text>
          <Text style={styles.author}>{guide?.author?.name}</Text>
        </Stack>
      </Stack>
    </TouchableWithoutFeedback>
  );
};

export default GuideCard;

const styles = StyleSheet.create({
  avatar: {
    width: 62,
    height: 62,
    borderRadius: 31,
  },
  title: {
    fontFamily: "HelveticaNeue-Medium",
    fontSize: 18,
    color: colors.blackText,
  },
  author: {
    fontFamily: "Lato_400Regular",
    fontSize: 14,
    color: colors.grayText,
  },
});
