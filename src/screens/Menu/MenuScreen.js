import { AntDesign } from "@expo/vector-icons";
import { Box, Stack } from "@mobily/stacks";
import { openURL } from 'expo-linking';
import React from "react";
import {
  FlatList,
  Image,
  Linking,
  StatusBar,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import { useSafeAreaInsets } from "react-native-safe-area-context";
import colors from "res/colors";
import images from "res/images";
import Facebook from "res/svgs/ic_facebook.svg";
import Instagram from "res/svgs/ic_instagram.svg";
import Tiktok from "res/svgs/ic_tiktok";
import Twitter from "res/svgs/ic_twitter.svg";
import Youtube from "res/svgs/ic_youtube.svg";
import styled from "styled-components";
import GradientCircleContainer from "../../components/GradientCircleContainer";
import packageJson from "../../../package.json";

const MenuScreen = ({ navigation }) => {
  const insets = useSafeAreaInsets();

  const screens = [
    {
      title: "Tournaments",
      name: "Tournaments",
      icon: images.profile_tournament,
    },
    { title: "Weather", name: "Weather", icon: images.profile_weather },
    {
      title: "Crappie info",
      name: "CrappieInfo",
      icon: images.profile_crappie_info,
    },
    { title: "Find a Guide", name: "Guide", icon: images.profile_guide },
    { title: "Podcast", name: "Podcast", icon: images.show_tab_active, link: "https://www.iheart.com/podcast/269-brushpile-fishing-podcast-50133561/" },
    { title: "About", name: "About", icon: images.star },
  ];

  const renderItem = ({ item }) => {
    return (
      <TouchableOpacity
        onPress={() => {
          if(item.link == null) {
            navigation.navigate(item.name);
          } else {
            openURL(item.link);
          }
        }}
      >
        <Box alignX="between" alignY="center" direction="row">
          <Stack space={24} horizontal align="center">
            <Image source={item.icon} style={{ width: 32, height: 32 }} />
            <Text style={styles.itemText}>{item.title}</Text>
          </Stack>
          <AntDesign name="right" size={16} color="#CACDD6" />
        </Box>
      </TouchableOpacity>
    );
  };

  const renderSeparator = () => {
    return <View style={{ height: 24 }} />;
  };

  const navToSetting = () => {
    navigation.navigate("Setting");
  };

  const openLink = (link) => {
    Linking.canOpenURL(link).then((supported) => {
      if (supported) {
        Linking.openURL(link);
      } else {
        console.log("Don't know how to open URI: " + link);
      }
    });
  };

  return (
    <View style={styles.container}>
      <StatusBar barStyle="light-content" />
      <View
        style={[
          styles.banner,
          { height: insets.top + 40, paddingTop: insets.top },
        ]}
      >
        {/* <Stack space={15} horizontal align="center">
          <GradientCircleContainer size={76}>
            <Image
              style={styles.avatar}
              source={{ uri: "https://i.pravatar.cc/300" }}
            />
          </GradientCircleContainer>
          <Name>Josh Smith</Name>
        </Stack>
        <TouchableOpacity onPress={navToSetting}>
          <View style={styles.setting}>
            <Image source={images.setting} />
          </View>
        </TouchableOpacity> */}
      </View>
      <FlatList
        style={{
          paddingHorizontal: 20,
          paddingVertical: 32,
          borderTopLeftRadius: 24,
          borderTopRightRadius: 24,
          backgroundColor: "white",
        }}
        data={screens}
        keyExtractor={(item) => item.name}
        renderItem={renderItem}
        ItemSeparatorComponent={renderSeparator}
      />
      <View style={styles.social}>
        <Text>Follow us on social media</Text>
        <Stack horizontal space={32} paddingTop={16}>
          <TouchableOpacity
            onPress={() => {
              openLink("https://www.facebook.com/BrushPileFishing");
            }}
          >
            <Facebook width={26} height={26} />
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              openLink("https://www.twitter.com/BrushPileFishin");
            }}
          >
            <Twitter width={26} height={26} />
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              openLink("https://www.instagram.com/brushpilefishing");
            }}
          >
            <Instagram width={26} height={26} />
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              openLink("https://www.youtube.com/channel/UCXSV-Q4ryWyVXAMA_q6-iJA");
            }}
          >
            <Youtube width={26} height={26} />
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              openLink("https://www.tiktok.com/@brushpilefishing");
            }}
          >
            <Tiktok width={26} height={26} />
          </TouchableOpacity>
        </Stack>
        <Text style={styles.version}>
          BrushPile Fishing {packageJson.version}
        </Text>
      </View>
    </View>
  );
};

export default MenuScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.darkBlue,
  },
  version: {
    marginTop: 25,
    textAlign: 'left',
    backgroundColor: 'white',
    textDecorationLine: "underline",
  },
  banner: {
    height: 135,
    backgroundColor: colors.darkBlue,
    alignItems: "center",
    flexDirection: "row",
    paddingHorizontal: 30,
    justifyContent: "space-between",
  },
  avatar: {
    width: 62,
    height: 62,
    borderRadius: 31,
    backgroundColor: "white",
  },
  setting: {
    width: 40,
    height: 40,
    borderRadius: 8,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#354A7C",
  },
  itemText: {
    fontSize: 18,
    fontWeight: "bold",
    color: "#1A284F",
  },
  social: {
    paddingHorizontal: 30,
    paddingBottom: 20,
    backgroundColor: "white",
  },
});

const Name = styled.Text`
  font-family: Roboto-Bold;
  font-style: normal;
  font-weight: normal;
  font-size: 18px;
  color: white;
`;
