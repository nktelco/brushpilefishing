import React from "react";
import {StyleSheet, Text, View, Image, ScrollView, TouchableOpacity, Linking} from "react-native";
import colors from "../../res/colors";
import images from "../../res/images";

const AboutScreen = () => {
  return (
    <View style={styles.container}>
      <Image source={images.bgAbout} style={styles.background} />
      <ScrollView style={styles.content}>
        <>
          <Text style={styles.text}>
            An Educational Crappie Fishing Show By NKT Productions
          </Text>
          <Text style={styles.text}>
          This exciting and educational fishing series takes viewers into the world of Crappie fishing in Ohio, Kentucky, Indiana, Michigan, Illinois, South Carolina, Minnesota, Wisconsin, Virginia, Alabama, Tennessee, Louisiana, Texas…and the list continues to grow.
          </Text>
          <Text style={styles.text}>
          Join host Russ Bailey as he explores the art of Crappie fishing and checking out all of the tricks and techniques necessary to get that perfect Crappie!
          </Text>
          <TouchableOpacity onPress={() => {
            Linking.openURL('https://pxlplz.com');
          }}>
            <Text style={styles.text}>
              App Developed by: Pxlplz
            </Text>
          </TouchableOpacity>
          </>
      </ScrollView>
      <Image source={images.logo} style={styles.logo} />
    </View>
  );
};

export default AboutScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  background: {
    backgroundColor: colors.mainBack,
    width: "100%",
    resizeMode: "cover",
    position: "absolute",
    top: -16,
  },
  content: {
    marginTop: 200,
    borderTopLeftRadius: 32,
    borderTopRightRadius: 32,
    backgroundColor: "white",
    flex: 1,
  },
  text: {
    marginLeft: 32,
    marginRight: 32,
    marginTop: 20,
    fontFamily: "Lato_400Regular",
    fontSize: 18,
    lineHeight: 25,
  },
  logo: {
    position: "absolute",
    left: "15%",
    right: "15%",
    top: 61,
    width: "70%",
    resizeMode: "contain",
  },
});
