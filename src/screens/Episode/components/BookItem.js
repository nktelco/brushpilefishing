import React from "react";
import { Image, StyleSheet, Text, View } from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";
import { useNavigation } from "@react-navigation/native";
import colors from "res/colors";
import GradientCircleContainer from "../../../components/GradientCircleContainer";
import {getBpfApiPath} from "../../../api/api";
import images from "res/images";

const BookItem = ({ book, large, hideTag }) => {

  const navigation = useNavigation();

  const onPress = () => {
    navigation.navigate("GuideDetail", { guide: book });
  };

  book.available = true;
  const image = book.image ? `${getBpfApiPath()}/${book.image}` : null;

  return (
    <View style={large ? styles.containerLarge : styles.container}>
      <View style={styles.leftContainer}>
        <TouchableOpacity onPress={onPress}>
          <GradientCircleContainer size={large ? 113 : 54}>
            <Image
                style={large ? styles.largeavatar : styles.avatar}
                source={ image ? { uri: image } : images.photo}
            />
          </GradientCircleContainer>
        </TouchableOpacity>
        {
          !large && <Text style={styles.name}>{book.name}</Text>
        }
      </View>
      {
        large &&
          <>
            <Text style={styles.nameLarge}>{book.name}</Text>
            <Text style={styles.fishingSpotTitle}>{book.fishingspot_name}</Text>
          </>
      }
      {!hideTag &&
        <TouchableOpacity onPress={onPress}>
        <View
            style={[
              styles.button,
              {
                backgroundColor: book.available
                    ? colors.primary
                    : "rgba(140, 147, 167, 0.1)",
              },
            ]}
        >
          <Text
              style={[
                styles.buttonText,
                {color: book.available ? colors.whiteText : colors.darkGray},
              ]}
          >
            {book.available ? "Book" : "Unavailable"}
          </Text>
        </View>
      </TouchableOpacity>
      }
    </View>
  );
};

export default BookItem;

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    marginVertical: 6,
  },
  containerLarge: {
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "space-between",
    marginVertical: 6,
  },
  leftContainer: {
    flexDirection: "row",
    alignItems: "center",
  },
  avatar: {
    width: 48,
    height: 48,
    borderRadius: 24,
    backgroundColor: "white",
  },
  largeavatar: {
    width: 99,
    height: 99,
    borderRadius: 56,
    backgroundColor: "white",
  },
  name: {
    fontFamily: "Lato_400Regular",
    fontSize: 16,
    color: colors.blueText,
    paddingLeft: 10,
  },
  nameLarge: {
    fontFamily: "Lato_400Regular",
    fontSize: 18,
    color: colors.blueText,
    paddingTop: 6,
  },
  fishingSpotTitle: {
    fontFamily: "Lato_400Regular",
    fontSize: 14,
    color: colors.blueText,
    paddingTop: 6,
  },
  button: {
    height: 40,
    width: 110,
    borderRadius: 20,
    alignItems: "center",
    justifyContent: "center",
  },
  buttonText: {
    fontFamily: "Lato_400Regular",
    fontSize: 14,
  },
});
