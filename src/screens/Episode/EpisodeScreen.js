import { Stack } from "@mobily/stacks";
import EpisodePlayView from "components/EpisodePlayView";
import GuestAvatar from "components/GuestAvatar";
import React, { useEffect, useState } from "react";
import {
  ScrollView,
  View,
  StyleSheet,
  Text,
  ActivityIndicator, Alert,
} from "react-native";
import { StretchyScrollView } from "react-native-stretchy";
import styled from "styled-components";
import {
  loadEpisodeById,
  getEpisodeDetails,
  getGuidesByLocation,
} from "../../api/api";
import colors from "../../res/colors";
import BookItem from "./components/BookItem";
import WatchEpisodeButton from "../../components/WatchEpisodeButton";

const EpisodeScreen = ({ route, navigation }) => {
  const { episode } = route.params;

  const [bookList, setBookList] = useState([]);
  const [episodeData, setEpisodeData] = useState({
    episode: null,
    epnkttv_id: null,
    epguest: [],
    content: null,
    title: null,
    posterImage: null,
  });

  const [isLoading, setIsLoading] = useState(false);

  const sampleVideo =
    "https://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4";
  const sampleThumb =
    "https://commondatastorage.googleapis.com/gtv-videos-bucket/sample/images/BigBuckBunny.jpg";
  const [videoURL, setVideoURL] = useState(null);

  useEffect(() => {
    setVideoURL(null);

    if (episode) {
      setIsLoading(true);
      loadEpisodeById(episode)
        .then((result) => {
          const epnkttv_id = result.meta_box?.epnkttv_id;
          const epguest = result.meta_box?.epguest;
          const content_rendered = result.content?.rendered;
          const title_rendered = result.title?.rendered;
          const posterImage = result.jetpack_featured_media_url || null;

          console.log("epnkttv_id", epnkttv_id);

          const { latitude, longitude } = result.meta_box?.eplake_location;
          getGuidesByLocation(latitude, longitude, 50).then((result) => {
            setBookList(result);
          });

          //clean up the data
          const content = content_rendered
            .replace(/<[^>]*>/g, "")
            .replace(/&#.*?;/g, "")
            .replace(/\n/g, "")
            .replace("Original Air Date", "\n\nOriginal Air Date");
          const titleBeforeSplit = title_rendered.replace(/&#.*?;/g, "");
          const title = titleBeforeSplit.split("–");
          setEpisodeData({
            episode,
            epnkttv_id,
            epguest,
            content,
            title,
            posterImage,
          });
          setIsLoading(false);
          return getEpisodeDetails(epnkttv_id);
        })
        .then((result) => {
          const videoUrl = `${result.video?.sources?.hls}` ?? sampleVideo;
          console.log("SOURCES: ", result.video?.sources);
          if(videoUrl === "undefined") {
              console.log(result);
              Alert.alert("Video not available", "There was an error loading this video. Please try again later.")
          }
          setVideoURL(videoUrl);
        })
        .catch((e) => {
          console.log(e, "fail");
          setVideoURL(sampleVideo);
        });
    }
  }, [episode]);

  React.useLayoutEffect(() => {
    navigation.setOptions({
      title: "", //episode.place + ': ' + episode.title,
    });
  }, [navigation]);
  return (
    <>
      <ScrollView>
        <EpisodePlayView
          videoURL={videoURL}
          posterImage={episodeData.posterImage}
          videoId={episodeData.epnkttv_id}
        />
        {isLoading === true ? (
          <View
            style={[
              {
                backgroundColor: colors.darkBlue,
                flex: 1,
              },
            ]}
          >
            <ActivityIndicator
              style={{ marginTop: 100 }}
              size="large"
              color={colors.primary}
            />
          </View>
        ) : (
          <>
            <View style={styles.subContainer}>
              <Stack space={8} paddingBottom={30}>
                <Tag>{episodeData.title ? episodeData.title[1] || "" : ""}</Tag>
                <Title>
                  {episodeData.title ? episodeData.title[0] || "" : ""}
                </Title>
              </Stack>
              {episodeData.epguest.length > 0 ? (
                <Stack space={18} paddingBottom={38}>
                  <GrayText>Guest Stars</GrayText>
                  <Stack space={10}>
                    {episodeData.epguest.map((guest, index) => (
                      <Stack horizontal space={14} align="center" key={index}>
                        <GuestAvatar guest={guest}/>
                        <Name>{guest}</Name>
                      </Stack>
                    ))}
                  </Stack>
                </Stack>
              ) : null}

              {episodeData.content ? (
                <Stack space={8} paddingBottom={30}>
                  <GrayText>Episode Info</GrayText>
                  <Text style={styles.info}>{episodeData.content}</Text>
                </Stack>
              ) : null}
            </View>
            <View style={styles.bottomContainer}>
              {bookList.length > 0 && (
                <>
                  <Text style={styles.bookTitle}>
                    Book your next fishing trip with
                  </Text>
                  {bookList.map((book, index) => {
                    return <BookItem key={index.toString()} book={book} />;
                  })}
                </>
              )}
            </View>
          </>
        )}
      </ScrollView>
    </>
  );
};

export default EpisodeScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  subContainer: {
    padding: 20,
    paddingBottom: 38,
    backgroundColor: colors.darkBlue,
    borderBottomStartRadius: 14,
    borderBottomEndRadius: 14,
  },
  bottomContainer: {
    paddingTop: 24,
    paddingHorizontal: 20,
    paddingBottom: 44,
  },
  title: {
    fontFamily: "Roboto-Bold",
    fontSize: 28,
  },
  info: {
    fontFamily: "Lato_400Regular",
    fontSize: 14,
    color: colors.whiteText,
  },
  bookTitle: {
    fontFamily: "HelveticaNeue-Medium",
    fontSize: 21,
    color: colors.blueText,
    marginBottom: 17,
  },
});

const Title = styled.Text`
  font-family: Roboto-Bold;
  font-style: normal;
  font-weight: normal;
  font-size: 28px;
  color: ${colors.whiteText};
`;
const Tag = styled.Text`
  font-family: Roboto-Bold;
  font-style: normal;
  font-weight: normal;
  font-size: 17px;

  color: ${colors.orange};
`;
// const Text = styled.Text`
//   font-family: Lato_400Regular;
//   font-style: normal;
//   font-weight: normal;
//   font-size: 14px;
//   line-height: 23px;
//   color: ${colors.whiteText};
// `;

const Name = styled.Text`
  font-family: Lato_400Regular;
  font-style: normal;
  font-weight: normal;
  font-size: 18px;
  color: ${colors.whiteText};
`;

const GrayText = styled.Text`
  font-family: Roboto-Bold;
  font-style: normal;
  font-weight: normal;
  font-size: 16px;
  color: ${colors.whiteText};
  text-transform: uppercase;
`;
