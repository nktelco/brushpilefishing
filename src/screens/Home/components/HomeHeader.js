import React, { useContext } from "react";
import { StyleSheet, Text, View } from "react-native";
import { useSafeAreaInsets } from "react-native-safe-area-context";
import { Box, Stack } from "@mobily/stacks";
import { LinearGradient } from "expo-linear-gradient";
import colors, { timeOfDayGradientColors } from "res/colors";
import { LocationContext } from "../../../context/LocationProvider";
import { getWeatherSymbol } from "../../../helpers/helpers";
const HomeHeader = () => {
  const insets = useSafeAreaInsets();
  const { isLoading, currentWeather, location } = useContext(LocationContext);
  const timeArrayEntry = Math.floor(new Date().getHours() / 2);
  return isLoading ? (
    <View />
  ) : (
    <View style={[styles.container, { top: insets.top }]}>
      <View style={{}}>
        <LinearGradient
          colors={[
            timeOfDayGradientColors[timeArrayEntry],
            timeOfDayGradientColors[timeArrayEntry + 1],
          ]}
          style={{
            position: "absolute",
            top: 0,
            right: 0,
            left: 0,
            bottom: 0,
            borderColor: "rgba(255, 255, 255, 0.6)",
            borderWidth: 1,
            borderRadius: 10,
            overflow: "hidden",
          }}
          start={{ x: 0, y: 0.5 }}
          end={{ x: 1, y: 0.5 }}
        />
        <Stack horizontal space={6} paddingX={10} paddingY={8}>
          <View style={{ width: 20, height: 20 }}>
            {getWeatherSymbol(currentWeather?.current?.weather[0].main)}
          </View>
          <Text style={styles.text}>
            {currentWeather?.current ? `${Math.ceil(currentWeather?.current?.temp)}°F` : "--"}
          </Text>
          <Stack horizontal space={13}>
            <View style={{ backgroundColor: "white", width: 1, height: 19 }} />
            <Text style={styles.text}>{location ? location.name : "--"}</Text>
          </Stack>
        </Stack>
      </View>
    </View>
  );
};

export default HomeHeader;

const styles = StyleSheet.create({
  container: {
    height: 56,
    position: "absolute",
    width: "100%",
    zIndex: 10,
    justifyContent: "center",
    alignItems: "center",
  },
  text: {
    fontSize: 14,
    color: "white",
  },
});
