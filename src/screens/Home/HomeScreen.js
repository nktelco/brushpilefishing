import { Ionicons } from "@expo/vector-icons";
import { Box, Stack } from "@mobily/stacks";
import CalenderView from "components/CalenderView";
import CircleItem from "components/CircleItem";
import CrappieInfoCard from "components/CrappieInfoCard";
import EpisodeItem from "components/EpisodeItem";
import ShowHeader from "components/ShowHeader";
import TournamentItem from "components/TournamentItem";
import ViewMore from "components/ViewMore";
import {ActivityIndicator} from "react-native";
import React, {useContext, useEffect, useRef, useState} from 'react';
import { LocationContext } from "../../context/LocationProvider";
import { decodeHtmlCharCodes } from "../../helpers/helpers";

import {
  Dimensions,
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  TouchableWithoutFeedback,
  TouchableHighlight,
  Linking
} from "react-native";
import { FlatList } from "react-native-gesture-handler";
import Carousel, { Pagination } from "react-native-snap-carousel";
import { StretchyScrollView } from "react-native-stretchy";
import colors from "res/colors";
import images from "res/images";
import styled from "styled-components";
//import guides from "../../../assets/data/guides";
//import tournaments from "../../../assets/data/tournaments";

import {
    getAllVideoCategories,
    loadCrappieInfos,
    loadEpisodesForSeason,
    getNearestGuides,
    getLatestTournaments,
    getOffers, getBpfApiPath
} from "../../api/api";
import HeaderForeground from "../../components/HeaderForeground";
import HomeHeader from "./components/HomeHeader";
import BookItem from '../Episode/components/BookItem';
const HomeScreen = ({ navigation }) => {
  const [activeIndex, setActiveIndex] = useState(0);
  const [lastSeason, setLastSeason] = useState(null);

  const [infos, setInfos] = useState([]);
  const [infosLoading, setInfosLoading] = useState(true);

  const [guides, setGuides] = useState([]);
  const [guidesLoading, setGuidesLoading] = useState(true);

  const [tournaments, setTournaments] = useState([]);
  const [tournamentsLoading, setTournamentsLoading] = useState(true);

  const [offers, setOffers] = useState([]);
  const [offersLoading, setOffersLoading] = useState(true);

  const width = Dimensions.get("window").width;
  const { isLoading, currentWeather, location } = useContext(LocationContext);

  React.useLayoutEffect(() => {
    navigation.setOptions({
      headerRight: (props) => (
        <TouchableOpacity {...props} style={{ marginRight: 26 }}>
          <Ionicons name="search-outline" size={26} color="black" />
        </TouchableOpacity>
      ),
      headerTitle: (props) => (
        <Image
          source={images.home_header_title}
          style={{ backgroundColor: "red" }}
          {...props}
        />
      ),
    });
  }, [navigation]);

  useEffect(() => {
    const unsubscribe = fetchData();
    return () => {
      unsubscribe;
    };
  }, []);

  useEffect( () => {
      if(!isLoading) {
          getNearestGuides(location.lat, location.long, 5, 0).then((result) => {
            const itemDupes = [];
              const filteredList = result.filter((item) => {
                if(itemDupes.indexOf(item.id) < 0) {
                    itemDupes.push(item.id);
                    return true;
                }
                return false;
            });

              setGuides(filteredList);
              setGuidesLoading(false);
          }).catch((e) => {
            console.log(e);
            setGuidesLoading(false);
          })
      }
  }, [isLoading, location])

  const fetchData = async () => {
    const list = await getAllVideoCategories();
    if (list.length > 0) {
      let season = list[0];
      const episodes = await loadEpisodesForSeason(season.id, 3);
      setLastSeason({ ...season, episodes: episodes });
    }
    const crappieInfo = await loadCrappieInfos();
    setInfos(crappieInfo);
    setInfosLoading(false);

    const latestTournaments = await getLatestTournaments();
    const t = (latestTournaments !== undefined ) ? latestTournaments.events : [];

    const tournamentData = t.map((o) => {
        const title = decodeHtmlCharCodes(o.title); //titles[titles.length - 1];

        return({
            id: o.id,
            start_date: o.start_date,
            end_date: o.end_date,
            url: o.url,
            organizer: o.organizer ? o.organizer : [],
            title: title,
            images: o.image ? [o.image] : [images.tournamentDefault],
            tags: o.tags ? o.tags.map((tag) => (tag.name)) : [],
            description: o.description
        })
    });

    setTournaments(tournamentData);
    setTournamentsLoading(false);

    const offersData = await getOffers(10, 100, 0);
    setOffers(offersData);
    setOffersLoading(false);
  };

  return (
    <View style={styles.container}>
      <HomeHeader />
      <StretchyScrollView
        image={images.home_video_preview}
        foreground={<HeaderForeground />}
      >
          {
              //<ShowHeader/>
          }
        <Box style={{ backgroundColor: colors.darkBlue, height: 1 }}>
          <Box
            style={{
              backgroundColor: "rgba(255, 255, 255, 0.1)",
              marginHorizontal: 20,
              height: 1,
            }}
          />
        </Box>
        <Box
          style={{
            paddingTop: 15,
            paddingBottom: 25,
            backgroundColor: colors.darkBlue,
          }}
        >
          {lastSeason === null && <ActivityIndicator />}
          <Stack space={25}>
            <Box direction="row" alignX="between" marginX={20}>
              <EpisodeTitle>latest episode</EpisodeTitle>
              <EpisodeDetail>{lastSeason?.name}</EpisodeDetail>
            </Box>
            <ScrollView
              style={{
                flexGrow: 1,
                backgroundColor: colors.darkBlue,
              }}
              contentContainerStyle={{
                paddingLeft: 20,
                paddingRight: 20,
              }}
              horizontal
              nestedScrollEnabled={true}
              showsHorizontalScrollIndicator={false}
            >
              {lastSeason && (
                <Stack space={53} horizontal>
                  { lastSeason.episodes[0] && <EpisodeItem episode={lastSeason.episodes[0]} /> }
                  { lastSeason.episodes[1] && <EpisodeItem episode={lastSeason.episodes[1]} /> }
                  <ViewMore season={lastSeason} />
                </Stack>
              )}
            </ScrollView>
          </Stack>
        </Box>
        <Box style={{ backgroundColor: colors.darkBlue, height: 1 }}>
          <Box
            style={{
              backgroundColor: "rgba(255, 255, 255, 0.1)",
              marginHorizontal: 20,
              height: 1,
            }}
          />
        </Box>
        <Box paddingTop={30}>
          <View
            style={{
              backgroundColor: colors.darkBlue,
              position: "absolute",
              height: 177,
              top: 0,
              left: 0,
              right: 0,
              borderBottomStartRadius: 16,
              borderBottomEndRadius: 16,
            }}
          />
            {
                offersLoading && <ActivityIndicator />
            }
          <Carousel
            layout={"default"}
            data={offers}
            sliderWidth={width}
            itemWidth={width}
            autoplay={true}
            autoplayInterval={6000}
            loop={true}
            renderItem={({ item, index }) => {
              return (
                    <View>
                        <TouchableWithoutFeedback onPress={() => {
                            if(item.link) {
                                Linking.openURL(item.link);
                            }
                        }}>
                            <View
                              style={{
                                backgroundColor: "#C4C4C4",
                                borderRadius: 8,
                                aspectRatio: 335 / 255,
                                marginLeft: 27,
                                marginRight: 27,
                                overflow: "hidden",
                              }}
                            >
                              <Image
                                source={{uri: `${getBpfApiPath()}/${item.image}`}}
                                style={{
                                  width: "100%",
                                  height: "100%",
                                  resizeMode: "cover",
                                }}
                              />
                            </View>
                        </TouchableWithoutFeedback>
                        <Text
                            style={{
                                marginLeft: 27,
                                marginRight: 27,
                                fontSize: 20,
                                textAlign: "center",
                            }}
                        >
                            {item.text}
                        </Text>
                    </View>
              );
            }}
            onSnapToItem={(index) => setActiveIndex(index)}
          />
          <View
              style={{minHeight: 32}}
          >
              {
                  /*
                  <Pagination
                      dotsLength={offers.length}
                      activeDotIndex={activeIndex}
                      containerStyle={{backgroundColor: colors.mainBack}}
                      dotStyle={{
                          width: 7,
                          height: 7,
                          borderRadius: 3.5,
                          backgroundColor: colors.darkBlue,
                      }}
                      inactiveDotStyle={{
                          backgroundColor: "white",
                          borderColor: colors.darkGray,
                          borderWidth: 1,
                      }}
                      inactiveDotOpacity={1}
                      inactiveDotScale={1}
                  />
                  */
              }
          </View>
        </Box>
        <Box paddingLeft={25} paddingRight={27} paddingBottom={33}>
          <Stack space={24}>
            <Box direction="row" alignX="between">
                <SectionTitleDark>Fishing Log</SectionTitleDark>
                <TouchableOpacity onPress={() => {
                    navigation.navigate("LogsScreen");
                }}>
                <Text style={styles.sectionViewMore}>View More</Text>
                </TouchableOpacity>
            </Box>
            <CalenderView />
          </Stack>
        </Box>
        <Box paddingTop={32} paddingBottom={24}>
          <Box
            paddingLeft={25}
            paddingRight={27}
            direction="row"
            alignX="between"
          >
            <SectionTitleDark>Tournament Schedule</SectionTitleDark>
            <TouchableOpacity onPress={() => {
                navigation.navigate("Tournaments");
            }}>
              <Text style={styles.sectionViewMore}>View More</Text>
            </TouchableOpacity>
          </Box>
        </Box>
          {
              tournamentsLoading && <ActivityIndicator />
          }
        <FlatList
          data={tournaments}
          keyExtractor={(item) => item.id.toString()}
          contentContainerStyle={{ paddingHorizontal: 25 }}
          horizontal
          showsHorizontalScrollIndicator={false}
          renderItem={({ item }) => {
            return (
              <TournamentItem
                item={item}
                onPress={() => {
                  navigation.navigate("TournamentDetail", {
                    tournament: item,
                  });
                }}
              />
             );
          }}
          ItemSeparatorComponent={() => {
            return <View style={{ width: 27 }} />;
          }}
        />
        <Box paddingY={35} paddingLeft={20} paddingRight={20}>
          <Stack space={24}>
            <Box direction="row" alignX="between">
              <SectionTitleDark>Crappie info</SectionTitleDark>
                <TouchableOpacity onPress={() => {
                    navigation.navigate("CrappieInfo");
                }}>
                    <Text style={styles.sectionViewMore}>View More</Text>
                </TouchableOpacity>
            </Box>
              {
                  infosLoading && <ActivityIndicator />
              }
            <Stack space={10}>
              {infos.map((item) => {
                return <CrappieInfoCard key={item.id} guide={item} />;
              })}
            </Stack>
          </Stack>
        </Box>

        <Box paddingTop={18} paddingX={20} paddingBottom={22}>
          <Box direction="row" alignX="between">
            <SectionTitleDark>Find Guides</SectionTitleDark>
              <TouchableOpacity onPress={() => {
                  navigation.navigate("Guide");
              }}>
                <Text style={styles.sectionViewMore}>View More</Text>
              </TouchableOpacity>
          </Box>
        </Box>
          {guidesLoading &&
              <ActivityIndicator/>
          }
              <FlatList
                  data={guides}
                  keyExtractor={(item) => item.id + "_" + item.fishingspot_id}
                  contentContainerStyle={{
                      paddingHorizontal: 20,
                      paddingBottom: 44,
                  }}
                  horizontal
                  renderItem={({item}) => {
                      item.id;
                      return <BookItem key={item.id + "_" + item.fishingspot_id} book={item} large={true} hideTag={true}
                                       textColor="white"/>;
                  }}
                  ItemSeparatorComponent={() => {
                      return <View key={Math.random()} style={{width: 25}}/>;
                  }}
              />
      </StretchyScrollView>
    </View>
  );
};

export default HomeScreen;

const styles = StyleSheet.create({
  container: { flex: 1, backgroundColor: colors.mainBack },

  banner: {
    height: 285,
  },
  info: {
    height: 212,
  },
  sectionViewMore: {
    fontFamily: "Lato_400Regular",
    fontSize: 14,
    color: colors.primary,
  },
});

const SectionTitleDark = styled.Text`
  font-family: Roboto-Bold;
  font-style: normal;
  font-weight: bold;
  font-size: 21px;
  line-height: 22px;
  color: ${colors.darkBlue};
`;
// const Text = styled.Text`
//   font-family: Roboto-Regular;
//   font-style: normal;
//   font-weight: normal;
//   font-size: 13px;
//   line-height: 15px;
//   color: #000000;
// `;

// const ViewMoreText = styled.Text`
//   font-family: Lato_400Regular;
//   font-style: normal;
//   font-weight: normal;
//   font-size: 14px;
//   color: #000000;
// `;

const EpisodeTitle = styled.Text`
  font-family: HelveticaNeue-Bold;
  font-style: normal;
  font-weight: bold;
  font-size: 14px;
  line-height: 17px;
  text-transform: uppercase;
  color: white;
`;

const EpisodeDetail = styled.Text`
  font-family: HelveticaNeue-Bold;
  font-style: normal;
  font-weight: bold;
  font-size: 14px;
  line-height: 17px;
  color: #cacdd6;
`;

const AdItems = [
  {
    title: "Item 1",
    text: "Text 1",
  },
  {
    title: "Item 2",
    text: "Text 2",
  },
  {
    title: "Item 3",
    text: "Text 3",
  },
];

const GUIDE_DATA = [
  {
    id: 1,
    title: "Michel Salgado",
    place: "Lake Wobegone",
  },
  {
    id: 2,
    title: "Michel Salgado",
    place: "Lake Wobegone",
  },
  {
    id: 3,
    title: "Michel Salgado",
    place: "Lake Wobegone",
  },
  {
    id: 4,
    title: "Michel Salgado",
    place: "Lake Wobegone",
  },
];
