/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */
import "react-native-gesture-handler";
import {
    Lato_100Thin,
    Lato_100Thin_Italic,
    Lato_300Light,
    Lato_300Light_Italic,
    Lato_400Regular,
    Lato_400Regular_Italic,
    Lato_700Bold,
    Lato_700Bold_Italic,
    Lato_900Black,
    Lato_900Black_Italic,
} from "@expo-google-fonts/lato";
import { PortalProvider } from "@gorhom/portal";
import { StacksProvider } from "@mobily/stacks";
import { registerRootComponent } from "expo";
import { Asset } from "expo-asset";
import { useFonts } from "expo-font";
import * as SplashScreen from "expo-splash-screen";
import React, { useCallback, useEffect, useState } from "react";
import { Image, StyleSheet, View } from "react-native";
import LogsContextProvider from "./src/context/LogsContext";
import NewLogContextProvider from "./src/context/NewLogContext";
import AppNavigator from "./src/navigation/AppNavigator";
import images from "./src/res/images";
import { createDB } from "./src/api/db";
import { ActionSheetProvider } from "@expo/react-native-action-sheet";
import { BottomSheetModalProvider } from "@gorhom/bottom-sheet";
import LocationProvider from "./src/context/LocationProvider";
function cacheImages(images) {
    return images.map((image) => {
        if (typeof image === "string") {
            return Image.prefetch(images);
        } else {
            return Asset.fromModule(image).downloadAsync();
        }
    });
}

const fonts = {
    "Roboto-Regular": require("./assets/fonts/Roboto-Regular.ttf"),
    "Roboto-Bold": require("./assets/fonts/Roboto-Bold.ttf"),
    "HelveticaNeue-Bold": require("./assets/fonts/HelveticaNeue-Bold.ttf"),
    "HelveticaNeue-Regular": require("./assets/fonts/HelveticaNeue.ttf"),
    "HelveticaNeue-Medium": require("./assets/fonts/HelveticaNeue-Medium.ttf"),
    Lato_100Thin,
    Lato_100Thin_Italic,
    Lato_300Light,
    Lato_300Light_Italic,
    Lato_400Regular,
    Lato_400Regular_Italic,
    Lato_700Bold,
    Lato_700Bold_Italic,
    Lato_900Black,
    Lato_900Black_Italic,
};

const App = () => {
    const [appIsReady, setAppIsReady] = useState(false);

    let [fontsLoaded] = useFonts(fonts);

    const _loadAssetsAsync = async () => {
        const imageAssets = cacheImages(Object.values(images));

        await Promise.all([...imageAssets]);
    };

    useEffect(() => {
        async function prepare() {
            try {
                // Keep the splash screen visible while we fetch resources
                await SplashScreen.preventAutoHideAsync();
                // Pre-load fonts, make any API calls you need to do here
                await _loadAssetsAsync();
                createDB();
                // Artificially delay for two seconds to simulate a slow loading
                // experience. Please remove this if you copy and paste the code!
                await new Promise((resolve) => setTimeout(resolve, 2000));
            } catch (e) {
                console.warn(e);
            } finally {
                // Tell the application to render
                setAppIsReady(true);
            }
        }

        prepare();
    }, []);

    const onLayoutRootView = useCallback(async () => {
        if (appIsReady && fontsLoaded) {
            await SplashScreen.hideAsync();
        }
    }, [appIsReady, fontsLoaded]);

    if (!appIsReady || !fontsLoaded) {
        return null;
    }

    return (
        <BottomSheetModalProvider>
            <View style={{ flex: 1 }} onLayout={onLayoutRootView}>
                <ActionSheetProvider>
                    <StacksProvider spacing={1}>
                        <LocationProvider>
                            <LogsContextProvider>
                                <NewLogContextProvider>
                                    <AppNavigator />
                                </NewLogContextProvider>
                            </LogsContextProvider>
                        </LocationProvider>
                    </StacksProvider>
                </ActionSheetProvider>
            </View>
        </BottomSheetModalProvider>
    );
};

export default App;
